package zoomba.lang.test;

import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.collections.*;
import zoomba.lang.core.operations.ZCollection;

import java.security.SecureRandom;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 */
public class CollectionTest {

    final String COLLECTION_BASIC = "samples/test/collection_basic.zm" ;

    final String MAP_BASIC = "samples/test/map_basic.zm" ;

    final String FILE_BASIC = "samples/test/file_basic.zm" ;

    final String STRUCTURED_DATA = "samples/test/structured_data.zm" ;

    final String PROTO_TEST = "samples/test/proto_class.zm" ;

    final String ITERATOR_TEST = "samples/test/iterator_method.zm" ;

    final String CARDINALITY_TEST = "samples/test/cardinality.zm" ;

    final String EXOTIC_TEST = "samples/test/collections_exotic.zm" ;

    final String GROUP_TEST = "samples/test/groups.zm" ;

    final String LOGICAL = "samples/test/collections_logical.zm" ;

    final String RANDOM = "samples/test/collections_random.zm" ;

    final String SORTED = "samples/test/collection_sorted.zm" ;

    final String SORTED_LIST = "samples/test/collections_sorted_list.zm" ;

    final String RANGE_TEST = "samples/test/collections_range.zm" ;

    final String COMBINATORICS = "samples/test/combinatorics.zm" ;

    @Test
    public void testCollections()  {
        InterpreterTest.danceWithScript( COLLECTION_BASIC );
    }

    @Test
    public void testMap()  {
        InterpreterTest.danceWithScript( MAP_BASIC );
    }

    @Test
    public void testFiles()  {
        InterpreterTest.danceWithScript( FILE_BASIC );
    }

    @Test
    public void testStructuredData()  {
        InterpreterTest.danceWithScript( STRUCTURED_DATA );
    }

    @Test
    public void testProtoObject()  {
        InterpreterTest.danceWithScript( PROTO_TEST );
    }

    @Test
    public void testIteratorMethods()  {
        InterpreterTest.danceWithScript( ITERATOR_TEST );
    }

    @Test
    public void testCardinality()  {
        InterpreterTest.danceWithScript( CARDINALITY_TEST );
    }

    @Test
    public void testExotic()  {
        InterpreterTest.danceWithScript( EXOTIC_TEST );
    }

    @Test
    public void testLogical()  {
        InterpreterTest.danceWithScript( LOGICAL );
    }

    @Test
    public void testRandom()  {
        InterpreterTest.danceWithScript( RANDOM );
    }

    @Test
    public void testSortedCollections()  {
        InterpreterTest.danceWithScript( SORTED );
    }

    @Test
    public void testSortedList()  {
        InterpreterTest.danceWithScript( SORTED_LIST );
    }

    @Test
    public void testZRangeCollections()  {
        InterpreterTest.danceWithScript( RANGE_TEST );
    }

    @Test
    public void testPermutations(){
        InterpreterTest.danceWithScript( COMBINATORICS );
    }

    @Test
    public void testGroupedCollections(){
        InterpreterTest.danceWithScript( GROUP_TEST );
    }


    @Test
    public void testZArrayImmutability(){
        ZArray zArray = new ZArray( new Object[] { 1,2,3,4 }, false );
        Throwable exception = Assert.assertThrows( UnsupportedOperationException.class , zArray::clear);
        Assert.assertNotNull(exception);
        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.add(0));
        Assert.assertNotNull(exception);
        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.add(0,1));
        Assert.assertNotNull(exception);
        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.remove(1));
        Assert.assertNotNull(exception);
        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.remove(""));
        Assert.assertNotNull(exception);

        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.addAll(Collections.emptyList()));
        Assert.assertNotNull(exception);
        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.addAll(1, Collections.emptyList()));
        Assert.assertNotNull(exception);

        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.removeAll(  Collections.emptyList() ));
        Assert.assertNotNull(exception);

    }

    static class CloneTest implements Cloneable {
        ZCollection l = new ZList();
        @Override
        public Object clone(){
            CloneTest o = new CloneTest();
            o.l = l.deepCopy();
            return o;
        }

        @Override
        public boolean equals(Object o){
            if ( !(o instanceof CloneTest) ) return false;
            if ( this == o ) return true;
            return l.equals(((CloneTest)o).l);
        }

        @Override
        public int hashCode() {
            return l.hashCode();
        }
    }
    @Test
    public void deepCopyTest(){
        CloneTest ct = new CloneTest();
        ct.l.add(0);
        ct.l.add(42);
        ZSet zs = new ZSet();
        zs.add(ct);
        ZCollection zc = zs.deepCopy();
        CloneTest ctOther = (CloneTest) zc.iterator().next();
        Assert.assertNotSame(ct, ctOther);
    }


}
