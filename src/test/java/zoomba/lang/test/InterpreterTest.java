package zoomba.lang.test;

import org.junit.Test;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.oop.ZObject;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZException;

import javax.script.*;
import java.io.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;
import static zoomba.lang.core.interpreter.DSLPlugin.NANO_TO_SEC;

/**
 */
public class InterpreterTest {

    final String BASIC = "samples/test/basic.zm";

    final String CONDITION = "samples/test/conditions.zm";

    final String ITERATORS = "samples/test/iterators.zm";

    final String FUNCTIONS = "samples/test/functions.zm";

    final String PREDICATES = "samples/test/predicates.zm";

    final String NUMBERS = "samples/test/numbers.zm";

    final String PERFORMANCE = "samples/test/perf.zm";

    final String ARITHMETIC = "samples/test/arithmetic.zm";

    final String ATOMIC = "samples/test/atomic.zm";

    final String RETRY = "samples/test/retry.zm";

    final String IMPORT = "samples/test/import_test.zm";

    final String SCHED = "samples/test/schedulers.zm";

    final String PREC = "samples/test/precision_e.zm" ;

    final String MAP_LOGICAL = "samples/test/map_logical.zm" ;

    final String ZMB_REACTIVE = "samples/test/reactive.zm" ;

    final String THREAD_IMPORT = "samples/test/thread_import/a.zm" ;

    public static void printInts(Integer... args) {
        for (int i : args) {
            System.out.println(i);
        }
    }

    public static void printSomeInts(Integer x, Integer... args) {
        System.out.println(x);
        for (int i : args) {
            System.out.println(i);
        }
    }

    public static void danceWithScript(String scriptLocation) {
        returnDance(scriptLocation);
    }

    public static Function.MonadicContainer returnDance(String scriptLocation) {
        ZScript script = new ZScript(scriptLocation, null);
        Function.MonadicContainer mc = script.execute();
        if (mc.value() instanceof Throwable) {
            System.err.printf("%s\n", mc.value());
        }
        assertFalse(mc.value() instanceof Throwable);
        return mc;
    }

    @Test
    public void danceWithBasicFile() {
        danceWithScript(BASIC);
    }

    @Test
    public void danceWithConditionFile() {
        danceWithScript(CONDITION);
    }

    @Test
    public void danceWithIteratorFile() {
        danceWithScript(ITERATORS);
    }

    @Test
    public void danceWithFunctionsFile() {
        danceWithScript(FUNCTIONS);
    }

    @Test
    public void danceWithPredicatesFile() {
        danceWithScript(PREDICATES);
    }

    @Test
    public void danceWithNumbersFile() {
        danceWithScript(NUMBERS);
    }

    @Test
    public void danceWithArithmetic() {
        danceWithScript(ARITHMETIC);
    }

    @Test
    public void danceWithAtomic() {
        danceWithScript(ATOMIC);
    }

    @Test
    public void danceWithRetries() {
        danceWithScript(RETRY);
    }

    @Test
    public void danceWithImport() {
        Function.MonadicContainer mc = returnDance(IMPORT);
        assertEquals(42, mc.value());
    }

    @Test
    public void danceWithSchedulers() {
        Function.MonadicContainer mc = returnDance(SCHED);
        assertEquals(55, mc.value());
    }

    @Test
    public void danceWithPrecision() {
        Function.MonadicContainer mc = returnDance(PREC);
        assertEquals(true, mc.value());
    }

    @Test
    public void danceWithMapLogical() {
        Function.MonadicContainer mc = returnDance(MAP_LOGICAL);
        assertEquals(true, mc.value());
    }

    @Test
    public void danceWithReactive() {
        Function.MonadicContainer mc = returnDance(ZMB_REACTIVE);
        assertEquals(true, mc.value());
    }

    @Test
    public void danceWithThreadImport() {
        Function.MonadicContainer mc = returnDance(THREAD_IMPORT);
        assertEquals(true, mc.value());
    }

    int testJava() {
        int j = 0;
        for (int i = 0; i < 1000000; i++) {
            j = i;
        }
        return j;
    }

    public static final int MAX_LIMIT = 200;

    @Test
    public void dancePerformance() {
        Function.MonadicContainer mc = returnDance(PERFORMANCE);
        Number num = (Number) mc.value();
        long cur = System.nanoTime();
        testJava();
        long delta = System.nanoTime() - cur;
        double doubleDeltaInSec = delta * NANO_TO_SEC;
        System.out.printf("Java %s (sec): ZoomBA %s (sec)\n", doubleDeltaInSec, mc.value());
        double ratio = num.doubleValue() / doubleDeltaInSec;
        System.out.printf("Java is %s times faster than ZoomBA with limit %d\n", ratio, MAX_LIMIT);
        assertTrue((int) ratio < MAX_LIMIT);
    }

    @Test
    public void serialization() throws Exception {
        ZScript zs = new ZScript(BASIC,null);
        FileOutputStream fileOut =
                new FileOutputStream("basic.ser");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(zs);
        out.close();
        fileOut.close();
        // now read back again...
        FileInputStream fileIn = new FileInputStream("basic.ser");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        ZScript zds = (ZScript) in.readObject();
        in.close();
        fileIn.close();
        // now check if body are same, that is all that matters
        assertEquals(zs.body(), zds.body());
    }

    @Test
    public void javaxScripting() throws Exception {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        ScriptEngine engine = scriptEngineManager.getEngineByName("zoomba");
        assertNotNull(engine);
        Object result = engine.eval("2+2");
        assertEquals(4,result);
        Bindings bindings = engine.createBindings();
        bindings.put("x", 2 );
        bindings.put("y", 2 );
        result = engine.eval("x+y", bindings);
        assertEquals(4,result);
        ScriptContext scriptContext = engine.getContext();
        scriptContext.setAttribute("x", 2, ScriptContext.ENGINE_SCOPE );
        scriptContext.setAttribute("y", 2, ScriptContext.GLOBAL_SCOPE );
        result = engine.eval("x+y");
        assertEquals(4,result);
        CompiledScript compiledScript = ((Compilable)engine).compile("x+y");
        assertEquals( engine, compiledScript.getEngine());
        result = compiledScript.eval(bindings);
        assertEquals(4,result);
        // function invoke ?
        engine.eval("def add(a,b){ a + b }",scriptContext);
        result = ((Invocable)engine).invokeFunction("add", 2, 2);
        assertEquals(4,result);
        // method invoke ?
        Object zo = engine.eval(new StringReader("def ZO{ def add(a,b){ a + b } } ; new(ZO) ") ,scriptContext);
        assertNotNull(zo);
        assertTrue(zo instanceof ZObject);
        result = ((Invocable)engine).invokeMethod(zo,"add", 2, 2);
        assertEquals(4,result);
        // now attributes stuff
        assertEquals( 2, scriptContext.getScopes().size());
        assertNotNull(scriptContext.getAttribute("x"));
        final int scope = scriptContext.getAttributesScope("x");
        assertEquals(ScriptContext.ENGINE_SCOPE , scope);
        assertNotNull(scriptContext.getAttribute("y", ScriptContext.GLOBAL_SCOPE));
        scriptContext.removeAttribute("y", ScriptContext.GLOBAL_SCOPE);
        assertNull(scriptContext.getAttribute("y", ScriptContext.GLOBAL_SCOPE));
        assertNull(scriptContext.getAttribute("y"));
        assertEquals(-1, scriptContext.getAttributesScope("y"));
        // now let's eval a script... from external file
        zo = engine.eval("@samples/test/date.zm",scriptContext);
        assertNotNull(zo);
        compiledScript = ((Compilable)engine).compile("@samples/test/date.zm");
        assertNotNull(compiledScript);
        engine.put("y",10);
        assertEquals(10,engine.get("y"));
    }

    @Test
    public void testLeniency(){
        ZScript script = new ZScript("'1' + 1");
        script.lenient(false);
        Function.MonadicContainer mc = script.execute();
        script.lenient(true);
        assertTrue( mc.value() instanceof ZException );
        mc = script.execute();
        assertEquals( "11", mc.value() );
    }
}
