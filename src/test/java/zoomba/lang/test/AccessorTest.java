package zoomba.lang.test;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.ri.model.NodePointer;
import org.jscience.mathematics.number.Real;
import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.operations.ZJVMAccess;
import zoomba.lang.core.types.ZTypes;

import java.util.*;
import static org.junit.Assert.*;
import static zoomba.lang.core.interpreter.ZMethodInterceptor.Default.myPath;


/**
 */
public class AccessorTest {

    final Object[] arr = new Object[ ]{ 1, 2, 3, 4, 5, 6 } ;

    final List list = new ZList(arr);

    @Test
    public void testIntegerIndexAccessor(){
        Function.MonadicContainer c =  ZJVMAccess.getProperty(arr,0);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.getProperty(arr,arr.length-1);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.getProperty(arr,arr.length );
        assertTrue( c.isNil() );
        assertTrue( c == ZJVMAccess.INVALID_PROPERTY );

        c =  ZJVMAccess.setProperty(arr, 0, 'A');
        assertFalse( c.isNil() );
        assertTrue( c == Function.SUCCESS );
        assertEquals( 'A', arr[0] );


        c = ZJVMAccess.getProperty(list,0);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.setProperty(list, 1, 'B');
        assertFalse( c.isNil() );
        assertEquals( 'B', list.get(1) );


        c = ZJVMAccess.getProperty(list,"size" );
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );
        int size = (int)c.value() ;

        c =  ZJVMAccess.getProperty(list, size - 1 );
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.getProperty(list, size );
        assertTrue( c.isNil() );
        assertTrue( c == ZJVMAccess.INVALID_PROPERTY );

    }

    @Test
    public void testIndexer(){

        final Object[] keysNum = new Object[ ]{ 1, 2, 3, 4, 5, 6 } ;

        final Object[] valuesChar = new Object[ ]{ 'a', 'b', 'c', 'd', 'e', 'f' } ;

        final Map map = new ZMap(keysNum, valuesChar );

        Function.MonadicContainer c =  ZJVMAccess.getProperty(map,1);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Character );

        c = ZJVMAccess.getProperty(map,42);
        assertTrue( c.isNil() );
        assertTrue( c == ZJVMAccess.INVALID_PROPERTY );

        c = ZJVMAccess.setProperty(map,42, "42" );
        assertFalse( c.isNil() );
        assertEquals("42", map.get(42) );

        c = ZJVMAccess.getProperty(map,"size");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.size());

        c = ZJVMAccess.getProperty(map,"keys");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.keySet());

        c = ZJVMAccess.getProperty(map,"values");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.values());

        c = ZJVMAccess.getProperty(map,"entries");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.entrySet());
    }

    @Test
    public void xpathTest(){
        Object root = ZTypes.json( "samples/large_json.json", true);
        JXPathContext context = JXPathContext.newContext(root);
        Iterator iter  = context.iteratePointers("//products");
        // all of these should be valid now...
        while ( iter.hasNext() ){
            NodePointer dp = (NodePointer) iter.next();
            String path = myPath(dp);
            // get new ptr
            NodePointer p = (NodePointer)context.getPointer(path);
            Assert.assertTrue(p.isActual());
        }
    }

    @Test
    public void orderingVarArgsTest(){
        Object foo = new Object(){
            public int firstCalled = 0;
            public int secondCalled = 0;

            public void printf(Integer i, String msg, Object... args){
                secondCalled++;
                System.out.print(i + " >> ");
                System.out.printf(msg, args);
            }

            public void printf(String msg, Object... args){
                firstCalled ++;
                System.out.printf(msg, args);
            }
        };
        // 1
        ZJVMAccess.callMethod( foo, "printf" , new Object[]{ "Hello, world %n" });
        Assert.assertEquals( 1, ZJVMAccess.getProperty( foo, "firstCalled").value() );
        // 1-1
        ZJVMAccess.callMethod( foo, "printf" , new Object[]{ "Hello, world : %s %n", "foo-bar" });
        Assert.assertEquals( 2, ZJVMAccess.getProperty( foo, "firstCalled").value());

        // 2-1
        ZJVMAccess.callMethod( foo, "printf" , new Object[]{ 0, "Hello, world : %s %n", "foo-bar" });
        Assert.assertEquals( 1, ZJVMAccess.getProperty( foo, "secondCalled").value());
        Throwable throwable = null;

        try {
            ZJVMAccess.callMethod( foo, "printf" , new Object[]{  });
        } catch (Throwable t){
            throwable = t;
        }
        Assert.assertNotNull(throwable);
    }

    interface Foo{
        default int value(){
            return 42;
        }
    }

    interface Bar{
        default int value(){
            return 41;
        }
    }

    @Test
    public void j8DefaultInterfaceTest(){
        Foo f = new Foo(){};
        Bar b = new Bar(){};
        Object r = ZJVMAccess.callMethod(f, "value", new Object[]{});
        Assert.assertEquals( 42,  r );
        r = ZJVMAccess.callMethod(b, "value", new Object[]{});
        Assert.assertEquals( 41,  r );
        Function.MonadicContainer mc = ZJVMAccess.getProperty(f, "value");
        Assert.assertEquals( 42,  mc.value() );
    }

    private boolean setSomethingWasCalled = false;

    public void setSomething(int x){
        System.out.println(x);
        setSomethingWasCalled = true;
    }

    private boolean formatWasCalled = false;

    public void format(String fmt, Object...args){
        System.out.printf(fmt,args);
        formatWasCalled = true ;
    }

    @Test
    public void jvmNumericArgsTest(){
        Object[] args = new Object[]{  Real.valueOf("42.90809809890890808") } ;
        Object r = ZJVMAccess.callMethod(Math.class, "abs", args);
        Assert.assertEquals( 42.9d , (double)r , 0.1d );
        r = ZJVMAccess.callMethod(Math.class, "signum", args);
        Assert.assertEquals( 1.0d , (double)r, 0.001 );
        // can it do null / void return
        r = ZJVMAccess.callMethod(this, "setSomething", new Object[]{ Real.valueOf(1)});
        Assert.assertNull(r);
        Assert.assertTrue(setSomethingWasCalled);

        // can it do null / void return
        r = ZJVMAccess.callMethod(this, "format", new Object[]{ "foo %s", "bar" });
        Assert.assertTrue(r instanceof Function.MonadicContainer);
        Assert.assertTrue(((Function.MonadicContainer) r).isNil());
        Assert.assertTrue(formatWasCalled);
    }

    @Test
    public void jvmFineTunedNumericMethodTest(){
         //Number abs(Number n)
        double x = 1.0;
        Math.abs((int)x);
         Object r = ZJVMAccess.callMethod(Math.class, "abs", new Object[] {1} );
         Assert.assertEquals( 1, r );
    }

    static class OverloadingIssue{

        public int function(int i, int  j){
            return 2;
        }
        public int function(int...args){
            return 42;
        }
        public int function(int i){
            return 1;
        }

        public int log(String fmt, Throwable err){
            return 3;
        }
        public int log(String data){
            return 1;
        }
        public int log(String fmt, Object data){
            return 2;
        }
    }

    @Test
    public void testVarArgsOverLoad(){
        final OverloadingIssue issue = new OverloadingIssue();
        Object r = ZJVMAccess.callMethod(issue, "function", new Object[]{});
        Assert.assertEquals( 42, r );
        r = ZJVMAccess.callMethod(issue, "function", new Object[]{ 42 });
        Assert.assertEquals( 1, r );
        r = ZJVMAccess.callMethod(issue, "function", new Object[]{ 42 , 420 });
        Assert.assertEquals( 2, r );
    }

    static final class StringFormatterIssue {
        public int log(String msg, Object...args){ return 42 ; }
        public int log(String msg){ return 0 ; }

    }

    @Test
    public void testStringFormatterArgsOverLoad(){
        final StringFormatterIssue issue = new StringFormatterIssue();
        Object r = ZJVMAccess.callMethod(issue, "log", new Object[]{ "hi!!"});
        Assert.assertEquals( 0, r );
        r = ZJVMAccess.callMethod(issue, "log", new Object[]{ "hi" , "hello!" });
        Assert.assertEquals( 42, r );
    }

    @Test
    public void testPerformance(){
        Object[] args = new Object[]{  Real.valueOf("42.90809809890890808") } ;
        final int times = 1000000;
        long start = System.currentTimeMillis();

        for ( int i=0; i< times ; i++ ) {
            Object r = ZJVMAccess.callMethod(Math.class, "sin", args);
        }
        long zDelta = System.currentTimeMillis() - start;
        start = System.currentTimeMillis();
        for ( int i=0; i< times; i++ ) {
            Object r = Math.sin(((Number)args[0]).doubleValue());
        }
        long jDelta = System.currentTimeMillis() - start;
        double ratio = zDelta / (double)jDelta;
        final boolean isRunningInCI = Boolean.TRUE.equals(
                ZTypes.bool(System.getProperty("inCI", "false"),false) );
        final int maxRatioAllowed = isRunningInCI ? 100: 25 ;
        System.out.println("Running in CI : " + isRunningInCI );
        System.out.printf("%d JVMAccess Method call (dynamic) zmb : %d , jvm (static) : %d , ratio: %.2f %n",
                times,  zDelta, jDelta, ratio );
        Assert.assertTrue( ratio < maxRatioAllowed );
    }
}
