package zoomba.lang.test;

import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.io.ZFileSystem;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ZFSTest {

    final static String THIS_FILE = "src/test/java/zoomba/lang/test/ZFSTest.java" ;
    final static String SAMPLES = "samples" ;
    final static String SAMPLES_TEST = "samples/test" ;

    public static Path createTmpFile(String fileName, String text) throws Exception {
        String tmpdir = System.getProperty("java.io.tmpdir");
        Path p = Paths.get(tmpdir, fileName);
        return Files.write( p, text.getBytes(StandardCharsets.UTF_8) );
    }

    @Test
    public void nameAndExtensionTest(){
        ZFileSystem.ZFile zFile = ZFileSystem.file( THIS_FILE) ;
        Assert.assertTrue( zFile.file.exists() );
        Assert.assertEquals( ".java", zFile.extension() );
        Assert.assertEquals( "ZFSTest", zFile.baseName() );
    }

    @Test
    public void parentChildTest(){
        ZFileSystem.ZFile sampleRoot = ZFileSystem.file( SAMPLES_TEST) ;
        Assert.assertTrue( sampleRoot.file.isDirectory() );
        Assert.assertFalse( sampleRoot.children().isEmpty() );
        Assert.assertEquals(SAMPLES, sampleRoot.parent().baseName() );
        Assert.assertTrue( ZFileSystem.file("foo-bar" ).children().isEmpty() ) ;
        Assert.assertTrue( ZFileSystem.file(THIS_FILE ).children().isEmpty() ) ;
    }

    @Test
    public void pathNameTest() throws Exception {
        ZFileSystem.ZFile sampleRoot = ZFileSystem.file( SAMPLES_TEST) ;
        Assert.assertTrue( sampleRoot.canon().startsWith("/") );
        final String rel = sampleRoot.relative();
        Assert.assertEquals( SAMPLES_TEST, rel );
    }

    @Test
    public void walkTest() throws Exception {
        ZFileSystem.ZFile sampleRoot = ZFileSystem.file( SAMPLES_TEST) ;
        Assert.assertTrue( sampleRoot.file.isDirectory() );
        sampleRoot.descendants().forEach(zf -> {
            if ( zf.file.isDirectory() ) return;
            Assert.assertEquals( ".zm", zf.extension() );
        });
    }

    @Test
    public void copyTest() throws Exception {
        Path p = createTmpFile( "hello.txt", "Hello, World!");
        ZFileSystem.ZFile file = ZFileSystem.file(p.toString());
        Assert.assertTrue( file.file.exists() );
        ZFileSystem.ZFile copy = file.copy( file.parent(), "hello2.txt" );
        Assert.assertTrue( copy.file.exists() );
        // check they have same data ?
        Assert.assertEquals( file.string(), copy.string() );
        file.file.delete();
        copy.file.delete();
    }

    @Test
    public void moveTest() throws Exception {
        final String text = "Hello, World!" ;
        Path p = createTmpFile( "hi.txt", text);
        ZFileSystem.ZFile file = ZFileSystem.file(p.toString());
        Assert.assertTrue( file.file.exists() );
        Assert.assertEquals( text, file.string());
        ZFileSystem.ZFile move = file.move( file.parent(), "hi2.txt" );
        Assert.assertFalse( file.file.exists() );
        Assert.assertEquals( text, move.string() );
        move.file.delete();
    }
}
