package zoomba.lang.test;

import org.jscience.mathematics.number.LargeInteger;
import org.jscience.mathematics.number.Real;
import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.collections.ZSet;
import zoomba.lang.core.types.ZNumber;

import java.math.BigInteger;
import java.util.Set;

import static org.junit.Assert.*;

/**
 */
public class ZNumberTest {

    @Test
    public void testAssignment(){
        ZNumber zn = new ZNumber(0);

        assertTrue( zn.actual() instanceof Integer );
        zn = new ZNumber(0.00);

        assertTrue( zn.actual() instanceof Double );

        // test auto assign
        zn = new ZNumber("0.121010011104111973947987917179719219798783798473974398");
        assertTrue( zn.actual() instanceof Real);

        zn = new ZNumber("0.121010011100000");
        assertTrue( zn.actual() instanceof Double);

        zn = new ZNumber(1134234229);
        assertTrue( zn.actual() instanceof Integer );

        zn = new ZNumber(1134234229998989l);
        assertTrue( zn.actual() instanceof Long );

        zn = new ZNumber("1134234229998989979473948759348759834793");
        assertTrue( zn.actual() instanceof LargeInteger);

        ZNumber n0 = new ZNumber(0);
        ZNumber d0 = new ZNumber(0.0);
        assertTrue( n0.equals(d0) );

        ZNumber d01 = new ZNumber(0.01);
        assertFalse( n0.equals(d01) );
        assertFalse( d0.equals(d01) );

        assertEquals( n0.compareTo(d0) , 0 );
        assertEquals( d0.compareTo(d01) , -1 );
        assertEquals( n0.compareTo(d01) , -1 );

    }

    @Test
    public void testAbs(){

        ZNumber n0 = new ZNumber(0);
        ZNumber d0 = new ZNumber(0.0);

        assertEquals( n0._abs_() , 0 );
        assertEquals( d0._abs_() , 0.0 );

        ZNumber d01 = new ZNumber(0.01);
        assertTrue( d01._abs_().doubleValue() - 0.01 < 0.0001);
        d01 = new ZNumber(-0.01);
        assertTrue( d01._abs_().doubleValue() > 0 );
        assertTrue( d01._abs_().doubleValue() - 0.01 < 0.0001);

        ZNumber zn = new ZNumber("0.121010011104111973947987917179719219798783798473974398");
        assertTrue( zn._abs_() instanceof Real);

    }

    @Test
    public void testAdd(){
        ZNumber n0 = new ZNumber(0);
        ZNumber d0 = new ZNumber(0.0);
        assertEquals( 0.0, d0._add_(n0));
        assertEquals( 0.0, n0._add_(d0));
        long ln = -123213131312311131l ;
        ZNumber zln = new ZNumber(ln);
        // must upcast?
        Object r = zln._add_(n0);
        assertEquals( ln , r);
    }

    @Test
    public void testPower(){
        ZNumber zn = new ZNumber(12331201231.12321312312301301d);
        long p = 0 ;
        Object o = zn._pow_(p);
        assertEquals( 1.0d, o );
        zn = new ZNumber("121213123182012810280129824109481098");
        o = zn._pow_(p);
        assertEquals( 1, o );
    }

    @Test
    public void testMultiplication(){
        int i = 6 ;
        ZNumber n = new ZNumber(7);
        Object o = n._mul_(i);
        assertEquals(42,o);
        n = new ZNumber(7.0);
        o = n._mul_(i);
        assertEquals(42.0,o);
        o = n._mul_(6.00);
        assertEquals(42.0,o);
    }

    @Test
    public void testDivision(){
        int i = 6 ;
        ZNumber n = new ZNumber(42);
        Object o = n._div_(i);
        assertEquals(7,o);

        n = new ZNumber(42.0);
        o = n._div_(6);
        assertEquals(7.0,o);

        ZNumber a = new ZNumber("0.11213131010801823013821093221097319873211381981111109099080921");
        ZNumber b = new ZNumber("0.9809809809890119018230981203981309173891481640872630164012746127");
        o = a._div_(b);
        assertTrue( o instanceof Real );
    }

    @Test
    public void testLogic(){
        ZNumber n = new ZNumber(1);
        n.and_mutable(0);
        assertEquals(n,0);

        n.or_mutable(1);
        assertEquals(n,1);

        n.xor_mutable(0);
        assertEquals(n,1);

        n.mod_mutable( 2);
        assertEquals(n,1);

        n.not_mutable();
        // this should work out
        assertEquals( n , -1);
        // now the mutable logic
        n.sub_mutable(-1);
        assertEquals( n , 0);

    }

    @Test
    public void testExtended(){
        ZNumber n = new ZNumber(1.1);
        assertEquals(1, n.floor() );
        assertEquals(2, n.ceil() );
        // test hash code
        Set s = ZSet.set( null, new ZNumber(1), new ZNumber(1) );
        assertEquals( s.size(), 1);
        s.clear();
        s = ZSet.set( null, new ZNumber(1), n );
        assertEquals( s.size(), 2);
    }

    @Test
    public void cielTest(){
        ZNumber zn = new ZNumber(0.1);
        assertEquals(1, zn.ceil());
        zn = new ZNumber(2.1);
        assertEquals(3, zn.ceil());
        zn = new ZNumber(1.5);
        assertEquals(2, zn.ceil());
        zn = new ZNumber(1.7);
        assertEquals(2, zn.ceil());

        zn = new ZNumber(-2.1);
        assertEquals(-2, zn.ceil());
        zn = new ZNumber(-2.0);
        assertEquals(-2, zn.ceil());
        zn = new ZNumber(0.0);
        assertEquals(0, zn.ceil());
    }

    @Test
    public void floorTest(){
        ZNumber zn = new ZNumber(0.1);
        assertEquals(0, zn.floor());
        zn = new ZNumber(0.5);
        assertEquals(0, zn.floor());

        zn = new ZNumber(2.1);
        assertEquals(2, zn.floor());
        zn = new ZNumber(-0.1);
        assertEquals(-1, zn.floor());
        zn = new ZNumber(-1.0);
        assertEquals(-1, zn.floor());

        zn = new ZNumber(-2.1);
        assertEquals(-3, zn.floor());
        zn = new ZNumber(-2.0);
        assertEquals(-2, zn.floor());
        zn = new ZNumber(0.0);
        assertEquals(0, zn.floor());
    }

    @Test
    public void roundingTest(){
        Number rn = ZNumber.round(0.2314, 2 );
        assertEquals( 4,rn.toString().length());
        assertEquals( rn, 0.23);
        rn = ZNumber.round(0.2514, 1 );
        assertEquals( 3,rn.toString().length());
        assertEquals( rn, 0.3);
    }

    @Test
    public void powerTest(){
        // -ve power
        ZNumber n = new ZNumber(2.0);
        Number p = (Number) n._pow_(-1);
        assertEquals(p,0.5);
        // 0 power
        n = new ZNumber(2.0);
        p = (Number) n._pow_(0);
        assertEquals(p,1.0);
        // fractional power
        n = new ZNumber(4.0);
        p = (Number) n._pow_(0.5);
        assertEquals(p,2.0);
    }

    private void testEquality(final String rep){
        Real ro = Real.valueOf(rep) ;
        Real rs = Real.valueOf( ro.toString() );
        assertEquals(ro, rs);
    }

    @Test
    public void correctStringRep(){
        testEquality("10001111213110.000000000231231231313131" );
        testEquality("0.908080800101820182301831903818797987978932781313813813810" );
        testEquality("0.0" );
        testEquality("1.0" );
        testEquality("-1.0" );
        testEquality("-0.0" );
        testEquality("42" );
    }

    @Test
    public void precisionJscience(){
        StringBuilder builder = new StringBuilder("0.");
        for ( int i=0; i < 100; i++){
            builder.append(i%10);
        }
        Real r = Real.valueOf(builder.toString());
        Real r_sq = r.times(r);
        Assert.assertTrue( r_sq.toString().length() > 200 );
    }

    @Test
    public void Issue41(){
        LargeInteger li = LargeInteger.valueOf("0000000000000000000000000000001");
        assertFalse(li.isZero());
        // now try the same with Real...?
        Real r = Real.valueOf("0.000000000000000000000000000000000000000000000000001");
        assertNotEquals( Real.ZERO, r);
        Number n = ZNumber.number("0.000000000000000000000000000000000000000000000000001");
        assertTrue( n instanceof  Double);
        n = ZNumber.number("0.000000000000000121213187897798790000000000000000000000001");
        assertTrue( n instanceof  Real);
    }

    @Test
    public void integerValueTest(){
        ZNumber zn = new ZNumber(1.0 );
        Assert.assertTrue( zn.integerValued() );
        zn = new ZNumber(12121.0 );
        Assert.assertTrue( zn.integerValued() );
        zn = new ZNumber(-1211.0 );
        Assert.assertTrue( zn.integerValued() );
        zn = new ZNumber(-11.0 );
        Assert.assertTrue( zn.integerValued() );
        // fractional
        zn = new ZNumber(-11.1 );
        Assert.assertFalse( zn.integerValued() );

    }

    /**
     * <a href="https://math.tools/calculator/base/10-62">...</a>
     */
    @Test
    public void toRadixConversionTest(){
        Assert.assertEquals( "0", ZNumber.radix(BigInteger.valueOf(0), 50 ));
        Assert.assertEquals( "10", ZNumber.radix(BigInteger.valueOf(50), 50 ));
        Assert.assertEquals( "-10", ZNumber.radix(BigInteger.valueOf(-50), 50 ));

        Assert.assertEquals( "j8oomjdGeneLBR85B", ZNumber.radix(new BigInteger("129100981290121212131231121999"), 52 ));
        Assert.assertEquals( "2Z8kMb4FAaEMgIgaA9ULYjVDF", ZNumber.radix(new BigInteger("408908098908790712907831290387190827301987"), 52 ));

        Assert.assertEquals( "10", ZNumber.radix(BigInteger.valueOf(62), 62 ));
        Assert.assertEquals( "-10", ZNumber.radix(BigInteger.valueOf(-62), 62 ));

        Assert.assertEquals( "2htso7V2orzNciEkJ", ZNumber.radix(new BigInteger("129100981290121212131231121999"), 62 ));
        Assert.assertEquals( "UkYUqKHu6QjUi4E5OP", ZNumber.radix(new BigInteger("90890123103198039123219998878797"), 62 ));

    }

    @Test
    public void fromRadixConversionTest(){
        Assert.assertEquals( "0", ZNumber.intRadix("0", 50 ).toString() );
        Assert.assertEquals( "50", ZNumber.intRadix("10", 50 ).toString() );
        Assert.assertEquals( "-50", ZNumber.intRadix("-10", 50 ).toString() );

        Assert.assertEquals( "129100981290121212131231121999",  ZNumber.intRadix("j8oomjdGeneLBR85B", 52 ).toString());
        Assert.assertEquals( "90890123103198039123219998878797",  ZNumber.intRadix("UkYUqKHu6QjUi4E5OP", 62 ).toString());

        Exception ex = Assert.assertThrows( IllegalArgumentException.class , () ->{
            ZNumber.intRadix("*", 45);
        });
        Assert.assertNotNull(ex);
    }
}
