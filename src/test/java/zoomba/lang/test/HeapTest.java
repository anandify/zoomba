package zoomba.lang.test;

import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.collections.ZHeap;
import zoomba.lang.core.operations.Function;

import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class HeapTest {

    final Integer[] ints = new Integer[]{ 100, 2, 4, 5, 1000, 6 , 1, 100, 100, 6, 6 } ;

    final List<Integer> list = Arrays.asList(ints);

    @Test
    public void noComparatorMinHeapTest(){
        final int size = 5;
        ZHeap heap = new ZHeap(size);
        heap.addAll(list);
        Assert.assertEquals(size, heap.size() );
        Assert.assertEquals(1, heap.top());
        Assert.assertEquals(1, heap.min());
        Assert.assertEquals(6, heap.max());
    }

    @Test
    public void noComparatorMaxHeapTest(){
        final int size = 5;
        ZHeap heap = new ZHeap(size,true);
        heap.addAll(list);
        Assert.assertEquals(size, heap.size() );
        Assert.assertEquals(1000, heap.top());
        Assert.assertEquals(6, heap.min());
        Assert.assertEquals(1000, heap.max());
    }

    @Test
    public void compartorMinHeapTest(){
        final int size = 5;
        ZHeap heap = new ZHeap(size, false, Comparator.naturalOrder() );
        heap.addAll(list);
        Assert.assertEquals(size, heap.size() );
        Assert.assertEquals(1, heap.top());
        Assert.assertEquals(1, heap.min());
        Assert.assertEquals(6, heap.max());
    }

    @Test
    public void comparatorMaxHeapTest(){
        final int size = 5;
        ZHeap heap = new ZHeap(size,true, Comparator.naturalOrder());
        heap.addAll(list);
        Assert.assertEquals(size, heap.size() );
        Assert.assertEquals(1000, heap.top());
        Assert.assertEquals(6, heap.min());
        Assert.assertEquals(1000, heap.max());
    }

    @Test
    public void inverseComparatorMinHeapAsMaxHeapTest(){
        final int size = 5;
        ZHeap heap = new ZHeap(size,false, Comparator.reverseOrder());
        heap.addAll(list);
        Assert.assertEquals(size, heap.size() );
        Assert.assertEquals(1000, heap.top());
        Assert.assertEquals(1000, heap.min());
        Assert.assertEquals(6, heap.max());
    }

    @Test
    public void functionMinHeapTest(){
        final int size = 5;
        Function f = new Function.Mapper() {
            @Override
            public Object map(Object... args) {
                return null;
            }

            @Override
            public String body() {
                return "";
            }

            @Override
            public MonadicContainer execute(Object... args) {
                return new MonadicContainerBase( args[1] );
            }

            @Override
            public String name() {
                return "";
            }
        };

        ZHeap heap = new ZHeap(size, false, f);
        heap.addAll(list);
        Assert.assertEquals(size, heap.size() );
        Assert.assertEquals(1, heap.top());
        Assert.assertEquals(1, heap.min());
        Assert.assertEquals(6, heap.max());
    }

    @Test
    public void heapManipulationTest(){
        final int size = 5;
        ZHeap heap = new ZHeap(size);
        heap.addAll(list);
        Assert.assertEquals(size, heap.size() );
        // now remove
        Assert.assertEquals( 1, heap.remove(0));
        Assert.assertEquals(size-1, heap.size() );
        Assert.assertEquals(2, heap.top() );
        // now add bunch of stuff
        Assert.assertTrue( heap.add( 99) );
        // now add bunch of stuff which should not be added
        Assert.assertFalse( heap.add( 600) );
    }

    @Test
    public void heapPerformanceTest(){
        final int heapSize = 42;  // default value
        final int colSize = 1000;
        final int itemBound = 100;
        final int times = 20000;
        final Random r = new SecureRandom();
        List<Integer> list = IntStream.range(0,colSize).map(x -> r.nextInt(itemBound) ).boxed().collect(Collectors.toList());
        // for zoomba heap
        long start = System.currentTimeMillis();
        IntStream.range(0,times).forEach( (x)->{
            ZHeap heap = new ZHeap(heapSize);
            heap.addAll(list);
        });
        long delta1 = System.currentTimeMillis() - start;
        // for Java PriorityQueue
        start = System.currentTimeMillis();
        IntStream.range(0,times).forEach( (x)->{
            PriorityQueue<Integer> heap = new PriorityQueue<>(heapSize);
            heap.addAll(list);
        });
        long delta2 = System.currentTimeMillis() - start;
        long f = delta1 / delta2 ;
        final long limit = 15;
        System.out.printf("ZHeap took %d ms while PriorityQueue took %d ms, %d is the fraction with limit %d", delta1, delta2, f, limit);
        Assert.assertTrue( f <= limit );
    }
}
