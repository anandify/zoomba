package zoomba.lang.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.collections.ZSet;
import zoomba.lang.core.interpreter.ZContext;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.operations.Function;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 */
public class FunctionsTest {

    final String BASIC = "samples/test/functions_basic.zm" ;

    final String EXTENDED = "samples/test/functions_extended.zm" ;

    final String THREADING = "samples/test/sys_thread.zm" ;

    final String PROCESS_MAC = "samples/test/sys_proc.zm" ;

    final String PROCESS_WIN = "samples/test/sys_proc_win.zm" ;

    final String PROCESS_LINUX = "samples/test/sys_proc_linux.zm" ;

    final String DATABASE = "samples/test/sys_db.zm" ;

    final String IO = "samples/test/sys_io.zm" ;

    final String WEB = "samples/test/sys_web.zm" ;

    final String TYPES = "samples/test/functions_types.zm" ;

    final String DATE = "samples/test/date.zm" ;

    @Test
    public void testBasicFunctions()  {
        InterpreterTest.danceWithScript( BASIC );
    }

    @Test
    public void testExtendedFunctions()  {
        InterpreterTest.danceWithScript( EXTENDED );
    }

    @Test
    public void testThreadFunctions()  {
        InterpreterTest.danceWithScript( THREADING );
    }

    @Test
    public void testProcessFunctions()  {
        // different for Unix and Windows...
        final String osHint = System.getProperty("os.name").toLowerCase() ;
        final String actualPath;
        if ( osHint.contains("win")  ){
            actualPath = PROCESS_WIN ;
        } else if ( osHint.contains("mac")) {
            actualPath = PROCESS_MAC;
        }else {
           actualPath = PROCESS_LINUX ;
        }
        System.out.println("Detected that the process path should be : " + actualPath );
        InterpreterTest.danceWithScript( actualPath );
    }

    @Test
    public void testDBFunctions() {
        // Load out the h2 stuff properly
        InterpreterTest.danceWithScript( DATABASE );
    }

    @Test
    public void testIOFunctions()  {
        InterpreterTest.danceWithScript( IO );
    }

    @Test
    public void testWebFunctions()  {
        InterpreterTest.danceWithScript( WEB );
    }

    @Test
    public void testTypeFunctions()  {
        InterpreterTest.danceWithScript( TYPES );
    }

    @Test
    public void testDateFunctions()  {
        InterpreterTest.danceWithScript( DATE );
    }

    private void threadFunction(ZScript zScript, Map<Integer,Integer> set, int x) {
       zScript.execute(set, x);
    }

    @Test
    public void testThreadingOnZScript() throws Exception {
        Executor executor = Executors.newFixedThreadPool(10);
        ZScript zScript = new ZScript(" #(s,x) = @ARGS ; s[x] = x ;");
        final Map<Integer,Integer> map = new ConcurrentHashMap<>();
        final int size = 10000;
        for ( int i = 0; i < size; i++ ){
            final int x = i ;
            executor.execute(() -> threadFunction( zScript, map, x  ) );
        }
        while ( map.size() != size );
        assertEquals(size, map.size());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testStreaming(){
        int[] a = new int[]{1,2,3};
        ZArray za = new ZArray( a );
        za.stream().forEach(System.out::println);
        ZList zl = new ZList(za);
        zl.stream().forEach(System.out::println);
        ZSet zs = new ZSet(zl);
        zs.stream().forEach(System.out::println);
    }

    List<Object> sel(List<Object> l, Predicate<Object> p){
        return l.stream().filter(p).collect(Collectors.toList());
    }

    List<Object> map(List<Object> l, java.util.function.Function<Object,Object> f){
        return l.stream().map(f).collect(Collectors.toList());
    }

    @Test
    public void testPredicateLambda(){
        ZContext.FunctionContext fc = new ZContext.FunctionContext();
        fc.put("o", this);
        fc.put( "l", Arrays.asList(1,2,3,4));
        ZScript zScript = new ZScript("o.sel(l , where{  $.o < 3 })");
        zScript.runContext(fc);
        Function.MonadicContainer mc =  zScript.execute();
        Assert.assertTrue( mc.value() instanceof List);
        Assert.assertEquals( 2, ((List<?>) mc.value()).size());
    }
    @Test
    public void testMapperLambda(){
        ZContext.FunctionContext fc = new ZContext.FunctionContext( );
        fc.put("o", this);
        fc.put( "l", Arrays.asList(1,2,3,4));
        ZScript zScript = new ZScript("o.map(l , as{  $.o * 2 } )");
        zScript.runContext(fc);
        Function.MonadicContainer mc =  zScript.execute();
        Assert.assertTrue( mc.value() instanceof List);
        Assert.assertEquals( 4, ((List<?>) mc.value()).size());
    }

    @Test
    public void testMapperLambdaAsAnonymous(){
        ZContext.FunctionContext fc = new ZContext.FunctionContext( );
        fc.put("o", this);
        fc.put( "l", Arrays.asList(1,2,3,4));
        ZScript zScript = new ZScript("o.map(l) as{  $.o * 2 }");
        zScript.runContext(fc);
        Function.MonadicContainer mc =  zScript.execute();
        Assert.assertTrue( mc.value() instanceof List);
        Assert.assertEquals( 4, ((List<?>) mc.value()).size());
    }

    @Test
    public void testPredicateLambdaWhereAnonymous(){
        ZContext.FunctionContext fc = new ZContext.FunctionContext();
        fc.put("o", this);
        fc.put( "l", Arrays.asList(1,2,3,4));
        ZScript zScript = new ZScript("o.sel(l) where{  $.o < 3 }");
        zScript.runContext(fc);
        Function.MonadicContainer mc =  zScript.execute();
        Assert.assertTrue( mc.value() instanceof List);
        Assert.assertEquals( 2, ((List<?>) mc.value()).size());
    }

    Runnable createRunnableWithMapContext(ZScript shared, Map<String,Object> ctx, boolean useMap, List<Throwable> errors, int expected){
        return () -> {
            ZContext.FunctionContext fc = new ZContext.FunctionContext(ctx);
            for ( int i=0; i < 10; i++ ){
                Function.MonadicContainer mc;
                if ( useMap ){
                    mc = shared.eval(ctx);
                } else {
                    shared.runContext(fc);
                    mc = shared.execute();
                }
                try {
                    Assert.assertEquals(expected,  mc.value());
                }catch (Throwable t){
                    System.err.println(t);
                    errors.add(t);
                }
            }
            System.out.printf("ctx %s is done with binding %s %n",  ctx, !useMap);
        };
    }

    @Test
    public void multiThreadMultiPlexContextTest() throws Exception{
        ZScript zScript = new ZScript("a + b");
        List<Throwable> errors = Collections.synchronizedList(new ArrayList<>());
        Runnable r1 = createRunnableWithMapContext( zScript, Map.of("a", 2, "b", 3 ), false, errors, 5 );
        Runnable r2 = createRunnableWithMapContext( zScript, Map.of("a", 20, "b", 30 ), false ,errors, 50 );
        Runnable r3 = createRunnableWithMapContext( zScript, Map.of("a", 10, "b", 20 ),false ,errors, 30 );
        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);
        Thread t3 = new Thread(r3);
        t1.start();
        t2.start();
        t3.start();
        Thread.sleep(100);
        Assert.assertTrue(errors.isEmpty());
    }

    @Test
    public void multiThreadMultiPlexBindingsTest() throws Exception{
        ZScript zScript = new ZScript("a + b");
        List<Throwable> errors = Collections.synchronizedList(new ArrayList<>());
        Runnable r1 = createRunnableWithMapContext( zScript, Map.of("a", 2, "b", 3 ),true, errors, 5 );
        Runnable r2 = createRunnableWithMapContext( zScript, Map.of("a", 20, "b", 30 ),true, errors, 50 );
        Runnable r3 = createRunnableWithMapContext( zScript, Map.of("a", 10, "b", 20 ),true, errors, 30 );
        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);
        Thread t3 = new Thread(r3);
        t1.start();
        t2.start();
        t3.start();
        Thread.sleep(100);
        Assert.assertTrue(errors.isEmpty());
    }
}
