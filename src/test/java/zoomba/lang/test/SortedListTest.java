package zoomba.lang.test;

import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.collections.ZSortedList;

import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class SortedListTest {

    final Integer[] ints = new Integer[]{ 100, 2, 4, 5, 1000, 6 , 1, 100, 100, 6, 6 } ;

    @Test
    public void noComparatorTest(){
        ZSortedList<Integer> zsi = new ZSortedList<>();
        zsi.addAll(Arrays.asList(ints));
        Assert.assertEquals( ints.length, zsi.size() );
        Assert.assertEquals( 1, zsi.getFirst().longValue() );
        Assert.assertEquals( 1000, zsi.getLast().longValue() );
        // now remove and check
        zsi.removeFirst();
        Assert.assertEquals( 2, zsi.getFirst().longValue() );
        zsi.removeLast();
        Assert.assertEquals( 100, zsi.getLast().longValue() );
        zsi.removeLast();
        Assert.assertEquals( 100, zsi.getLast().longValue() );
        zsi.removeLast();
        Assert.assertEquals( 100, zsi.getLast().longValue() );
        zsi.removeLast();
        Assert.assertEquals( 6, zsi.getLast().longValue() );
    }

    @Test
    public void withComparatorTest(){
        ZSortedList<Integer> zsi = new ZSortedList<Integer>(Comparator.reverseOrder());
        zsi.addAll(Arrays.asList(ints));
        Assert.assertFalse( zsi.isEmpty() );
        // contains ?
        Arrays.stream(ints).forEach( i -> Assert.assertTrue( zsi.contains(i) ) );
        Assert.assertEquals( ints.length, zsi.size() );
        Assert.assertEquals( 1000, zsi.getFirst().longValue() );
        Assert.assertEquals( 1, zsi.getLast().longValue() );
        Assert.assertTrue( zsi.containsAll( Arrays.asList( 100, 1000)));
        Assert.assertFalse( zsi.containsAll( Arrays.asList( 42, 1000)));

    }

    @Test
    public void removeTest(){
        ZSortedList<Integer> zsi = new ZSortedList<>();
        // check against empty
        zsi.addAll(Arrays.asList(ints));
        zsi.retainAll(Collections.emptyList());
        Assert.assertTrue( zsi.isEmpty() );

        // check against itself
        zsi.clear();
        zsi.addAll(Arrays.asList(ints));
        zsi.retainAll(Arrays.asList(ints));
        Assert.assertEquals( ints.length, zsi.size() );

        // something in between
        zsi.clear();
        zsi.addAll(Arrays.asList(ints));
        zsi.retainAll(Arrays.asList(6,6, 100, 42 ));
        Assert.assertEquals( 3, zsi.size() );

        // now remove all
        zsi.clear();
        zsi.addAll(Arrays.asList(ints));
        Assert.assertTrue( zsi.removeAll(Arrays.asList(ints)) );
        Assert.assertTrue( zsi.isEmpty() );

        // again
        zsi.clear();
        zsi.addAll(Arrays.asList(ints));
        Assert.assertFalse( zsi.removeAll(Arrays.asList(2,4,6, 10)) );
        Assert.assertEquals( ints.length- 3, zsi.size() );
    }

    @Test
    public void getElementTest(){
        List<Integer> list = Arrays.asList(ints) ;
        ZSortedList<Integer> zsi = new ZSortedList<>( list );
        list.sort( Comparator.naturalOrder() );
        IntStream.range(0, ints.length ).forEach( inx -> {
            Integer act = zsi.get(inx);
            Integer exp = list.get(inx);
            Assert.assertEquals( exp, act );
        });
    }

    @Test
    public void findElementTest(){
        ZSortedList<Integer> zsi = new ZSortedList<>( Arrays.asList(ints)  );
        Assert.assertEquals(0, zsi.indexOf(1));
        Assert.assertEquals(4, zsi.indexOf(6));
        Assert.assertEquals( -1, zsi.indexOf(5000) );
        // now the reverse
        Assert.assertEquals(0, zsi.lastIndexOf(1));
        Assert.assertEquals(6, zsi.lastIndexOf(6));
        Assert.assertEquals( -1, zsi.lastIndexOf(-30) );
    }

    @Test
    public void iteratorTest(){
        ZSortedList<Integer> zsi = new ZSortedList<>( Arrays.asList(ints) );
        Iterator<Integer> it = zsi.iterator();
        Assert.assertTrue( it instanceof  ListIterator);
        ListIterator<Integer> li = zsi.listIterator();
        Assert.assertTrue( li.hasNext() );
        Assert.assertEquals( 1, li.next().intValue() );
        // indexed
        li = zsi.listIterator(0);
        Assert.assertTrue( li.hasNext() );
        Assert.assertEquals( 1, li.next().intValue() );
        // offset and check
        li = zsi.listIterator(1);
        Assert.assertTrue( li.hasNext() );
        Assert.assertEquals( 2, li.next().intValue() );
    }

    @Test
    public void performanceTest(){
        final int n = 500;  // default value
        final int times = 10000;
        final Random r = new SecureRandom();
        List<Integer> list = IntStream.range(0,n).map(x -> r.nextInt(10000) ).boxed().collect(Collectors.toList());
        // for zoomba SortedList
        long start = System.currentTimeMillis();
        IntStream.range(0,times).forEach( (x)->{
            ZSortedList<Integer>  heap = new ZSortedList();
            heap.addAll(list);
        });
        long delta1 = System.currentTimeMillis() - start;
        // for Java PriorityQueue
        start = System.currentTimeMillis();
        IntStream.range(0,times).forEach( (x)->{
            PriorityQueue<Integer> heap = new PriorityQueue<>(n);
            heap.addAll(list);
        });
        long delta2 = System.currentTimeMillis() - start;
        long f = delta1 / delta2 ;
        final long limit = 15;
        System.out.printf("Sorted List took %d ms while PriorityQueue took %d ms, %d is the fraction with limit %d", delta1, delta2, f, limit);
        Assert.assertTrue( f <= limit );
    }
}
