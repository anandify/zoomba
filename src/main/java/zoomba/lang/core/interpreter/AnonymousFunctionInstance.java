/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.interpreter;

import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZJVMAccess;
import zoomba.lang.core.sys.ZThread;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.parser.ASTBlock;

import java.lang.reflect.Array;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * The class that is responsible for all the anonymous hocus pocus
 * Semi concrete implementation of the Function interface, which is simpler than Java and works well.
 * I sincerely have no idea why Java 8 Functional interface is so so so bloated out of proportion
 * The type guys needs to have their head checked
 */
public abstract class AnonymousFunctionInstance implements Function, Comparator {

    /**
     * Any argument passed into the function is abstracted as instance of this class
     * This is inherently non thread safe, because they are not meant to be invoked cross thread
     */
    public static final class AnonymousArg implements Map {

        static final Set<String> KEY_SET = new HashSet<>();

        static {
            KEY_SET.addAll( Arrays.asList( "$" , "parent", "i", "index" , "o" , "object" , "item" ,
                    "c", "context" , "p", "partial" , "previous", "prev" ,"seed",
                    "left" , "right" , "l", "r", "key", "value", "k", "v", "t", "tuple" ) );
        }

        Object $;

        Number i;

        Object o;

        Object c;

        Object p;

        /**
         * Create an instance
         */
        public AnonymousArg(){
            setArgs();
        }

        /**
         * Creating instances are costly, so we reuse - do much more memory optimization
         * @param args the parameters
         *             args[0] is the index of iteration - can be any number. Ints are bad,
         *             because we may want to iterate for a very long time ...
         *             args[1] Object in question - the item for the iteration
         *             args[2] context of the object, the container so to speak if we iterate over a collection
         *             args[3] partial result of the computation, for example seed for a fold
         *
         */
        public void setArgs(Object... args) {
            i = -1;
            o = NIL;
            c = NIL;
            p = NIL;

            if  ( args.length == 0 ){
                return;
            }
            if  ( args.length == 1 ){
                o = args[0];
                return;
            }
            if  ( args.length == 2 ){
                i = (Number) args[0];
                o = args[1] ;
                return;
            }

            if  ( args.length == 3 ){
                i = (Number) args[0];
                o = args[1] ;
                p = args[2];
                return;
            }

            i = (Number) args[0];
            o = args[1] ;
            c = args[2] ;
            p = args[3];
        }

        /**
         * Creates an ArgContext from parent context
         * @param parent the parent context
         * @return an ArgContext
         */
        public ZContext.ArgContext argContext(ZContext parent){
            if ( parent.has( THIS ) ){
                this.$ = parent.get(THIS).value();
            }else{
                // absolutely necessary
                this.$ = NIL ;
            }
            return new ZContext.ArgContext(
                    Collections.singletonList(THIS),
                    Collections.singletonList(new MonadicContainerBase(this)));
        }

        @Override
        public int size() {
            return 5;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean containsKey(Object key) {
            return KEY_SET.contains(key) ||
                    key instanceof Integer ||
                    (o instanceof Map && ((Map<?,?>) o).containsKey(key));
        }

        @Override
        public boolean containsValue(Object value) {
            return values().contains( value );
        }

        @Override
        public Object get(Object key) {
            if ( key instanceof Integer ){
                // becomes a convenience for join
                if ( o.getClass().isArray() ){ return Array.get(o,(int)key); }
                if ( o instanceof List ){ return ((List<?>)o).get((int)key); }
            }
            switch ( String.valueOf(key) ){
                case "$":
                case "parent" :
                    return $;
                case "i" :
                case "index":
                    return i;
                case "o" :
                case "object":
                case "item" :
                    return o;
                case "c" :
                case "context":
                    return c;
                case "p":
                case "partial":
                case "previous" :
                case "prev":
                case "seed":
                    return p;
                case "left":
                case "l":
                    if ( o instanceof Object[] ){ return ((Object[]) o)[0]; }
                    if ( o instanceof List ){ return ((List<?>) o).get(0); }
                    return NIL;
                case "right":
                case "r":
                    if ( o instanceof Object[] ){ return ((Object[]) o)[1]; }
                    if ( o instanceof List ){ return ((List<?>) o).get(1); }
                    return NIL;
                case "k":
                case "key":
                    if ( o instanceof Map.Entry ) return ((Map.Entry<?,?>)o).getKey();
                    return NIL;
                case "v":
                case "value":
                    if ( o instanceof Map.Entry ) return ((Map.Entry<?,?>)o).getValue();
                    return NIL;
                case "t":
                case "tuple":
                    if ( o instanceof Object[] || o instanceof List ){ return o;}
                    return NIL;
                default:
                    MonadicContainer mc = ZJVMAccess.getProperty(o,key);
                    if ( !mc.isNil() ) return mc.value();
                    return NIL;
            }
        }

        @Override
        public Object put(Object key, Object value) {
            switch ( String.valueOf(key) ){
                case "$":
                case "parent" :
                    return $ = value ;
                case "i" :
                case "index":
                    return i = (Number) value ;
                case "o" :
                case "object":
                case "item" :
                    return o = value;
                case "c" :
                case "context":
                    return c = value ;
                case "p":
                case "partial":
                case "previous" :
                case "prev":
                case "seed":
                    return p = value ;

            }
            return NIL;
        }

        @Override
        public Object remove(Object key) {
            return NIL;
        }

        @Override
        public void putAll(Map m) {

        }

        @Override
        public void clear() {
        }

        @Override
        public Set<?> keySet() {
            return  KEY_SET ;
        }

        @Override
        public Collection<?> values() {
            return Arrays.asList( $ , i, o, c, p ) ;
        }

        @Override
        public Set<Entry<?,?>> entrySet() {
            Set<Entry<?,?>> entries = new HashSet<>();
            for ( Object key : KEY_SET ){
                entries.add(  new ZMap.Pair( key, get(key) ) );
            }
            return entries ;
        }
    }


    /**
     * The block of method - the anonymous function block
     */
    public final ASTBlock methodBlock;

    /**
     * Thread specific Interpreter
     */
    public final ThreadLocal<ZInterpret> tlZInterpret;

    /**
     * Thread specific AnonymousArg
     */
    public final ThreadLocal<AnonymousArg> tlAnonymousArg;

    /**
     * Thread specific Function Context
     */
    public final ThreadLocal<ZContext.FunctionContext> tlRunContext;


    @Override
    public void runContext(ZContext context){
        if ( context instanceof ZContext.FunctionContext) {
            // need to do something here, we shall see..
            ZContext.FunctionContext current = tlRunContext.get();
            current.map.clear();;
            current.map.putAll(((ZContext.FunctionContext) context).map);
        }
    }

    @Override
    public ZContext runContext() { return tlRunContext.get(); }

    /**
     * Creates an anonymous function instance
     * @param i the interpreter
     * @param m the method block
     */
    public AnonymousFunctionInstance(ZInterpret i, ASTBlock m) {
        tlZInterpret = ThreadLocal.withInitial( () -> new ZInterpret(i));
        tlAnonymousArg = ThreadLocal.withInitial(AnonymousArg::new);
        methodBlock = m;
        ZContext parent = i.frames.peek();
        tlRunContext = ThreadLocal.withInitial( () ->  new ZContext.FunctionContext(parent,null) );
    }

    @Override
    public MonadicContainer execute(Object... args) {
        ZInterpret methodInterpreter = tlZInterpret.get() ;
        AnonymousArg anonymousArg = tlAnonymousArg.get();
        ZContext.FunctionContext myContext = tlRunContext.get();
        try {
            // set args
            anonymousArg.setArgs(args);
            myContext.argContext =  anonymousArg.argContext( myContext.parent );
            methodInterpreter.prepareCall(myContext);
            Object r = methodBlock.jjtAccept(methodInterpreter, null);
            return new MonadicContainerBase(r);

        } catch (ZException.MonadicException me) {
            ZThread.appendError(new ArrayList<>(methodInterpreter.callStack));
            return me ;
        }
        finally {
            methodInterpreter.endCall();
            myContext.argContext.clear();
        }
    }

    @Override
    public String body() {
        return methodBlock.body(tlZInterpret.get().danceMaster.body);
    }

    @Override
    public int compare(Object o1, Object o2) {
        MonadicContainer mc = execute(-1, new Object[]{o1,o2},NIL,NIL);
        if ( mc.isNil() ) throw new RuntimeException("Comparator must return a value!");
        Object o = mc.value();
        Number cmp = ZNumber.number( o );
        if ( cmp != null ) return cmp.intValue();
        Boolean b = ZTypes.bool( o );
        if ( b != null ){ return b ? -1 : 1 ; }
        throw new RuntimeException("Comparator must return an integer or boolean!");
    }

    @Override
    public String toString() {
        return  String.format("%s --> %s", methodBlock.locationInfo(), body());
    }

    /**
     * A mapper lambda abstraction over anonymous function
     * fold ( ... ) as {  }
     */
    public static final class MapperLambda extends AnonymousFunctionInstance implements Mapper {

        /**
         * Creates a MapperLambda from
         * @param i the interpreter
         * @param m the method block
         */
        public MapperLambda(ZInterpret i, ASTBlock m) {
            super(i, m);
        }

        @Override
        public String name() {
            return "__map__";
        }

        @Override
        public Object map(Object... args) {
            MonadicContainer mc = execute(args);
            if (mc.isNil()) {
                return tlAnonymousArg.get().o;
            }
            return mc.value();
        }
    }

    /**
     * A Predicate lambda abstraction over anonymous function
     * join ( ... ) ( where | :: ) {  }
     */
    public static final class PredicateLambda extends AnonymousFunctionInstance implements Predicate {

        /**
         * Creates a PredicateLambda from
         * @param i the interpreter
         * @param m the method block
         */
        public PredicateLambda(ZInterpret i, ASTBlock m) {
            super(i, m);
        }

        @Override
        public String name() {
            return "__select__";
        }

        @Override
        public boolean accept(Object... args) {
            MonadicContainer mc = execute(args);
            return ZTypes.bool(mc.value(), false);
        }
    }

    @Override
    public  <T> Consumer<T> consumer(Object context){
        return  (value) ->{
            try {
                execute( -1, value, context, Function.NIL );
            }catch (Throwable ignore){}
        };
    }

    @Override
    public <U,T> BiConsumer<U,T> biConsumer(Object context){
        return  (left,right) ->{
            try {
                execute( -1, right, context, left );
            }catch (Throwable ignore){}
        };
    }
}
