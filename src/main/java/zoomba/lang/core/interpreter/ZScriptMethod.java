/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.interpreter;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZSet;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZEventAware;
import zoomba.lang.core.sys.ZThread;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.parser.*;
import zoomba.lang.core.interpreter.ZContext.*;

import static zoomba.lang.core.types.ZException.Break.BREAK;
import static zoomba.lang.core.types.ZException.Return.RETURN;

import java.util.*;

/**
 * The Script Method Infrastructure
 */
public class ZScriptMethod {

    /**
     * The arguments variable
     */
    public static final String ARGS_VAR = "@ARGS";

    /**
     * Name of the Method if any
     */
    public final String name;

    /**
     * The DOM Method Node
     */
    public final Node methodNode;

    /**
     * The actual MethodBody - or block
     */
    public final Node block;

    /**
     * The parameter node
     */
    public final Node paramsNode;

    /**
     * Is the function var = def(){} form, is it anonymous
     */
    public final boolean isAnonymous;

    /**
     * Is the method defined as a variable
     */
    public final boolean isVar;

    /**
     * If the method is nested, inner method, stores the outer context
     */
    public final ZContext outer;

    /**
     * The before event handlers
     */
    public final Set<ZScriptMethod> before = new ZSet();

    /**
     * The after event handlers
     */
    public final Set<ZScriptMethod> after = new ZSet();


    /**
     * Underlying ZScript which has the method
     */
    public final ZScript script;

    /*
    private boolean isRecursive;

    private boolean isTailRecursive;

    public void findInnerMethodCalls(Node node, List<ASTMethodNode> calls){
        if ( node instanceof ASTMethodNode ){
            calls.add( (ASTMethodNode)node );
        }
        int num = node.jjtGetNumChildren();
        for ( int i = 0 ; i < num; i++ ){
            findInnerMethodCalls( node.jjtGetChild(i), calls );
        }
    }

    private void setRecursive(){
        if ( name.isEmpty() ){
            isTailRecursive = false ;
            isRecursive = false ;
            return;
        }
        List<ASTMethodNode> calls = new ArrayList<>();
        findInnerMethodCalls( block , calls );
        boolean rec = false ;
        boolean tailRec = false ;
        for ( int i = calls.size()-1 ; i >= 0 ; i-- ){
            rec =  name.equals( ( (ZoombaNode)calls.get(i).jjtGetChild(0)).image );
            if ( tailRec ){
                tailRec = false ;
                break;
            }
            tailRec = rec ;
        }
        isRecursive = rec ;
        isTailRecursive = tailRec ;
    }
    */

    /**
     * Creates a method from a DOM node
     *
     * @param parent the script where I am defined
     * @param node   the method node
     */
    public ZScriptMethod(ZScript parent, Node node) {
        this(parent, node, null);
    }

    private void uniqueParams(Node node) {
        int num = node.jjtGetNumChildren();
        HashSet s = new HashSet();
        for (int i = 0; i < num; i++) {
            ZoombaNode n = ((ASTIdentifier) node.jjtGetChild(i).jjtGetChild(0));
            String name = n.image;
            if (s.contains(name)) {
                throw new UnsupportedOperationException(
                        "Non Unique parameters to function! " + n.locationInfo());
            }
            s.add(name);
        }
        s.clear();
    }

    private int cache = -1;


    static class EquitableArray{

        public final Object[] array;

        EquitableArray( Object[] array){
            this.array = array;
        }

        public static EquitableArray from( Object o){
            return new EquitableArray( (Object[]) o);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            EquitableArray that = (EquitableArray) o;
            return Arrays.equals(array, that.array);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(array);
        }
    }

    private Map<EquitableArray, Function.MonadicContainer> lru = Collections.emptyMap();

    /**
     * Set up the LRU cache
     * Specify "all" to cache everything
     * Else specify any positive integer
     * @param max maximum size of the cache
     */
    public void cache( Object max){
        final int limit;
        if ( max.equals("all")){
            limit = Integer.MAX_VALUE;
        } else {
            limit = ZNumber.integer(max,-1).intValue();
        }

        if ( limit <= 0 ) return;
        cache = limit;
        lru = new LinkedHashMap(){
            @Override
            protected boolean removeEldestEntry(Map.Entry eldest) {
                return size() > limit;
            }
        };
    }

    /**
     * Gets the size of the Cache
     * @return size of the cache
     */
    public int cache() { return cache; }

    /**
     * Creates a nested inner function
     *
     * @param parent who is the script that is my parent
     * @param node   the node
     * @param outer  the outer context ( frozen )
     */
    public ZScriptMethod(ZScript parent, Node node, ZContext outer) {
        script = parent;
        methodNode = node;
        this.outer = outer;
        if (methodNode instanceof ASTMethodDef) {
            Node firstChild = methodNode.jjtGetChild(0);
            if (firstChild instanceof ASTIdentifier) {
                name = ((ASTIdentifier) firstChild).image;
                isVar = false;
            } else {
                name = "";
                isVar = true;
            }
            isAnonymous = false;
        } else {
            isVar = false;
            name = "";
            isAnonymous = true;
            // anonymous
        }
        int num = methodNode.jjtGetNumChildren();
        block = methodNode.jjtGetChild(num - 1);
        paramsNode = methodNode.jjtGetChild(num - 2);
        uniqueParams(paramsNode);
        //setRecursive();
    }

    /**
     * Creates an instance of the ZScriptMethodInstance
     * @param interpret using this interpreter
     * @param instance underlying object if any
     * @return a ZScriptMethodInstance
     */
    public ZScriptMethodInstance instance(ZInterpret interpret, Object instance) {
        return new ZScriptMethodInstance(interpret, instance);
    }

    /**
     * Creates an instance of the ZScriptMethodInstance
     * @param interpret using this interpreter
     * @return a ZScriptMethodInstance
     */
    public ZScriptMethodInstance instance(ZInterpret interpret) {
        return new ZScriptMethodInstance(interpret);
    }

    @Override
    public String toString() {
        return String.format("%s --> %s",
                ((ZoombaNode) methodNode).locationInfo(),
                ((ZoombaNode) methodNode).body(script.body));
    }

    /**
     * Implementation of a MethodInstance
     */
    public final class ZScriptMethodInstance implements Function, ZEventAware, Iterator {

        @Override
        public void after(ZEventArg a) {
            if ( cache > 0 ){
                EquitableArray ea= EquitableArray.from(a.args.items);
                if ( !lru.containsKey(ea)) { lru.put( ea, a.result); }
            }
            for (ZScriptMethod zsm : after) {
                try {
                    ZScriptMethodInstance i = zsm.instance(this.interpret);
                    i.execute(a);
                } catch (Throwable t) {

                }
            }
        }

        @Override
        public void before(ZEventArg b) {
            for (ZScriptMethod zsm : before) {
                try {
                    ZScriptMethodInstance i = zsm.instance(this.interpret);
                    i.execute(b);
                } catch (Throwable t) {
                }
            }
        }

        /**
         * The interpreter to execute this instance
         */
        public final ZInterpret interpret;

        /**
         * Create an instance using
         *
         * @param interpret instance of interpreter
         */
        public ZScriptMethodInstance(ZInterpret interpret) {
            this(interpret, NIL);
        }

        /**
         * Create an instance using
         *
         * @param interpret an instance of interpreter
         * @param instance  with a object instance for $
         */
        public ZScriptMethodInstance(ZInterpret interpret, Object instance) {
            this.interpret = interpret;
            this.instance = instance;
        }

        ArgContext getArgs(Object... args) {
            // now only positional
            ArgContext ctx = (ArgContext) paramsNode.jjtAccept(interpret, args);
            if (NIL != instance) {
                if (ctx == ArgContext.EMPTY_ARGS_CONTEXT) {
                    // albeit I am given an empty arg context,
                    // there is instance, so can not re-use empty context, create one
                    // for it, else there will be chaos
                    ctx = new ArgContext(ZArray.EMPTY_ARRAY);
                }
                ctx.setInstance(instance);
            }
            return ctx;
        }

        @Override
        public String body() {
            return ((ZoombaNode) methodNode).body(ZScriptMethod.this.script.body);
        }

        Object instance = NIL;

        @Override
        public MonadicContainer execute(Object... args) {
            before(new ZEventArg(this, When.BEFORE, this, args));
            ZContext.ArgContext argContext = getArgs(args);
            if ( cache > 0 ){
                EquitableArray ea= EquitableArray.from(argContext.values);
                if ( lru.containsKey(ea)) {
                    MonadicContainer result = lru.get(ea);
                    after(new ZEventArg(this, When.AFTER, this, args, result, RETURN));
                    return result;
                }
            }
            ZContext parent;
            if (outer == null) {
                if (interpret.frames.empty()) {
                    parent = ZContext.EMPTY_CONTEXT;
                } else {
                    parent = interpret.frames.peek();
                }
            } else {
                parent = outer;
            }
            ZInterpret methodInterpreter = interpret;
            ZContext.FunctionContext functionContext = null;
            if (ZScriptMethod.this.script.runContext() != null) {
                functionContext = new ZContext.FunctionContext(ZScriptMethod.this.script.runContext(), argContext);
            } else {
                functionContext = new ZContext.FunctionContext(parent, argContext);
            }
            // thread mismatch...
            boolean threaded = (interpret.threadId != Thread.currentThread().getId());
            if (threaded) {
                methodInterpreter = new ZInterpret(interpret);
            }
            methodInterpreter.prepareCall(functionContext);
            Throwable error = RETURN;
            MonadicContainer result = NOTHING;
            try {
                // so that functions know who they are and where they are from
                functionContext.set(ME, ZScriptMethod.this );
                // #89, fix the import context
                FunctionContext importContext = ZScriptMethod.this.script.importContext() ;
                if ( importContext != null ){
                    functionContext.putAll( importContext.map );
                }
                Object r = block.jjtAccept(methodInterpreter, null);
                result = new MonadicContainerBase(r);
                return result;
            } catch (Throwable t) {
                if (t instanceof ZException.Return) {
                    result = new MonadicContainerBase(((ZException.Return) t).value);
                    return result;
                }
                error = t; // set it up
                if (t instanceof ZException.MonadicException) {
                    return (ZException.MonadicException) t;
                }
                ZThread.appendError(new ArrayList<>(methodInterpreter.callStack));
                throw t; // throw it up
            } finally {
                methodInterpreter.endCall();
                // adjust for this --> error
                after(new ZEventArg(this, When.AFTER, this, args, result, error));
            }
        }

        @Override
        public String name() {
            return name;
        }

        private Object iteratorValue;

        @Override
        public boolean hasNext() {
            iteratorValue = NIL;
            Function.MonadicContainer mc = execute(ZArray.EMPTY_ARRAY);
            if (mc == BREAK) {
                return false;
            }
            iteratorValue = mc.value();
            return true;
        }

        @Override
        public Object next() {
            return iteratorValue;
        }


        @Override
        public String toString() {
            return String.format("=> %s => %s => %n%s",
                    script.location(),
                    ((ZoombaNode) methodNode).locationInfo(), body());
        }
    }
}
