/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.interpreter;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Pointer;
import org.apache.commons.jxpath.ri.model.NodePointer;
import zoomba.lang.core.collections.*;
import zoomba.lang.core.io.ZDataBase;
import zoomba.lang.core.io.ZWeb;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.operations.ZMatrix;
import zoomba.lang.core.operations.ZRandom;
import zoomba.lang.core.sys.ZProcess;
import zoomba.lang.core.sys.ZThread;
import zoomba.lang.core.types.*;
import zoomba.lang.parser.ASTArgDef;
import zoomba.lang.parser.ZoombaNode;
import zoomba.lang.core.io.ZFileSystem;

import java.io.File;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static zoomba.lang.core.operations.Function.*;
import static zoomba.lang.core.types.ZTypes.bool;

/**
 * The Interceptor for all the fancy methods
 */
public interface ZMethodInterceptor {

    /**
     * Intercept and execute standard utility methods
     * @param context for the execution
     * @param interpret using this interpreter
     * @param methodName name of the method
     * @param args arguments for the method
     * @param anons list of anonymous functions
     * @param callNode underlying actual node where the call was generated
     * @return a MonadicContainer as result
     */
    Function.MonadicContainer intercept(Object context, ZInterpret interpret,
                                        String methodName,
                                        Object[] args,
                                        List<Function> anons,
                                        ZoombaNode callNode);

    /**
     * In case the interception failed, we should use this instance to showcase we could not intercept the call
     */
    Function.MonadicContainer UNSUCCESSFUL_INTERCEPT = new Function.MonadicContainerBase();

    /**
     * The default interceptor
     */
    Default DEFAULT_INTERCEPTOR = new Default();

    /**
     * A default implementation of the ZMethodInterceptor
     */
    final class Default implements ZMethodInterceptor {

        static final String ASYNC = "async";

        static final String TASK = "task";

        static final String V_TASK = "job";

        static final String SCHEDULE = "batch";

        static final String BINARY_SEARCH = "bsearch";

        static final String POLL = "poll";

        static final String PRINTF = "printf";

        static final String PRINTLN = "println";

        static final String EPRINTF = "eprintf";

        static final String EPRINTLN = "eprintln";

        static final String SIZE = "size";

        static final String EMPTY = "empty";

        static final String LIST = "list";

        static final String SORTED_LIST = "slist";

        static final String SET = "set";

        static final String SORTED_SET = "sset";

        static final String ORDERED_SET = "oset";

        static final String MSET = "mset";

        static final String GROUP_BY = "group";

        static final String DICT = "dict";

        static final String SORTED_DICT = "sdict";

        static final String ORDERED_DICT = "odict";

        static final String SELECT = "select";

        static final String PARTITION = "partition";

        static final String JOIN = "join";

        static final String FOLD = "fold";

        static final String LFOLD = "lfold";

        static final String RFOLD = "rfold";

        static final String LREDUCE = "reduce";

        static final String RREDUCE = "rreduce";

        static final String FIND = "find";

        static final String LINDEX = "index";

        static final String EXISTS = "exists";

        static final String RINDEX = "rindex";

        static final String INT = "int";

        static final String FLOAT = "float";

        static final String INT_L = "INT";

        static final String FLOAT_L = "FLOAT";

        static final String NUM = "num";

        static final String BOOL = "bool";

        static final String TIME = "time";

        static final String ROUND = "round";

        static final String FLOOR = "floor";

        static final String CEIL = "ceil";

        static final String LOG = "log";

        static final String STRING = "str";

        static final String JSON_STRING = "jstr";

        static final String ENUM = "enum";

        static final String THREAD = "thread";

        /*
        https://cr.openjdk.org/~rpressler/loom/Loom-Proposal.html
        * */
        static final String V_THREAD = "fiber";

        static final String JSON = "json";

        static final String XML = "xml";

        static final String OPEN = "open";

        static final String FILE = "file";

        static final String READ = "read";

        static final String WRITE = "write";

        static final String RANDOM = "random";

        static final String SHUFFLE = "shuffle";

        static final String SORTA = "sorta";

        static final String SORTD = "sortd";

        static final String SUM = "sum";

        static final String MINMAX = "minmax";

        static final String SYSTEM = "system";

        static final String POPEN = "popen";

        static final String MATRIX = "matrix";

        static final String BYE = "bye";

        static final String RAISE = "raise";

        static final String LOAD = "load";

        static final String FROM = "from";

        static final String CONCURRENT = "concur" ;

        static final String HEAP = "heap";

        static final String HASH = "hash";

        static final String TOKENS = "tokens";

        static final String SEQUENCE = "seq";

        static final String SUB_SEQUENCES = "sequences";

        static final String COMBINATION = "comb";

        static final String PERMUTATION = "perm";

        static final String TUPLE = "tuple";

        static final String YAML = "yaml";

        static final String YSTR = "ystr";

        static final String SIGN = "sign";

        static final String PRIORITY_QUEUE = "pqueue";

        static final String JX_PATH = "xpath";

        static final String JX_ELEMENT = "xelem";

        private Default() {
        }

        static final Pattern NAME_PATTERN = Pattern.compile("\\[@name='(?<n>[^']+)'\\]\\[(?<i>[\\d]+)\\]" , Pattern.MULTILINE );

        /**
         * There is a bug in the XPath system - sometimes the path asPath() does not evaluate right.
         * As we can see, if it can find the pointer, it would return asPath() or, it sanitizes on it's own
         * @param p node to gather the right path
         * @return a sanitized  path
         */
        public static String myPath( Pointer p){
            String path = p.asPath();
            JXPathContext rc = JXPathContext.newContext(p.getRootNode());
            NodePointer rec = (NodePointer)rc.getPointer(path);
            if ( rec.isActual() ) {
                return path;
            }
            else{
                String r = path.substring(2);
                Matcher m = NAME_PATTERN.matcher(r);
                StringBuilder buf = new StringBuilder();
                while (m.find()) {
                    String nodeName = m.group("n");
                    String inxName = m.group("i");
                    buf.append("/").append( nodeName ).append("[").append(inxName).append("]");
                }
                return buf.toString();
            }
        }

        /**
         * Extracts any arbitrary sub object
         * based on xpath (using apache JXPath library ) from the argument object
         * @param args arguments, defined as below
         *          args[0] is the source object
         *          args[1] is the xpath expression
         *          args[2] when true tells the result would be a collection, instead of first match
         * @return result of the xpath expression applied over the source object
         *          either first match, or when args[2] is true then a collection of values which matches the xpath
         */
        public static Object jxPath(Object... args) {
            if (args.length < 2) return null;
            JXPathContext context;
            String myPath = "" ;
            if ( args[0] instanceof Pointer ){
                // treat differently...
                Pointer p = (Pointer) args[0];
                myPath = myPath(p);
                context = JXPathContext.newContext(p.getRootNode());
            } else {
                context = JXPathContext.newContext(args[0]);
            }

            String path = String.valueOf(args[1]);
            myPath = myPath.isEmpty()? path : myPath + "/" + path;

            if (args.length > 2) {
                if (bool(args[2], false)) {
                    Iterator iterable = context.iterate(myPath);
                    return new ZList(iterable);
                }
            }
            return context.getValue(myPath);
        }

        /**
         * Extracts any arbitrary sub object Pointer
         * based on xpath (using apache JXPath library ) from the argument object
         * Advantage of the Pointer is it still knows the context in the source object,
         * so one can find children or parent. See org.apache.commons.jxpath.Pointer for further details
         * @param args arguments, defined as below
         *          args[0] is the source object
         *          args[1] is the xpath expression
         *          args[2] when true tells the result would be a collection, instead of first match
         * @return A Pointer result of the xpath expression applied over the source object
         *          either first match, or when args[2] is true then a collection of Pointers which matches the xpath
         */
        public static Object jxElement(Object... args) {
            if (args.length < 2) return null;
            JXPathContext context;
            String myPath = "" ;
            if ( args[0] instanceof Pointer ){
                // treat differently...
                Pointer p = (Pointer) args[0];
                myPath = myPath(p);
                context = JXPathContext.newContext(p.getRootNode());
            } else {
                context = JXPathContext.newContext(args[0]);
            }

            String path = String.valueOf(args[1]);
            myPath = myPath.isEmpty()? path : myPath + "/" + path;

            if (args.length > 2) {
                if (bool(args[2], false)) {
                    Iterator iterable = context.iteratePointers(myPath);
                    return new ZList(iterable);
                }
            }
            return context.getPointer(myPath);
        }

        static Object from(List<Function> anons, Object... args) {
            if (args.length > 1 && args[1] instanceof Collection) {
                return BaseZCollection.compose(args[0], (Collection) args[1], anons, 1);
            }
            if (args.length > 1 && args[1] instanceof Map ) {
                ZMap zm = new ZMap((Map)args[1]);
                BaseZCollection.compose(args[0], zm.collection(false), anons, 1);
                return zm;
            }
            if (args.length > 0) {
                return BaseZCollection.compose(args[0], null, anons, 1);
            }
            return Collections.emptyList();
        }

        static Object convertNoDoubleWrapSynchronized(Object o){
            if ( o == null ) return Collections.EMPTY_LIST;
            // There is really no other way of knowing, hence...
            final String syncMather = "java.util.Collections$Synchronized" ;
            final String myClass = o.getClass().getName();
            if ( myClass.startsWith(syncMather)) return o;
            if ( o instanceof Map ) return Collections.synchronizedMap((Map<?,?>)o);
            if ( o instanceof List ) return Collections.synchronizedList((List<?>)o);
            if ( o instanceof Set ) return Collections.synchronizedSet((Set<?>)o);
            if ( o instanceof Collection ) return Collections.synchronizedCollection((Collection<?>)o);
            return Collections.EMPTY_LIST;
        }

        static Object concurrent(List<Function> anons, Object... args) {
            if ( args.length == 0 ){
                return Collections.EMPTY_LIST;
            }
            // concur(collection)
            if ( args.length == 1 ){
                // when no functions are passed, concur Must return Synchronized Collection
                if ( anons.isEmpty() ){
                    return convertNoDoubleWrapSynchronized(args[0]);
                }
                return BaseZCollection.compose(args[0], null, anons, 0);
            }
            if ( args.length == 2 ){
                // concur(collection, into_collection)
                if ( args[1] instanceof Collection ){
                    return BaseZCollection.compose(args[0], (Collection) args[1], anons, 0);
                }
                if ( args[1] instanceof Map ){
                    ZMap zm = new ZMap((Map)args[1]);
                    BaseZCollection.compose(args[0], zm.collection(true), anons, 0);
                    return zm; // special casing
                }
                // concur(collection, numThreads)
                return BaseZCollection.compose(args[0], null, anons, ZNumber.integer(args[1], 0).intValue());
            }
            // concur(collection, into, numThreads)
            if ( args[1] instanceof Map ){
                ZMap zm = new ZMap((Map)args[1]);
                BaseZCollection.compose(args[0], zm.collection(true), anons, ZNumber.integer(args[2], 0).intValue());
                return zm;
            }
            return BaseZCollection.compose(args[0], (Collection) args[1], anons, ZNumber.integer(args[2], 0).intValue());
        }

        /**
         * Reads from the args support file as well as url reading
         * @param args bunch of arguments
         * @return result of the reading
         */
        public static Object read(Object... args) {
            if (args.length == 0) { // in case no args, reads from stdin
               Scanner sc = new Scanner(System.in);
               if ( sc.hasNext() ){
                   return sc.next();
               }
               return NIL;
            }
            String path;
            NamedArgs params = null;
            if ( args[0] instanceof Map ){
                params = NamedArgs.fromMap( (Map)args[0] );
                path = params.typed("path","");
            } else {
                path = String.valueOf(args[0]);
            }

            if (path.contains(":/") && path.startsWith("http")) {
                // web protocol
                int conTO = Integer.MAX_VALUE;
                int readTO = Integer.MAX_VALUE;
                Map<String,String> headers = Collections.emptyMap();
                if (args.length > 1) {
                    conTO = (int) ZNumber.integer(args[1], conTO);
                    if (args.length > 2) {
                        readTO = (int) ZNumber.integer(args[1], readTO);
                        if (args.length > 3) {
                            headers = (Map)args[2];
                        }
                    }
                }
                if ( params != null ){
                    conTO = params.typed( "conTo", conTO);
                    readTO = params.typed( "readTo", readTO);
                    headers = params.typed( "headers", headers );
                }

                try {
                    return ZWeb.readUrl(path, headers, conTO, readTO);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            try {
                return ZFileSystem.read(args);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        static Object open(Object... args) {
            if (args.length == 0) return NIL;
            String path = String.valueOf(args[0]);
            if (path.toLowerCase().contains("db:")) {
                String dbLoc = path.replaceAll("[dD][bB]\\:", "");
                return new ZDataBase(dbLoc);
            }
            if (path.contains(":/") && path.startsWith("http")) {
                // web protocol
                return new ZWeb(path);
            }
            // file later...
            return ZFileSystem.open(args);
        }

        static Thread thread(boolean virtual, boolean startNow, Function anon, Object... args) {
            if (args.length == 0) {
                if (anon == null) return Thread.currentThread();
            }
            if (anon != null) {
                return new ZThread(anon, args).thread(virtual, startNow);
            } else {
                if ( args[0] instanceof Function ) {
                    Function f = (Function)args[0];
                    args = ZTypes.shiftArgsLeft(args);
                    return new ZThread(f, args).thread(virtual, startNow);
                }
            }
            throw new UnsupportedOperationException("Can not create thread from these args!");
        }

        static Object schedule(ZInterpret interpret, Function anon,  Object... args){
            if ( args.length == 0 ) return  ZThread.getExecutor(0) ;
            if ( args[0] instanceof Integer && args.length == 1 && anon == null ){
                return ZThread.getExecutor((int)args[0]) ;
            }
            ZThread.SchedulerInputConfig config = ZThread.SchedulerInputConfig.fromArgs(interpret, anon, args);
            return ZThread.schedule(config);
        }

        static boolean empty(Object o) {
            if (o == null) return true;
            if (o instanceof Collection) {
                return ((Collection) o).isEmpty();
            }
            if (o.getClass().isArray()) {
                return (0 == Array.getLength(o));
            }
            if (o instanceof CharSequence) {
                return ((CharSequence) o).length() == 0;
            }
            if (o instanceof Map) return ((Map) o).isEmpty();
            if (o == Function.NIL ) return true;
            if (o instanceof ZFileSystem.ZFile) {
                o = ((ZFileSystem.ZFile) o).file;
            }
            if (o instanceof File) {
                if (((File) o).exists()) {
                    return ((File) o).length() == 0;
                } else {
                    return true;
                }
            }
            return false;
        }

        static Number size(Object... args) {
            if (args.length == 0) return -1;
            Object o = args[0];
            if (o == null) return -1;
            if (o instanceof Collection) {
                if ( o instanceof ZRange.ZRangeList ){
                    return ((ZRange.ZRangeList) o).zRange.zSize();
                }
                return ((Collection) o).size();
            }
            if (o.getClass().isArray()) {
                return Array.getLength(o);
            }
            if (o instanceof ZRange) {
                return ((ZRange) o).zSize();
            }
            if (o instanceof CharSequence) {
                return ((CharSequence) o).length();
            }
            if (o instanceof Map) return ((Map) o).size();
            if (o == Function.NIL) return 0;
            if (o instanceof ZFileSystem.ZFile) {
                o = ((ZFileSystem.ZFile) o).file;
            }
            if (o instanceof File) {
                if (((File) o).exists()) {
                    return ((File) o).length();
                } else {
                    return -1;
                }
            }
            if (o instanceof Number) {
                if (args.length > 1) {
                    Number n = ZNumber.integer(args[1]);
                    if (n != null && n.intValue() > 1) {
                        return ZTypes.string(o, n.intValue()).length();
                    }
                }
                return cardinality(o);
            }
            // fallback...
            throw new UnsupportedOperationException("Can not Computer Size for Object Type " + o.getClass());
        }

        static Number cardinality(Object o) {
            if (o == null) return 0;
            if (o instanceof Number) {
                ZNumber zn = new ZNumber(o);
                return zn._abs_();
            }
            return size(o);
        }

        static void print(boolean error, Object... f) {
            PrintStream ps = error ? System.err : System.out;
            if (f == null) {
                ps.println("null");
                return;
            }
            if (f.length == 0) {
                ps.println();
                return;
            }
            Object a = f[0];
            if (a == null) {
                ps.println("null");
                return;
            }
            if (a.getClass().isArray()) {
                a = new ZArray(a, false);
            }
            if (a instanceof Number) {
                a = new ZNumber(a);
            }
            ps.println(a);
        }

        static void println(Object... f) {
            print(false, f);
        }

        static void eprintln(Object... f) {
            print(true, f);
        }

        static void formatPrint(boolean error, Object... args) {
            if (args.length == 0) return;
            if ( error ){
                System.err.print(ZTypes.string(args));
            } else {
                System.out.print(ZTypes.string(args));
            }
        }

        static void printf(Object... args) {
            formatPrint(false, args);
        }

        static void eprintf(Object... args) {
            formatPrint(true, args);
        }

        static Collection join(List<Function> anons, Object... args) {
            if (args.length == 0) return new ZList();
            if (args[0] == null || args.length < 2) return Collections.EMPTY_LIST;
            ZCollection initial;
            if (args[0] instanceof ZRange) {
                args[0] = ((ZRange) args[0]).yielding().asList();
            }

            if (args[0].getClass().isArray()) {
                initial = new ZArray(args[0], false);
            } else if (args[0] instanceof ZCollection) {
                initial = (ZCollection) args[0];
            } else if (args[0] instanceof Iterable) {
                initial = new ZList((Iterable) args[0]);
            } else {
                throw new UnsupportedOperationException("Can not join non Collection!");
            }
            Collection[] cols = new Collection[args.length - 1];
            for (int i = 0; i < cols.length; i++) {
                int j = i + 1;
                if (args[j] instanceof ZRange) {
                    args[j] = ((ZRange) args[j]).yielding().asList();
                }

                if (args[j].getClass().isArray()) {
                    cols[i] = new ZArray(args[j], false);
                } else if (args[j] instanceof ZCollection) {
                    cols[i] = (ZCollection) args[j];
                } else if (args[i] instanceof Iterable) {
                    cols[i] = new ZList((Iterable) args[j]);
                } else {
                    throw new UnsupportedOperationException("Can not join non Collection!");
                }
            }
            if (anons.isEmpty()) return initial.join(cols);
            Function predicate = TRUE;
            Function mapper = COLLECTOR_IDENTITY;
            if ( anons.get(0) instanceof AnonymousFunctionInstance.MapperLambda ){
                if ( anons.size() > 1 ){
                    Function hashIndex = anons.get(0);
                    mapper = anons.get(1);
                    if ( anons.size() > 2 ){
                        predicate = anons.get(1);
                        mapper = anons.get(2);
                    }
                    return initial.joinIndex( hashIndex, predicate, mapper, cols );
                }
            }

            for (Function f : anons) {
                if (f instanceof AnonymousFunctionInstance.PredicateLambda) {
                    predicate = f;
                }
                if (f instanceof AnonymousFunctionInstance.MapperLambda) {
                    mapper = f;
                }
            }
            if (mapper == COLLECTOR_IDENTITY) {
                return initial.join(predicate, cols);
            }
            return initial.join(predicate, mapper, cols);
        }

        static Map accumulatorMap(NamedArgs na){
           if ( Boolean.TRUE.equals( na.getOrDefault("sort", false ) ) ){
                return new TreeMap<>();
           }
           if ( Boolean.TRUE.equals( na.getOrDefault("order", false ) ) ){
               return  new LinkedHashMap<>();
           }
           Object o = na.get("acc");
           if ( o != null ) {
               if (o instanceof Map<?, ?>) {
                   return (Map) o;
               }
               throw new IllegalArgumentException("'acc' is not a map!" );
           }
           return new HashMap<>();
        }

        static Map multiSet(Function anon, Object[] args) {
            if (args.length == 0) return new ZMap();
            Object a = args[0];
            Map accumulator = new HashMap<>();
            if (args.length > 1) {
                a = args; // implicit set from args, no flattening
            } else {
                if ( a instanceof NamedArgs ){
                    NamedArgs na = (NamedArgs)a;
                    a = na.get("col");
                    accumulator = accumulatorMap(na);
                }
            }
            Iterable<?> iterable = Converter.iterable(a, "Type can not be converted to a multi set!");
            if (anon == null) {
                // this is clean multiset, list of (item_key, count)
                return new ZMap.MultiSet(BaseZCollection.multiSet(iterable, accumulator));
            }
            return BaseZCollection.multiSet(anon, iterable, accumulator );
        }

        static Map groupBy(List<Function> anons, Object[] args) {
            if (args.length == 0) return Collections.EMPTY_MAP;
            if (anons.isEmpty()) return multiSet(null, args);
            if (anons.size() == 1) return multiSet(anons.get(0), args);
            Object a = args[0];
            Map accumulator = new HashMap<>();

            if (args.length > 1) {
                a = args; // implicit set from args, no flattening
            } else {
                if ( a instanceof NamedArgs ){
                    NamedArgs na = (NamedArgs)a;
                    a = na.get("col");
                    accumulator = accumulatorMap(na);
                }
            }
            Iterable<?> iterable = Converter.iterable(a, "Type can not be converted to a multi set!");
            return BaseZCollection.groupBy(anons.get(0), anons.get(1), iterable, accumulator);
        }

        static Collection<?> select(Object[] args, Function anon, List<Function> anons) {
            if (anons.size() > 1) {
                return (Collection<?>) from(anons, args);
            }
            if (args.length == 0) return Collections.EMPTY_LIST;
            Object a = args[0];
            if (a == null || anon == null) return Collections.EMPTY_LIST;
            final Collection<?> collector;
            if (args.length > 1 && args[1] instanceof Collection) {
                collector = (Collection<?>) args[1];
            } else {
                // collector not specified
                if ( a instanceof Set ){
                    collector = new ZSet();
                } else {
                    collector = new ZList();
                }
            }
            Iterable<?> iterable = Converter.iterable(a, "Type can not be converted to a collection!");
            return BaseZCollection.select(collector, anon, iterable);
        }

        final static Collection<?>[] EMPTY_PARTITIONS = new Collection[]{
                Collections.emptyList(), Collections.emptyList()};

        static Collection<?>[] partition(Function anon, Object[] args) {
            if (args.length == 0) return EMPTY_PARTITIONS;
            Object a = args[0];
            if (args.length > 1) {
                a = args; // implicit set from args, no flattening
            }
            if (a == null || anon == null) return EMPTY_PARTITIONS;
            final Collection<?>[] collectors;
            if ( a instanceof Set ){
                collectors = new Collection[] { new ZSet() , new ZSet() } ;
            } else {
                collectors = new Collection[] { new ZList(), new ZList() } ;
            }
            Iterable<?> iterable = Converter.iterable(a, "Type can not be converted to a collection!");
            return BaseZCollection.partition(collectors, anon, iterable);
        }

        static Object leftReduce(Object a, Function anon) {
            if (a == null || anon == null) return NIL;
            Iterable<?> iterable = Converter.iterable(a, "Type can not be converted to a collection!");
            return BaseZCollection.leftReduce(iterable, anon);
        }

        static Object leftFold(Object a, Function anon, Object... args) {
            if (a == null || anon == null) return NIL;
            Iterable<?> iterable = Converter.iterable(a, "Type can not be converted to a collection!");
            return BaseZCollection.leftFold(iterable, anon, args);
        }

        static Object rightFold(Object a, Function anon, Object... args) {
            if (a == null || anon == null) return NIL;
            List<?> list = Converter.list(a, "Type can not be converted to a List!");
            return BaseZCollection.rightFold(list, anon, args);
        }

        static Object rightReduce(Object a, Function anon) {
            if (a == null || anon == null) return NIL;
            List<?> list = Converter.list(a, "Type can not be converted to a List!");
            return BaseZCollection.rightReduce(list, anon);
        }

        static Function.MonadicContainer find(Object a, Function anon) {
            if (a == null || anon == null) return NOTHING;
            Iterable<?> iterable = Converter.iterable(a, "Type can not be converted to a collection!");
            return BaseZCollection.find(iterable, anon);
        }

        static int leftIndex(Object a, Function anon, Object... args) {
            if (a == null) return -1;
            Iterable<?> iterable = Converter.iterable(a, "Type can not be converted to a collection!");
            if (anon == null) {
                if (a instanceof CharSequence) {
                    return a.toString().indexOf(String.valueOf(args[0]));
                }
                return BaseZCollection.leftIndex(iterable, args[0]);
            }
            return BaseZCollection.leftIndex(iterable, anon);
        }

        static int rightIndex(Object a, Function anon, Object... args) {
            if (a == null) return -1;
            List<?> list = Converter.list(a, "Type can not be converted to a List!");
            if (anon == null) {
                if (a instanceof CharSequence) {
                    return a.toString().lastIndexOf(String.valueOf(args[0]));
                }
                return BaseZCollection.rightIndex(list, args[0]);
            }
            return BaseZCollection.rightIndex(list, anon);
        }

        /**
         * Converts to NamedArgs given the args are passed as Name Value pair
         * @param interpret using this interpreter
         * @param args arguments to a method
         * @return same args, or args[0] as NamedArgs wrapped in Object array
         */
        public static Object[] namedArgs(ZInterpret interpret, Object[] args) {
            if (args.length > 0 && args[0] instanceof ASTArgDef) {
                NamedArgs namedArgs = new NamedArgs();
                for (int i = 0; i < args.length; i++) {
                    ASTArgDef a = (ASTArgDef) args[i];
                    String name = ((ZoombaNode) a.jjtGetChild(0)).image;
                    Object value = a.jjtGetChild(1).jjtAccept(interpret, null);
                    namedArgs.put(name, value);
                }
                return new Object[]{namedArgs};
            }
            return args;
        }

        @Override
        public Function.MonadicContainer intercept(Object context, ZInterpret interpret, String methodName,
                                                   Object[] args, List<Function> anons, ZoombaNode callNode) {
            Function anon = anons.isEmpty() ? null : anons.get(0);
            // create named args, when necessary
            args = namedArgs(interpret, args);
            switch (methodName) {
                case EMPTY:
                    return new Function.MonadicContainerBase(empty(args[0]));
                case SIZE:
                    return new Function.MonadicContainerBase(size(args));
                case PRINTF:
                    printf(args);
                    return Function.Void;
                case PRINTLN:
                    println(args);
                    return Function.Void;
                case EPRINTF:
                    eprintf(args);
                    return Function.Void;
                case EPRINTLN:
                    eprintln(args);
                    return Function.Void;
                case LIST:
                    return new Function.MonadicContainerBase(ZList.list(anon, args));
                case SORTED_LIST:
                    return new Function.MonadicContainerBase(ZSortedList.BaseZSortedList.list(anons, args));
                case SET:
                    return new Function.MonadicContainerBase(ZSet.set(anon, args));
                case SORTED_SET:
                    return new Function.MonadicContainerBase(ZSet.sortedSet(anons, args));
                case ORDERED_SET:
                    return new Function.MonadicContainerBase(ZSet.orderedSet(anon, args));
                case DICT:
                    return new Function.MonadicContainerBase(ZMap.dict(anon, args));
                case SORTED_DICT:
                    return new Function.MonadicContainerBase(ZMap.sortedDict(anons, args));
                case ORDERED_DICT:
                    return new Function.MonadicContainerBase(ZMap.orderedDict(anon, args));
                case SELECT:
                    return new Function.MonadicContainerBase(select(args, anon, anons));
                case FOLD: // it is boring to do lfold all the time, default should be fold <-> lfold
                case LFOLD:
                    Object o = args[0];
                    Object[] others = ZTypes.shiftArgsLeft(args);
                    return new Function.MonadicContainerBase(leftFold(o, anon, others));
                case BOOL:
                    return new Function.MonadicContainerBase(bool(args));
                case INT:
                    return new Function.MonadicContainerBase(ZNumber.integer(args));
                case FLOAT:
                    return new Function.MonadicContainerBase(ZNumber.floating(args));
                case INT_L:
                    return new Function.MonadicContainerBase(ZNumber.INT(args));
                case FLOAT_L:
                    return new Function.MonadicContainerBase(ZNumber.FLOAT(args));
                case NUM:
                    return new Function.MonadicContainerBase(ZNumber.number(args));
                case TIME:
                    return new Function.MonadicContainerBase(ZDate.time(args));
                case FIND:
                    if (args.length == 0) return Function.FAILURE;
                    return new Function.MonadicContainerBase(find(args[0], anon));
                case EXISTS:
                    o = args[0];
                    others = ZTypes.shiftArgsLeft(args);
                    int i = leftIndex(o, anon, others);
                    if (i >= 0) return Function.SUCCESS;
                    return Function.FAILURE;
                case LINDEX:
                    o = args[0];
                    others = ZTypes.shiftArgsLeft(args);
                    i = leftIndex(o, anon, others);
                    return new Function.MonadicContainerBase(i);

                case LREDUCE:
                    if (args.length == 0) return Function.NOTHING;
                    o = leftReduce(args[0], anon);
                    return new Function.MonadicContainerBase(o);

                case RREDUCE:
                    if (args.length == 0) return Function.NOTHING;
                    o = rightReduce(args[0], anon);
                    return new Function.MonadicContainerBase(o);

                case PARTITION:
                    return new Function.MonadicContainerBase(partition(anon, args));
                case MSET:
                    return new Function.MonadicContainerBase(multiSet(anon, args));
                case GROUP_BY:
                    return new Function.MonadicContainerBase(groupBy(anons, args));
                case STRING:
                    if (anon != null && args.length > 1) {
                        return new Function.MonadicContainerBase(
                                ZTypes.string(anon, args[0], String.valueOf(args[1])));
                    }
                    return new Function.MonadicContainerBase(ZTypes.string(args));
                case JSON_STRING:
                    if (args.length != 1) {
                        return new Function.MonadicContainerBase(ZTypes.jsonFormatted(args));
                    }
                    return new Function.MonadicContainerBase(ZTypes.jsonString(args[0]));
                case JSON:
                    return new Function.MonadicContainerBase(ZTypes.json(args));
                case OPEN:
                    return new Function.MonadicContainerBase(open(args));
                case RFOLD:
                    o = args[0];
                    others = ZTypes.shiftArgsLeft(args);
                    return new Function.MonadicContainerBase(rightFold(o, anon, others));
                case RINDEX:
                    o = args[0];
                    others = ZTypes.shiftArgsLeft(args);
                    i = rightIndex(o, anon, others);
                    return new Function.MonadicContainerBase(i);
                case JOIN:
                    return new Function.MonadicContainerBase(join(anons, args));
                case FROM:
                    return new Function.MonadicContainerBase(from(anons, args));
                case CONCURRENT:
                    return new Function.MonadicContainerBase(concurrent(anons, args));
                case ROUND:
                    Number num = (Number) args[0];
                    args = ZTypes.shiftArgsLeft(args);
                    return new Function.MonadicContainerBase(ZNumber.round(num, args));
                case FLOOR:
                    return new Function.MonadicContainerBase(ZNumber.floor((Number) args[0]));
                case CEIL:
                    return new Function.MonadicContainerBase(ZNumber.ceil((Number) args[0]));
                case LOG:
                    return new Function.MonadicContainerBase(ZNumber.log(args));
                case THREAD:
                    return new Function.MonadicContainerBase(thread(false, true, anon, args));
                case V_THREAD:
                    return new Function.MonadicContainerBase(thread(true, true, anon, args));

                case TASK:
                    return new Function.MonadicContainerBase(thread(false, false, anon, args));
                case V_TASK:
                    return new Function.MonadicContainerBase(thread(true, false, anon, args));
                case FILE:
                    return new Function.MonadicContainerBase( args.length == 0 ? Collections.emptyList() :
                            ZFileSystem.file(args));
                case READ:
                    return new Function.MonadicContainerBase(read(args));
                case WRITE:
                    return new Function.MonadicContainerBase(ZFileSystem.write(args));
                case XML:
                    return new Function.MonadicContainerBase(ZXml.xml(args));
                case RANDOM:
                    return new Function.MonadicContainerBase(ZRandom.random(args));
                case SHUFFLE:
                    return new Function.MonadicContainerBase(ZRandom.shuffle(args[0]));
                case SORTA:
                    return new Function.MonadicContainerBase(ZRandom.sortAscending(args, anon));
                case SORTD:
                    return new Function.MonadicContainerBase(ZRandom.sortDescending(args, anon));
                case SUM:
                    return new Function.MonadicContainerBase(ZRandom.sum(anon, args));
                case MINMAX:
                    if (args.length == 0) return Function.FAILURE;
                    return new Function.MonadicContainerBase(ZRandom.minMax(anons, args));
                case SYSTEM:
                    return new Function.MonadicContainerBase(ZProcess.system(args));
                case POPEN:
                    return new Function.MonadicContainerBase(ZProcess.popen(args));
                case MATRIX:
                    return new Function.MonadicContainerBase(ZMatrix.Loader.load(args));
                case ENUM:
                    return new Function.MonadicContainerBase(ZTypes.enumCast(args));
                case BYE:
                    ZTypes.bye(args);
                    // unreachable code for sure
                    return Function.SUCCESS;
                case RAISE:
                    ZTypes.raise(args);
                    // unreachable
                    break;
                case HEAP:
                    return new Function.MonadicContainerBase(ZHeap.heap(anon, args));
                case LOAD:
                    return ZTypes.loadJar(args);

                case HASH:
                    return new Function.MonadicContainerBase(ZRandom.hash(args));
                case TOKENS:
                    return new Function.MonadicContainerBase(ZRandom.tokens(anons, args));
                case SEQUENCE:
                    return new Function.MonadicContainerBase(ZSequence.BaseSequence.sequence(anon, args));
                case POLL:
                    return new Function.MonadicContainerBase(ZThread.poll(anon, args));
                case ASYNC:
                    if (args.length != 0 && args[0] instanceof Function) {
                        return new Function.MonadicContainerBase(ZThread.async(anon, (Function) args[0]));
                    }
                    return Function.FAILURE;

                case SCHEDULE:
                    return new Function.MonadicContainerBase(schedule(interpret, anon, args));

                case PERMUTATION:
                    return new Function.MonadicContainerBase(Combinatorics.permutations(args));
                case COMBINATION:
                    return new Function.MonadicContainerBase(Combinatorics.combinations(args));
                case SUB_SEQUENCES:
                    return new Function.MonadicContainerBase(new Combinatorics.Sequences(args));
                case TUPLE:
                    return new Function.MonadicContainerBase(ZMatrix.Tuple.tuple(args));
                case YAML:
                    return new Function.MonadicContainerBase(ZTypes.yaml(args));
                case YSTR:
                    return new Function.MonadicContainerBase(ZTypes.yamlString(args));
                case SIGN:
                    return new Function.MonadicContainerBase(ZNumber.sign(args));
                case PRIORITY_QUEUE:
                    return new Function.MonadicContainerBase(ZTypes.pqueue(anon, args));

                case BINARY_SEARCH:
                    return new Function.MonadicContainerBase(ZRandom.binarySearch(args, anon));
                case JX_PATH:
                    return new Function.MonadicContainerBase(jxPath(args));
                case JX_ELEMENT:
                    return new Function.MonadicContainerBase(jxElement(args));

            }
            return UNSUCCESSFUL_INTERCEPT;
        }
    }
}
