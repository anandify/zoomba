/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.interpreter;

import zoomba.lang.core.collections.ZSet;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.Observable;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.parser.ZoombaNode;

import java.util.EventObject;
import java.util.Set;

/**
 * Assertion repo for ZoomBA
 */
public final class ZAssertion {

    /**
     * Types of assertion
     */
    public enum AssertionType {
        /**
         * Test assertion : when true it passes, when false it fails
         */
        TEST,

        /**
         * Assertion : when true it passes, when false it fails,the current test script will be aborted
         */
        ASSERT,

        /**
         * Assertion : when true, the current test script will be aborted , false means assertion passed
         */
        PANIC;

        /**
         * Given the value passed to the assertion, did the assertion fail ?
         * assertion value result
         * assert  false  failed
         * panic  true failed
         * test  false  failed
         * @param value the value
         * @return true if failed, false if did not
         */
        public boolean failed(boolean value){
            if ( this == TEST || this == ASSERT ) return !value ;
            return value ;
        }
    }


    /**
     * An Assertion Event object
     */
    public static class AssertionEvent extends EventObject {

        /**
         * Type of assertion
         */
        public  final  AssertionType type;

        /**
         * In case the caller passed any data
         */
        public final Object[] data;

        /**
         * The final value of this assertion
         */
        public final boolean value;

        /**
         * <pre>
         * What caused this assertion?
         * Either it is an expression evaluated, in which case it will be @{AssertionAssignment}
         * Else it is created by failure to evaluate block of code ( anonymous function )
         * where the resultant error would be stored.
         * </pre>
         */
        public final Throwable cause;

        /**
         * Creates an object
         * @param source source of the event
         * @param type what type the assertion is
         * @param value for test true passes, for abort false passes
         * @param cause cause of the assertion
         * @param data any data people wants to pass
         */
        public AssertionEvent(Object source, AssertionType type, boolean value, Throwable cause, Object[] data) {
            super(source);
            this.type = type;
            this.cause = cause ;
            this.data = data ;
            this.value = value ;
        }

        @Override
        public String toString(){
            boolean failed = type.failed(value);
            String ret = String.format("%s %s => { %s }", type, value,
                    ZTypes.string(data));
            if ( ZException.ZRuntimeAssertion.NOT_AN_EXCEPTION != cause ){
                ret += String.format( " | caused by : %s" , cause );
            }
            if ( failed ){
                return "!!!" + ret ;
            }
            return ret;
        }
    }

    /**
     * Anyone who wants to listen to assertions
     */
    public interface AssertionEventListener{

        /**
         * Gets triggered when an assertion gets fired
         * @param assertionEvent the event that triggered
         */
        void onAssertion(AssertionEvent assertionEvent);

    }

    /**
     * The event listeners
     */
    public final Set<AssertionEventListener> eventListeners;

    /**
     * Constructs a ZAssertion object
     */
    public ZAssertion(){
        eventListeners = new ZSet();
    }

    // this is the lazy evaluation, ONLY when panic fired or assert failed
    static void processArgsForObservables(Object[] args){
        for ( int i=0; i < args.length ; i++ ){
            try {
                args[i] = ((Observable<?>)args[i]).value();
            }catch (Throwable ignore){ }
        }
    }

    /**
     * Is the name of the method assertion type ( assert, panic, test )
     * @param name of any method
     * @return whether method is assertion
     */
    public static boolean isAssertion(String name){
        return "assert".equals(name) || "panic".equals(name) ||"test".equals(name) ;
    }

    /**
     * Fires a abort assertion -
     * @param type Type of assertion
     * @param node the location where assertion was fired
     * @param anon the function which has to be evaluated
     * @param args any args one may pass
     */
    public void testAssert(AssertionType type, ZoombaNode node, Function anon, Object...args)  {
        Throwable cause = ZException.ZRuntimeAssertion.NOT_AN_EXCEPTION;
        boolean value  ;
        switch ( type ){
            case TEST:
            case ASSERT:
                value = false;
                break;
            default:
                value = true ;

        }

        if ( anon != null ){
            try {
                Function.MonadicContainer mc = anon.execute();
                value = ZTypes.bool(mc.value(), value);
            }catch (Throwable t){
                cause = t.getCause();
                if ( cause == null ){
                    cause = t;
                }
            }
        }else{
            if ( args.length > 0 ){
                Object v = args[0];
                try {
                    v = ((Observable<?>)v).value();
                }catch (Throwable ignore){
                    ignore.printStackTrace();
                }

                args = ZTypes.shiftArgsLeft(args);
                if ( v instanceof Function ){
                    testAssert( type,node,(Function)v,args);
                    return;
                }
                value = ZTypes.bool(v, value);
            }
        }

        for ( AssertionEventListener listener : eventListeners ){
            AssertionEvent event = new AssertionEvent(this, type , value, cause, args);
            try {
                listener.onAssertion(event);
            }catch (Throwable t){
                System.err.printf("Error *%s* asserting to listener : %s [%s]\n",type, listener,t);
            }
        }

        boolean noThrow = ( !value && type == AssertionType.PANIC ) ||
                ( value && ( type == AssertionType.ASSERT || type == AssertionType.TEST ) );
        if ( noThrow ) return;
        // delay processing all args till this late
        processArgsForObservables(args);
        // now raise stuff ...
        switch (type){
            case PANIC:
                throw new ZException.Panic(node,args);
            case ASSERT:
                throw new ZException.Assertion(node,args);
            case TEST:
                System.err.printf("Test Assertion Failed @ %s : { %s }\n",
                            node.locationInfo(), ZTypes.string(args));
                break;
        }
    }
}
