/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Objects are a necessary evil
 * <pre>
 * This package provides a way to package fields and properties
 * in ECMA script like prototype object model with
 * 1. Inheritance
 * 2. Polymorphism
 * 3. Operator Overloading
 * </pre>
 */
package zoomba.lang.core.oop;