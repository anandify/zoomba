/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.io;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.ZMatrix;
import zoomba.lang.core.types.ZTypes;

import java.io.File;
import java.nio.file.Files;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A DataMatrix representation from Text Files
 */
public class ZTextDataFile extends ZMatrix.BaseZMatrix{

    /**
     * The TextDataMatrix loader
     */
    public static final class ZTextDataFileLoader implements ZMatrixLoader{

        /**
         * Underlying ZTextDataFileLoader
         */
        public static final ZTextDataFileLoader LOADER = new ZTextDataFileLoader();

        private ZTextDataFileLoader(){}

        @Override
        public ZMatrix load(String path,Object...args) {
            String sep = "\t" ;
            boolean noHeaders = false;
            boolean winExcel = false ;
            String valueSep = "\"";
            if ( args.length > 0 ){
                if ( args[0] instanceof Map ){
                  Map<String,Object> params = (Map)args[0];
                  sep = params.getOrDefault("sep", "\t").toString();
                  valueSep = params.getOrDefault("valueSep", "\"").toString();
                  noHeaders = !(boolean)params.getOrDefault("headers", true ) ;
                  winExcel = (boolean)params.getOrDefault("excel", false ) ;
                }
                else {
                    sep = String.valueOf(args[0]);
                    if (args.length > 1) {
                        noHeaders = ZTypes.bool(args[1], false);
                        if (args.length > 2) {
                            winExcel = ZTypes.bool(args[2], false);
                        }
                    }
                }
            }
            return new ZTextDataFile( winExcel, path,sep,noHeaders,valueSep);
        }

        @Override
        public Pattern loadPattern() {
            return Pattern.compile("^.+\\.(csv|tsv|txt|text)$", Pattern.CASE_INSENSITIVE );
        }
    }

    /**
     * Lines of data , not cells, because saves memory
     */
    public final List<String> lines ;

    /**
     * The separator string
     */
    public final String sep;

    /**
     * Value Separator in case sep is inside data
     */
    public final String valueSep ;

    /**
     * Value pattern
     */
    public final Pattern valuePattern;
    /**
     * Headers of the columns
     */
    public final List<String> headers;

    @Override
    public List<String> headers() {
        return headers;
    }

    private final boolean noHeaders;
    @Override
    public boolean hasHeaders(){
        return !noHeaders;
    }

    /**
     * Creates a Matrix from
     * @param winExcel if true, imagines that lines are '\r\n'
     * @param path the file path
     * @param sep the separator
     * @param noHeaders if true, then no header, if false, first row becomes header
     * @param valueSep if values contains separators, then use this
     */
    public ZTextDataFile(boolean winExcel, String path,String sep, boolean noHeaders, String valueSep){
        File f = new File(path);
        try {
            if ( winExcel ){
              String full = ZFileSystem.read(path).toString();
              lines = new ZArray( full.split("\r\n"), false );
            } else {
                lines = Files.readAllLines(f.toPath());
            }
            this.sep = sep ;
            this.valueSep = valueSep;
            this.noHeaders = noHeaders;
            final String patternFormat =  "%s[^%s]*%s" ;
            final String patternString = String.format(patternFormat,  valueSep, valueSep, valueSep);
            valuePattern = Pattern.compile(patternString);

            if ( lines.isEmpty() ){
                System.err.printf("Matrix  '%s' does not have any data! %n", path);
                this.headers = Collections.emptyList();
                return;
            }

            if ( noHeaders ){
                headers = defaultHeaders( row(0).size());
            } else {
                headers = new ZArray( lines.get(0).split(sep,-1),false );
            }
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    /**
     * Data which were cached in matrix format
     */
    protected Map<Integer,List> cachedData = new WeakHashMap<>();

    @Override
    public List row(int rowNum) {

        if ( cachedData.containsKey( rowNum ) ){
            return cachedData.get(rowNum);
        }

        final String line = lines.get(rowNum);
        List l = new ZArray( line.split(sep,-1),false ) ;
        int tmp;
        // there is an issue, for which for header less matrix at load time headers will be null,...
        try{ tmp = headers.size(); }catch (Throwable t){ tmp = l.size(); }
        final int colSize = tmp;

        if ( l.size() != colSize ){
            String tmpLine = line;
            final String replacement = "\uD83D\uDD23";
            // need to do something here...
            List r  = new ArrayList<>();
            Matcher m = valuePattern.matcher(tmpLine);
            while ( m.find() ){
                String v = m.group();
                tmpLine = tmpLine.replaceFirst(v,replacement);
                v = v.replaceAll(valueSep,"");
                r.add(v);
                m = valuePattern.matcher(tmpLine);
            }
            l = new ZArray( tmpLine.split(sep,-1),false ) ;
            for ( int i=0, j=0; i < l.size(); i++ ){
                if ( replacement.equals( l.get(i) ) ){
                    l.set(i, r.get(j));
                    j++;
                }
            }
        }
        cachedData.put(rowNum,l);
        return l;
    }

    @Override
    public int rows() {
        return lines.size() ;
    }

}
