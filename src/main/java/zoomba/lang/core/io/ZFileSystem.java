/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.io;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZTypes;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * A FileSystem utility
 */
public class ZFileSystem {

    /**
     * A File Item iterator.
     * Treats a file as a collection of string lines,
     * TODO given condition, also as chunks of bytes, we should
     * In case of directory, as a collection of child Files
     */
    public static class ZFItemIterator implements Iterator {

        /**
         * The underlying file object
         */
        public final File file;

        /**
         * The underlying iterator
         */
        ZArray.ArrayIterator filesIterator;

        /**
         * Is the object a dictionary
         */
        final boolean isDict;

        private static class WinEnabledReader{

            final BufferedReader br;

            final Reader reader;

            WinEnabledReader( File f, boolean crlf) throws Exception {
                reader = new FileReader( f );
                if ( crlf ){
                    br = null;
                } else {
                    br = new BufferedReader( reader );
                }
            }

            String readLine() throws IOException {
                if ( br != null ){ return br.readLine(); }
                StringBuffer buf = new StringBuffer();
                int c;
                while ( true ){
                    c = reader.read();
                    if ( c == -1 ){
                        reader.close();
                        if ( buf.length() == 0 ){
                            return null; // ignore last empty line...
                        }
                        break;
                    }
                    // forward looking?
                    if ( (char)c == '\r' ){
                        c = reader.read();
                        if ( c == -1 ){
                            reader.close();
                            if ( buf.length() == 0 ){
                                return null; // ignore last empty line...
                            }
                            break;
                        }
                        if ( (char)c == '\n'){
                            // newline
                            break;
                        }
                    }
                    buf.append((char)c);
                }
                return buf.toString();
            }
        }

        WinEnabledReader br;

        String line;

        ZFItemIterator(File f, boolean winExcel ) {
            file = f;
            if (!file.exists())
                throw new UnsupportedOperationException( new
                        FileNotFoundException( "'" + f.getAbsolutePath() + "', Can not create iterator on nonexistent file")
                );
            if (file.isDirectory()) {
                filesIterator = new ZArray.ArrayIterator(file.listFiles());
                isDict = true;
                br = null;
            } else {
                try {
                    br = new WinEnabledReader(file, winExcel );
                    line = br.readLine();
                    isDict = false;
                } catch (Exception e) {
                    throw new UnsupportedOperationException(e);
                }
            }
        }

        @Override
        public boolean hasNext() {
            if (isDict) return filesIterator.hasNext();
            return line != null;
        }

        @Override
        public Object next() {
            if (isDict) return filesIterator.next();
            String oldLine = line;
            line = null;
            try {
                line = br.readLine();
            } catch (Throwable t) {
            }
            return oldLine;
        }

    }

    /**
     * A File Item iterable
     * Treats a file as a collection of string lines,
     * TODO given condition, also as chunks of bytes, we should
     * In case of directory, as a collection of child Files
     */
    public static class ZFile implements Iterable {

        /**
         * Underlying file object
         */
        public final File file;

        private ZArray names = null ;

        /**
         * Gets the entire data for the file
         * @return data as in byte array
         * @throws Exception in case of error
         */
        public byte[] data() throws Exception {
            return Files.readAllBytes( file.toPath() ) ;
        }

        /**
         * Gets the entire data for the file
         * @return data as in String
         * @throws Exception in case of error
         */
        public String string() throws Exception {
            byte[] data = data();
            return new String(data);
        }

        /**
         * Gets the Parent ZFile
         * @return parent file
         */
        public ZFile parent(){
            return new ZFile(file.getParent());
        }

        /**
         * Gets the List of Children ZFile
         * @return List of Child file if it is a directory
         */
        public List<ZFile> children(){
            if ( !file.exists() || file.isFile() ) return Collections.emptyList();
            String[] child = file.list() ;
            if ( child == null ) return Collections.emptyList();
            return Arrays.stream(child).map(ZFile::new).collect(Collectors.toList());
        }

        /**
         * Canonical Absolute Path of this file
         * @return Canonical Abs file Path of the file, or empty if file does not exist
         * @throws Exception in case of error
         */
        public String canon() throws Exception {
            if ( !file.exists() ) return "" ;
            return file.getCanonicalFile().getPath() ;
        }

        /**
         * Gets the relative path of this file, basis of baseTo folder
         * @param baseTo the ancestor basis
         * @return relative path of this file, basis of baseTo folder
         * @throws Exception in case of error, or in case of baseTo folder was not a ancestor of this file
         */
        public String relative(String baseTo) throws Exception {
            String canonBase = new File(baseTo).getCanonicalPath() ;
            String myCanon = canon();
            if ( !myCanon.startsWith( canonBase) ) throw new IllegalArgumentException("Base is not a ancestor of this!") ;
            return myCanon.substring( canonBase.length() + 1 );
        }

        /**
         * Gets the relative path of this file, basis of Current Working Directory
         * @return relative path of this file, basis of baseTo folder
         * @throws Exception in case of error, or in case Current Working Directory is not an ancestor
         */
        public String relative() throws Exception {
            return relative( System.getProperty("user.dir"));
        }

        /**
         * Copy file to a new path of type String
         * @param toPath the path
         * @return a new ZFile
         * @throws Exception in case of any error
         */
        public ZFile copy(Path toPath) throws Exception {
            Files.copy( file.toPath(), toPath);
            return new ZFile(toPath.toString());
        }

        /**
         * Copy file to a new path of type Path
         * @param toPath the path
         * @return a new ZFile
         * @throws Exception in case of any error
         */
        public ZFile copy(String toPath) throws Exception {
            return copy( Paths.get(toPath));
        }

        /**
         * Copy file to a directory with a new name
         * @param dir the directory where to copy
         * @param fileName the file name of the copy
         * @return a new ZFile
         * @throws Exception in case of any error
         */
        public ZFile copy(ZFile dir, String fileName) throws Exception {
            Path toPath = Paths.get( dir.file.getPath(), fileName ) ;
            return copy(toPath);
        }

        /**
         * Copy file to a directory - name remains same as the current file
         * @param dir the directory where to copy
         * @return a new ZFile
         * @throws Exception in case of any error
         */
        public ZFile copy(ZFile dir) throws Exception {
            return copy( dir, file.getName());
        }

        /**
         * Move the file to a new path of type String
         * @param toPath the path
         * @return a new ZFile
         * @throws Exception in case of any error
         */
        public ZFile move(Path toPath) throws Exception {
            Files.move( file.toPath(), toPath);
            return new ZFile(toPath.toString());
        }

        /**
         * Move the file to a new path of type Path
         * @param toPath the path
         * @return a new ZFile
         * @throws Exception in case of any error
         */
        public ZFile move(String toPath) throws Exception {
            return move( Paths.get(toPath));
        }

        /**
         * Move file to a directory with a new name
         * @param dir the directory where to copy
         * @param fileName the file name of the copy
         * @return a new ZFile
         * @throws Exception in case of any error
         */
        public ZFile move(ZFile dir, String fileName) throws Exception {
            Path toPath = Paths.get( dir.file.getPath(), fileName ) ;
            return move(toPath);
        }

        /**
         * Move file to a directory - name remains same as the current file
         * @param dir the directory where to copy
         * @return a new ZFile
         * @throws Exception in case of any error
         */
        public ZFile move(ZFile dir) throws Exception {
            return move( dir, file.getName());
        }

        /**
         * Renames a file
         * @param newName the new name
         * @return true if success, false on failure
         */
        public boolean rename(String newName){
            return file.renameTo( new File(newName));
        }


        /**
         * Should we be using crlf line ending? '\r\n'
         */
        public final boolean crlfLineEnding;

        ZFile(String path) {
            this(path,false);
        }

        ZFile(String path, boolean winFile) {
            this.file = new File(path);
            this.crlfLineEnding = winFile;
        }

        private synchronized ZArray names(){
            if ( names != null ) return names;

            final String name = file.getName() ;
            int last = name.lastIndexOf( '.' );
            if ( last < 0 ) {
                names = new ZArray( new Object[]{ name, ""  } , false );
            } else {
                final String ext = name.substring(last);
                final String baseName = name.substring(0, last);
                names = new ZArray(new Object[]{baseName, ext}, false);
            }
            return names;
        }

        /**
         * abc.txt has base name abc
         * foobar has base name foobar
         * Gets the base name of the file, sans the extension if any
         * @return the base name
         *
         */
        public String baseName(){
           return names().get(0).toString();
        }

        /**
         * Gets the extension of the file, if any
         * @return the extension, wo the dot
         */
        public String extension() {
            return names().get(1).toString();
        }

        @Override
        public Iterator iterator() {
            return new ZFItemIterator(file, crlfLineEnding);
        }

        /**
         * Creates a stream of ZFile walking the file
         *
         * @return
         *  If file does not exist, returns an empty stream
         *  if it is only a file, return a singleton stream comprising the file itself
         *  if it is a directory, then recursively finds all children of the directory and that is the stream
         * @throws Exception when something goes wrong while walking
         */
        public Stream<ZFile> descendants( ) throws Exception {
            if ( !file.exists() ) return Stream.of(); // empty stream
            if ( file.isFile() ) return Stream.of(this);
            final Path path = file.toPath();
            return Files.walk(path).map( p -> new ZFile(p.toString()) ) ;
        }

        /**
         * The whole of the file as a single String
         * @return the full content of the file
         * @throws Exception in case any errors
         */
        public String all() throws Exception {
            Path p = file.toPath();
            byte[] bytes = Files.readAllBytes(p);
            return new String(bytes);
        }

        /**
         * The whole of the file as a List of Strings, lines
         * @return the content of the file as list of strings
         * @throws Exception in case any errors
         */
        public List<String> lines() throws Exception {
            Path p = file.toPath();
            return Files.readAllLines(p);
        }

        /**
         * Any meta property of the file
         * @param propertyName the property one needs to read
         *                     It does not follow links
         * @return the value of the property
         * @throws Exception in case of error
         */
        public Object get(String propertyName) throws Exception {
            Path p = file.toPath();
            return Files.getAttribute(p, propertyName, LinkOption.NOFOLLOW_LINKS);
        }

        @Override
        public String toString() {
            return file.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ZFile zFile = (ZFile) o;

            return file.equals(zFile.file);

        }

        @Override
        public int hashCode() {
            return file.hashCode();
        }

        /**
         * Packs the file into the destination
         * @param destFileName the destination full path
         * @return true if success false if failed
         */
        public boolean pack(String destFileName){
            try {
                if (file.isDirectory()) {
                    packDirectory(file.getAbsolutePath(), destFileName);
                } else {
                    packFile(file, destFileName);
                }
                return true;
            }catch (IOException e){
                return false;
            }
        }
        /**
         * Packs the file into the same directory as the current file
         * With extension fileName.zip
         * @return true if success false if failed
         */
        public boolean pack(){
            String destFileName = file.getParent() + "/" + file.getName() + ".zip" ;
            return pack(destFileName);
        }
    }

    /**
     * The File Abstraction from a path
     * @param args 0 path to the file 1 optional boolean treat '\r\n' as newline and nothing else
     * @return a ZFile object
     */
    public static ZFile file(Object...args) {
        String path = String.valueOf(args[0]);
        if ( args.length > 1 ){
            return new ZFile(path, ZTypes.bool(args[1], false ));
        }
        return new ZFile(path);
    }

    /**
     * Opens a file
     * @param args
     *     0 path
     *     1 mode one of "r"ead,"w"rite,"a"pend
     * @return a PrintStream or BufferedReader in case of no args, returns System.in
     */
    public static Closeable open(Object... args) {
        if (args.length == 0) return System.in;
        String path = "";
        if (args[0] instanceof File) {
            path = args[0].toString();
        } else {
            path = String.valueOf(args[0]);
        }
        String mode = "r";
        if (args.length > 1) {
            mode = String.valueOf(args[1]);
        }
        mode = mode.toLowerCase();
        try {
            switch (mode) {
                case "wb":
                case "bw":
                    return new FileOutputStream(path);

                case "w":
                    return new PrintStream(new FileOutputStream(path));
                case "a":
                    return new PrintStream(new FileOutputStream(path, true), true);

                case "rb":
                case "br":
                    return new FileReader(path);

                case "r":
                default:
                    return new BufferedReader(new FileReader(path));
            }
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    /**
     * Writes into a file
     * @param args
     *   0 path
     *   1 the data - if it is byte[] writes binary, else gets the byte[] from the string and writes
     *   2 append mode - if true, appends, if false truncate file
     *   3 mkdirs - if true ensures creating the entire parent path before writing the file
     * @return the path to the file
     */
    public static Path write(Object... args) {

        if (args.length == 0) {
            throw new UnsupportedOperationException("Can not write to nothing!");
        }
        try {
            Path filePath = Paths.get( String.valueOf(args[0]) );
            byte[] dataBytes = new byte[]{} ;
            if (args.length == 1) {
                return Files.write(filePath, dataBytes );
            }
            OpenOption[] options = new OpenOption[ ]{ StandardOpenOption.CREATE,  StandardOpenOption.TRUNCATE_EXISTING };
            // if binary data is passed, just write binary
            if ( args[1] instanceof byte[] ){
                dataBytes = (byte[])args[1] ;
            } else {
                dataBytes = String.valueOf(args[1]).getBytes();
            }

            if ( args.length > 2 && Boolean.TRUE.equals( ZTypes.bool(args[2], false) ) ) {
                // append
                options[1] = StandardOpenOption.APPEND ;
            }

            if ( args.length > 3 && Boolean.TRUE.equals( ZTypes.bool(args[3], false) ) ){
                // mkdirs
                File parentDir = filePath.getParent().toFile() ;
                if ( !parentDir.exists() && !parentDir.mkdirs()){
                    throw new IllegalStateException("Failed to mkdirs for the path!");
                }
            }
            return Files.write(filePath, dataBytes, options);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Zips a directory
     * @param sourceDirPath source directory
     * @param zipFilePath output directory
     * @throws IOException in error
     */
    public static void packDirectory(String sourceDirPath, String zipFilePath) throws IOException {
        Path p = Files.createFile(Paths.get(zipFilePath));
        try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p))) {
            Path pp = Paths.get(sourceDirPath);
            Files.walk(pp)
                    .filter(path -> !Files.isDirectory(path))
                    .forEach(path -> {
                        ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString());
                        try {
                            zs.putNextEntry(zipEntry);
                            Files.copy(path, zs);
                            zs.closeEntry();
                        } catch (IOException e) {
                            System.err.println(e);
                        }
                    });
        }
    }

    /**
     * Zips a File
     * @param sourceFile source file
     * @param zipFilePath output directory
     * @throws IOException in error
     */
    public static void packFile(File sourceFile, String zipFilePath) throws IOException{
        byte[] buffer = new byte[1024];
        FileOutputStream fos = new FileOutputStream(zipFilePath);
        ZipOutputStream zos = new ZipOutputStream(fos);
        ZipEntry ze= new ZipEntry(sourceFile.getName());
        zos.putNextEntry(ze);
        FileInputStream in = new FileInputStream(sourceFile);

        int len;
        while ((len = in.read(buffer)) > 0) {
            zos.write(buffer, 0, len);
        }

        in.close();
        zos.closeEntry();

        //remember close it
        zos.close();
    }

    /**
     * Reads from a file
     * @param args
     *    0 the path
     *    1 if boolean true, reads all the lines as List of Strings
     *      if 1 arg is "b" read all data bytes out of the file
     * @return String or List of Strings the data of the file
     * @throws Exception in case of error
     */
    public static Object read(Object... args) throws Exception {
        if (args.length == 0) return Function.NIL;
        ZFile zf = new ZFile(String.valueOf(args[0]));
        if (args.length == 1) {
            return zf.all();
        }
        if (args[1] instanceof Boolean) {
            if ((boolean) args[1]) {
                return zf.lines();
            }
        }
        if ( "b".equals(args[1]) ){
            // binary, read all bytes
            return Files.readAllBytes( zf.file.toPath());
        }
        return Function.NIL;
    }
}
