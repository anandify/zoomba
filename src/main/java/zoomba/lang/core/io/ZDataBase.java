/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.io;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.ZMatrix;
import zoomba.lang.core.types.ZTypes;
import java.sql.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.regex.Pattern;
import static zoomba.lang.core.operations.Function.NIL;

/**
 * Connectivity using Database
 */
public class ZDataBase {

    /**
     * An implementation of Matrix over DataBase ResultSet
     */
    public static final class ResultSetMatrix extends ZMatrix.BaseZMatrix{

        ResultSet resultSet;

        List dataColumns = null ;

        int size = -1;

        Map<Integer,List> rows = new WeakHashMap<>();

        /**
         * Creates a result set matrix
         * @param resultSet using this result set
         */
        public ResultSetMatrix(ResultSet resultSet){
            this.resultSet = resultSet;
        }

        /**
         * Gets the list of data column names
         * @return data column names
         */
        public List dataColumns(){
            if ( dataColumns != null ) return dataColumns;
            try {
                ResultSetMetaData md = resultSet.getMetaData();
                int numCols = md.getColumnCount();
                String[] arr = new String[numCols];
                for ( int i = 0 ; i < arr.length; i++ ){
                    arr[i] = md.getColumnName(i+1);
                }
                dataColumns = new ZArray(arr,false);
                return dataColumns;
            }catch (Exception e){
                throw new RuntimeException(e);
            }
        }

        private int colSize = -1;

        /**
         * Gets the size of the columns
         * @return the size of the columns
         */
        public int colSize(){
            if ( colSize >= 0 ) return colSize;
            colSize = row(0).size();
            return colSize;
        }

        @Override
        public List row(int rowNum) {
            if ( rowNum == 0 ) return dataColumns();
            if ( rows.containsKey( rowNum ) ) return rows.get(rowNum);
            // do something else ...
            try {
                resultSet.absolute( rowNum );
                String[] arr = new String[ colSize() ];
                for ( int i = 0 ; i < arr.length; i++ ){
                    arr[i] = resultSet.getString(i+1);
                }
                ZArray l = new ZArray( arr, false );
                rows.put(rowNum,l);
                return l;

            }catch (Exception e){
                throw new UnsupportedOperationException(e);
            }
        }

        @Override
        public int rows() {
            if ( size < 1 ){
               try{
                   if (resultSet.last()) {
                       size = resultSet.getRow();
                       // Move to beginning
                       resultSet.beforeFirst();
                       // because of it row 0 is headers
                       // see the row() function
                       size += 1;
                   }
               } catch (Exception e){
                   System.err.println(e);
               }
            }
            return size;
        }
    }

    /**
     * Selecting select in any query...
     * A pattern that matches select command
     */
    public static Pattern SELECT_PATTERN = Pattern.compile("^\\s*select\\s+.*",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    /**
     * Every driver has different style, thus, it is better to specify it that way
     * https://www.petefreitag.com/articles/jdbc_urls/
     */
    public static final class ZDBConnection {
        /**
         * Name of the connection
         */
        public final String name;
        /**
         * The driver class to connect
         * @return the driver name
         */
        public String driver(){ return entry.get("driver") ; }
        /**
         * connection url of the data base
         * @return the url
         */
        public String url() { return entry.get("url") ; }
        /**
         * User name of the user
         * @return the user name
         */
        public  String user() { return entry.containsKey("user")? entry.get("user") : "" ; }

        /**
         * Password of the user
         * @return the password if any specified
         */
        public  String pass() { return entry.containsKey("pass")? entry.get("pass") : "" ; }

        Connection connection;

        Map<String,String> entry;

        /**
         * Create from a map
         * @param n the name of the entry
         * @param entry the map
         */
        public ZDBConnection(String n, Map<String,String> entry) {
            name = n;
            this.entry = entry ;
        }

        /**
         * Gets a connection
         * @return the connection
         */
        public Connection connection(){
            if ( connection != null  ) return connection;

            try {
                Class.forName( driver() );
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
            try {
                connection = DriverManager.getConnection( url(),  user(), pass() );
                return connection;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        /**
         * Executes sql
         * @param sql the text sql
         * @return either a result set or an int
         */
        public Object exec( String sql) {
            sql = sql.trim();
            Connection  conn = connection();
            try {
                Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                if (SELECT_PATTERN.matcher(sql).matches()) {
                    return stmt.executeQuery(sql);
                } else {
                    return stmt.executeUpdate(sql);
                }
            }catch (Throwable e){
                throw new RuntimeException(e);
            }
        }

        @Override
        public String toString(){
            return String.format("{ \"%s\" : [ \"%s\", \"%s\", \"%s\"] }", name, driver(),url(),user() );
        }
    }

    Map<String,ZDBConnection> connectionMap;

    /**
     * Creates a new instance
     * @param configLocation given this configuration
     */
    public ZDataBase(String configLocation){
        Map<String,Map> config = (Map)ZTypes.json(configLocation,true);
        connectionMap = new HashMap<>();
        for ( String key : config.keySet() ){
            ZDBConnection con = new ZDBConnection( key, config.get(key) );
            connectionMap.put(key,con);
        }
    }

    /**
     * Given a connectionId, and query executes the query
     * @param connectionId the connection id
     * @param query the query string
     * @return the result of the execution
     */
    public Object exec(String connectionId, String query){
        if (!connectionMap.containsKey(connectionId )){
           throw new IllegalArgumentException( connectionId + " is not a valid connection id!\n");
        }
        ZDBConnection con = connectionMap.get(connectionId);
        return con.exec(query);
    }

    /**
     * Given a connectionId, and query executes the query and returns Matrix
     * @param connectionId the connection id
     * @param query the query string
     * @return the result of the execution as Matrix
     */
    public ZMatrix matrix(String connectionId, String query){
        Object o = exec(connectionId,query);
        if ( !(o instanceof ResultSet) ){
            return ZMatrix.EMPTY_MATRIX ;
        }
        return new ResultSetMatrix((ResultSet)o);
    }

    @Override
    public String toString(){
       return "[" + ZTypes.string( connectionMap.values() , "," ) + "]" ;
    }
}
