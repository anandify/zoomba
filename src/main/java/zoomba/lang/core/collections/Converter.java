/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.collections;

import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZRange;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility class to convert arg object to various iterable types
 */
public final class Converter {

    /**
     * Converts an object to iterable if possible, else raise error with message
     * @param a object to be converted to an iterable
     * @param errMessage message for error, in case conversion failed
     * @return an Iterable
     */
    public static Iterable<?> iterable(Object a, String errMessage){
        if ( a instanceof Iterable){
            return  (Iterable<?>) a;
        }
        if ( a instanceof ZRange){
            return  ((ZRange)a).asList();
        }
        if (a instanceof Iterator) {
            return ZCollection.iterable((Iterator<?>) a);
        }
        if ( a.getClass().isArray() ){
            return new ZArray(a,false);
        }
        if ( a instanceof Stream<?>){
            return ZCollection.iterable((Stream<?>) a);
        }
        if ( a instanceof Map){
            return  ((Map<?,?>) a).entrySet();
        }
        if ( a instanceof CharSequence ){
            return new ZArray( a.toString().toCharArray() , false );
        }
        throw new IllegalArgumentException(errMessage);
    }

    /**
     * Converts an object to a List if possible, else raise error with message
     * @param a object to be converted to a List
     * @param errMessage message for error, in case conversion failed
     * @return a List
     */
    public static List<?> list(Object a, String errMessage){
        if ( a instanceof List){
            return  (List<?>) a;
        }
        if ( a instanceof ZRange){
            return  ((ZRange)a).asList();
        }
        if ( a.getClass().isArray() ){
            return new ZArray(a,false);
        }
        if ( a instanceof Stream){
            return ((Stream<?>)a).collect(Collectors.toList());
        }
        if ( a instanceof CharSequence ){
            return new ZArray( a.toString().toCharArray() , false );
        }
        throw new IllegalArgumentException(errMessage);
    }
}
