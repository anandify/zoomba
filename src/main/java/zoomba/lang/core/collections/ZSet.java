/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.collections;

import zoomba.lang.core.interpreter.AnonymousFunctionInstance;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZRange;

import java.util.*;


/**
 * A Set class to be used as Wrapper for All Set interfaces for JVM
 */
public class ZSet extends BaseZCollection implements SortedSet, NavigableSet {

    /**
     * An empty ZSet implementation
     */
    public final static ZSet EMPTY = new ZSet(Collections.EMPTY_SET);

    /**
     * Creates a set
     *
     * @param anon using the optional function as mapper for the key
     * @param args using the args
     * @return a set
     */
    public static Set set(Function anon, Object... args) {
        if (args.length == 0) return new ZSet();
        Object a = args[0];
        if (args.length > 1) {
            a = args; // implicit set from args, no flattening
        }
        if (anon == null) {
            if (a instanceof ZRange) {
                a = ((ZRange) a).asList();
            }
            if (a instanceof Collection) {
                return new ZSet((Collection) a);
            }
            if (a == null) return Collections.emptySet();
            if (a.getClass().isArray()) {
                return new ZSet(new ZArray(a, false));
            }
            return new ZSet(a);
        }
        final Iterable iterable = Converter.iterable(a, "Type can not be converted to a set!");
        return BaseZCollection.set(new ZSet(), anon, iterable);
    }

    /**
     * Creates an Ordered set
     *
     * @param anon using the optional function as mapper for the key
     * @param args using the args
     * @return a set
     */
    public static Set orderedSet(Function anon, Object... args) {
        if (args.length == 0) return new ZSet(new LinkedHashSet());
        Object a = args[0];
        if (args.length > 1) {
            a = args; // implicit set from args, no flattening
        }
        if (anon == null) {
            if (a instanceof ZRange) {
                a = ((ZRange) a).asList();
            }
            if (a instanceof Collection) {
                return new ZSet(new LinkedHashSet((Collection) a));
            }
            if (a == null) return Collections.emptySet();
            if (a.getClass().isArray()) {
                return new ZSet(new LinkedHashSet(new ZArray(a, false)));
            }
            return new ZSet(new LinkedHashSet(Arrays.asList(a)));
        }
        final Iterable iterable = Converter.iterable(a, "Type can not be converted to an Ordered Set!");
        return BaseZCollection.set(new ZSet(new LinkedHashSet()), anon, iterable);
    }

    /**
     * Creates a Sorted set
     *
     * @param anons using the optional functions as mapper and comparator for the key
     * @param args  using the args
     * @return a set
     */
    public static Set sortedSet(List<Function> anons, Object... args) {
        if (args.length == 0) return new ZSet(new TreeSet(Arithmetic.INSTANCE));
        if (args.length == 1 && args[0] instanceof Comparator) {
            return new ZSet(new TreeSet((Comparator) args[0]));
        }
        Object a = args[0];
        if (args.length > 1) {
            a = args; // implicit set from args, no flattening
        }
        if (anons.isEmpty()) {
            if (a instanceof ZRange) {
                a = ((ZRange) a).asList();
            }
            if (a instanceof Collection) {
                return new ZSet(new TreeSet((Collection) a));
            }
            if (a == null) return Collections.emptySet();
            if (a.getClass().isArray()) {
                return new ZSet(new TreeSet(new ZArray(a, false)));
            }
            return new ZSet(new TreeSet(Arrays.asList(a)));
        }
        Function anon = Function.COLLECTOR_IDENTITY;
        Comparator cmp = null;
        TreeSet seed;
        for (Function f : anons) {
            if (f instanceof AnonymousFunctionInstance.PredicateLambda) {
                cmp = (Comparator) f;
            } else {
                anon = f;
            }
        }
        if (cmp != null) {
            seed = new TreeSet(cmp);
        } else {
            seed = new TreeSet(Arithmetic.INSTANCE);
        }

        final Iterable iterable = Converter.iterable(a, "Type can not be converted to a Sorted Set!");
        return BaseZCollection.set(new ZSet(seed), anon, iterable);

    }

    /**
     * Creates a set
     */
    public ZSet() {
        super(new HashSet());
    }

    /**
     * Creates a set
     *
     * @param arr from the array argument
     */
    public ZSet(Object arr) {
        super(arr);
        col = new HashSet<>(col);
    }

    /**
     * Creates a set
     *
     * @param c from other set
     */
    public ZSet(Set c) {
        super(c);
    }

    /**
     * Creates a set
     *
     * @param c from other collection
     */
    public ZSet(Collection c) {
        super(c);
        col = new HashSet<>(col);
    }

    @Override
    public Comparator comparator() {
        if (col instanceof SortedSet) {
            return ((SortedSet) col).comparator();
        }
        return null;
    }

    @Override
    public SortedSet subSet(Object fromElement, Object toElement) {
        if (col instanceof SortedSet) {
            return ((SortedSet) col).subSet(fromElement, toElement);
        }
        return EMPTY;
    }

    @Override
    public SortedSet headSet(Object toElement) {
        if (col instanceof SortedSet) {
            return ((SortedSet) col).headSet(toElement);
        }
        return EMPTY;
    }

    @Override
    public SortedSet tailSet(Object fromElement) {
        if (col instanceof SortedSet) {
            return ((SortedSet) col).tailSet(fromElement);
        }
        return EMPTY;
    }

    @Override
    public Object first() {
        if (col instanceof SortedSet) {
            return ((SortedSet) col).first();
        }
        return Function.NIL;
    }


    @Override
    public Object last() {
        if (col instanceof SortedSet) {
            return ((SortedSet) col).last();
        }
        return Function.NIL;
    }

    @Override
    public Object lower(Object o) {
        if (col instanceof NavigableSet) {
            return ((NavigableSet) col).lower(o);
        }
        return Function.NIL;
    }

    @Override
    public Object floor(Object o) {
        if (col instanceof NavigableSet) {
            return ((NavigableSet) col).floor(o);
        }
        return Function.NIL;
    }

    @Override
    public Object ceiling(Object o) {
        if (col instanceof NavigableSet) {
            return ((NavigableSet) col).ceiling(o);
        }
        return Function.NIL;
    }

    @Override
    public Object higher(Object o) {
        if (col instanceof NavigableSet) {
            return ((NavigableSet) col).higher(o);
        }
        return Function.NIL;
    }

    @Override
    public Object pollFirst() {
        if (col instanceof NavigableSet) {
            return ((NavigableSet) col).pollFirst();
        }
        return Function.NIL;
    }

    @Override
    public Object pollLast() {
        if (col instanceof NavigableSet) {
            return ((NavigableSet) col).pollLast();
        }
        return Function.NIL;
    }

    @Override
    public NavigableSet descendingSet() {
        if (col instanceof NavigableSet) {
            return ((NavigableSet) col).descendingSet();
        }
        return EMPTY;
    }

    @Override
    public Iterator descendingIterator() {
        if (col instanceof NavigableSet) {
            return ((NavigableSet) col).descendingIterator();
        }
        return Collections.emptyIterator();
    }

    @Override
    public NavigableSet subSet(Object fromElement, boolean fromInclusive, Object toElement, boolean toInclusive) {
        if (col instanceof NavigableSet) {
            return ((NavigableSet) col).subSet(fromElement, fromInclusive, toElement, toInclusive);
        }
        return EMPTY;
    }

    @Override
    public NavigableSet headSet(Object toElement, boolean inclusive) {
        if (col instanceof NavigableSet) {
            return ((NavigableSet) col).headSet(toElement, inclusive);
        }
        return EMPTY;
    }

    @Override
    public NavigableSet tailSet(Object fromElement, boolean inclusive) {
        if (col instanceof NavigableSet) {
            return ((NavigableSet) col).tailSet(fromElement, inclusive);
        }
        return EMPTY;
    }

    private ZSet fromCollection(Collection c) {
        if (col instanceof LinkedHashSet) return new ZSet(new LinkedHashSet(c));
        return new ZSet(c);
    }

    @Override
    public ZCollection collector() {
        return fromCollection(ZArray.EMPTY_Z_ARRAY);
    }

    @Override
    public Set setCollector() {
        return fromCollection(ZArray.EMPTY_Z_ARRAY);
    }

    @Override
    public ZCollection myCopy() {
        return fromCollection(col);
    }

    @Override
    public String containerFormatString() {
        return "{ %s }";
    }

}
