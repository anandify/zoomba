/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.collections;

import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZRange;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A sorted list Implementation for ZoomBA
 * @param <T> type of the List
 */
public class ZSortedList<T> implements List<T> {

    /**
     * Underlying comparator
     */
    protected final Comparator<T> cmp;

    /**
     * Underlying TreeMap used as bucket
     */
    protected final TreeMap<T,Integer> pq;

    /**
     * Underlying size variable
     */
    protected volatile int _size;

    /**
     * Constructs a ZSortedList
     */
    public ZSortedList(){
        this((Comparator<T>) null);
    }

    /**
     * Constructs a ZSortedList
     * @param cmp with this Comparator
     */
    public ZSortedList(Comparator<T> cmp){
        this.cmp = cmp;
        this.pq = new TreeMap<>( this.cmp );
        _size = 0;
    }

    /**
     * Constructs a ZSortedList
     * @param col with this collection and null Comparator
     */
    public ZSortedList(Collection<T> col){
        this(col, null);
    }

    /**
     * Constructs a ZSortedList
     * @param col with this collection
     * @param cmp using this Comparator
     */
    public ZSortedList(Collection<T> col, Comparator<T> cmp){
        this(cmp);
        addAll(col);
    }

    @Override
    public synchronized int size() {
        return _size;
    }

    @Override
    public synchronized boolean isEmpty() {
        return _size == 0;
    }

    @Override
    public synchronized boolean contains(Object o) {
        return pq.containsKey(o);
    }

    @Override
    public Iterator<T> iterator() {
        return listIterator();
    }

    @Override
    public synchronized Object[]  toArray() {
        Object[] arr = new Object[_size];
        int i = 0;
        for ( Map.Entry<T,Integer> entry: pq.entrySet() ){
            int cnt = entry.getValue();
            T v = entry.getKey();
            while( cnt > 0 ){
                cnt --;
                arr[i] = v;
                i++;
            }
        }
        return arr;
    }

    private List<T> frozenList(){
        return Arrays.asList( (T[])toArray() );
    }

    @Override
    public <V> V[] toArray(V[] a) {
        return  (V[])toArray();
    }

    @Override
    public synchronized boolean add(T t) {
        try {
            pq.compute(t, (k, c) -> c + 1);
        }catch (Throwable ignore){
            pq.put(t,1);
        }
        _size ++;
        return true;
    }

    @Override
    public synchronized boolean remove(Object o) {
        try {
            int c = pq.get(o) - 1;
            if ( c == 0 ){
                pq.remove(o);
            }else{
                pq.put((T)o,c);
            }
            _size --;
            return true;
        }catch (Throwable ignore){
            return false;
        }
    }

    @Override
    public synchronized boolean containsAll(Collection<?> c) {
        if ( isEmpty() ) return false;
        boolean found = true;
        for ( Object o : c ){
            found &= contains(o);
        }
        return found;
    }

    @Override
    public synchronized boolean addAll(Collection<? extends T> c) {
        for ( T t : c ){
            add(t);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw new UnsupportedOperationException("indexed addAll is not supported in Sorted List");
    }

    @Override
    public synchronized boolean removeAll(Collection<?> c) {
        if ( isEmpty() ) return false;
        if ( c.isEmpty() ) return false;
        boolean removed = true;
        for ( Object o : c ){
            removed &= remove(o);
        }
        return removed;
    }

    @Override
    public synchronized boolean retainAll(Collection<?> c) {
        Map<Object,Integer> counter = new HashMap<>();
        for ( Object o : c){
            int i = counter.getOrDefault(o,0);
            counter.put(o,i+1);
        }
        final int prevSize = _size;
        List<T> keySet = pq.keySet().stream().collect(Collectors.toList());
        for ( T k : keySet ){
            final int myFreq = pq.get(k);
            if ( counter.containsKey(k) ){
                int otherFreq = counter.get(k);
                int finalFreq = Math.min( myFreq, otherFreq );
                _size -= ( myFreq - finalFreq );
                pq.put(k,finalFreq);
            }else{
                // this does not exist in other collection
                pq.remove(k);
                _size -= myFreq;
            }
        }
        return prevSize != _size;
    }

    @Override
    public synchronized void clear() {
        pq.clear();
        _size = 0;
    }

    /**
     * Try to use it rarely - this has runtime of O(n) with n is the size of the list
     * @param index index of the element to return
     * @return item if found, or null
     */
    @Override
    public synchronized T get(int index) {
        if ( index < 0 || index >= _size ) throw new IndexOutOfBoundsException(index);
        int i = 0;
        for ( Map.Entry<T,Integer> entry: pq.entrySet() ){
            int cnt = entry.getValue();
            final T v = entry.getKey();
            while( cnt > 0 ){
                cnt --;
                if ( i == index ) return v;
                i++;
            }
        }
        return null;
    }

    @Override
    public T set(int index, T element) {
        throw new UnsupportedOperationException("indexed set is not supported in Sorted List");
    }

    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException("indexed add is not supported in Sorted List");
    }

    @Override
    public synchronized T remove(int index) {
        T v = get(index);
        if( v == null ){
            return null;
        }
        int f = pq.get(v);
        if  ( f == 1 ){
            pq.remove(v);
        }
        _size --;
        return v;
    }


    @Override
    public synchronized int indexOf(Object o) {
        if ( !pq.containsKey(o) ) return -1;

        int i = 0;
        for ( Map.Entry<T,Integer> entry: pq.entrySet() ){
            int cnt = entry.getValue();
            final T v = entry.getKey();
            if ( Objects.equals(v,o)) return i;
            i += cnt;
        }
        return -1;
    }

    @Override
    public synchronized int lastIndexOf(Object o) {
        if ( !pq.containsKey(o) ) return -1;

        int i = 0;
        for ( Map.Entry<T,Integer> entry: pq.entrySet() ){
            int cnt = entry.getValue();
            final T v = entry.getKey();
            if ( Objects.equals(v,o)) return (i+cnt -1);
            i += cnt;
        }
        return -1;
    }

    @Override
    public ListIterator<T> listIterator() {
        return frozenList().listIterator();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return frozenList().listIterator(index);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return frozenList().subList(fromIndex, toIndex);
    }

    public synchronized T getFirst() {
        return pq.firstKey();
    }

    public synchronized T getLast() {
        return pq.lastKey();
    }

    public synchronized T removeFirst() {
        T v = pq.firstKey();
        remove(v);
        return v;
    }

    public synchronized T removeLast() {
        T v = pq.lastKey();
        remove(v);
        return v;
    }

    /**
     * A Default Wrapper  of SortedList implementing ZCollection
     */
    public static class BaseZSortedList extends ZList {

        private final ZSortedList _list ;

        /**
         * Constructs a BaseZSortedList
         */
        public BaseZSortedList(){
            _list = new ZSortedList();
            this.col = _list;
        }

        /**
         * Constructs a BaseZSortedList
         * @param l with the underlying ZSortedList
         */
        public BaseZSortedList( ZSortedList l){
            _list = l;
            this.col = _list;
        }

        /**
         * Constructs a BaseZSortedList
         * @param col with the underlying collection
         */
        public BaseZSortedList( Collection col){
            _list = new ZSortedList( col );
            this.col = _list;
        }

        @Override
        public ZCollection myCopy() {
            ZSortedList clone = new ZSortedList( _list, _list.cmp );
            return new BaseZSortedList( clone );
        }

        @Override
        public Set toSet() {
            return _list.pq.keySet();
        }

        @Override
        public ZCollection collector() {
            return new BaseZSortedList();
        }

        @Override
        public Set setCollector() {
            return new ZSet( new TreeSet( _list.cmp ) );
        }

        /**
         * Creates a BaseZSortedList from
         * @param anons List of anonymous function
         * @param args using the args
         *             either the args is empty - then empty list
         *             the args is a collection, creates a list containing collection
         *             the args themselves are taken as list
         * @return a BaseZSortedList
         */
        public static List list( List<Function> anons, Object... args){
            if ( args.length == 0 ) return new BaseZSortedList() ;
            Object a = args[0];
            if ( args.length > 1 ){
                a = args ; // implicit list from args, no flattening
            }
            if ( anons.isEmpty() ){
                if ( a instanceof ZRange) {
                    return new BaseZSortedList( ((ZRange) a).asList() );
                }
                if ( a instanceof Collection ){
                    return new BaseZSortedList((Collection)a);
                }
                if ( a == null ) return Collections.emptyList();
                return new BaseZSortedList(Arrays.asList(args));
            }
            final Iterable iterable = Converter.iterable(a, "Type can not be converted to a list!");

            final BaseZSortedList acc = new BaseZSortedList();
            final Function anon ;

            if ( anons.size() == 1 ){
                anon = anons.get(0);
            } else {
                Function f = anons.get(0);
                Comparator cmp = new Function.ComparatorLambda( acc, f);
                acc.col = new ZSortedList(cmp);
                anon = anons.get(1);
            }
            return (List) BaseZCollection.map(acc, anon, iterable);
        }
    }
}
