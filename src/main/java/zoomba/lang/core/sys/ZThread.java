/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.sys;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.interpreter.*;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZJVMAccess;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * A Thread wrapper for ZoomBA
 * Capable of returning value back to the main
 * Because it is a MonadicContainer type
 */
public class ZThread extends Thread
        implements Runnable, Function.MonadicContainer, Callable<Function.MonadicContainer> {

    private static final Map<Thread, List<String>> errorStacks = new ConcurrentHashMap<>();

    /**
     * Appends error
     * @param callStack list of string call stack to append to error
     */
    public static void appendError( List<String> callStack ){
      Thread th = Thread.currentThread();
      if ( !errorStacks.containsKey( th ) ){
         errorStacks.put(th,callStack);
      }
    }

    /**
     * Removes and Gets the trace for the thread
     * @return trace - call stack for the current thread
     */
    public static List<String> removeAndGetTrace(){
        Thread th = Thread.currentThread();
        if ( errorStacks.containsKey( th ) ){
            return errorStacks.remove(th);
        }
        return Collections.emptyList();
    }

    private static final Function.MonadicContainer THREAD_RUNNING = new Function.MonadicContainerBase();

    /**
     * Underlying function which would run
     */
    protected Function f;

    /**
     * Current state of the thread
     */
    protected Function.MonadicContainer state;

    /**
     * Argument for the thread
     */
    protected Object threadArg;

    /**
     * Which method to run post completing the thread
     */
    protected Function after;

    /**
     * The call stack which generated this thread
     */
    protected List<String> callStackUptoThread;

    /**
     * In case error was encountered while running the thread
     */
    protected Throwable error;

    /**
     * Underlying system thread
     */
    protected Thread sysThread = this;

    static final class VirtualThreading {
        private static <T> T  safeOrNull( Callable<T> callable) {
            try {
                return callable.call();
            }catch (Throwable t){
                return null;
            }
        }
        private static final Class<?>  threadBuilderInterface = safeOrNull( () -> ZJVMAccess.findClass("java.lang.Thread$Builder") );
        private static final Object vThreadBuilder = safeOrNull(() -> ZJVMAccess.callMethod(Thread.class, "ofVirtual", new Object[]{}));
        private static final Object pThreadBuilder = safeOrNull(() -> ZJVMAccess.callMethod(Thread.class, "ofPlatform", new Object[]{}));
        private static final Method startMethod = safeOrNull( () -> ZJVMAccess.findMatchingMethod( threadBuilderInterface,  (m) -> "start".equals(m.getName())));
        private static final Method unStartedMethod = safeOrNull( () -> ZJVMAccess.findMatchingMethod( threadBuilderInterface,  (m) -> "unstarted".equals(m.getName())));

        private static final Method virtualExecutorService = safeOrNull( () -> ZJVMAccess.findMatchingMethod(
                Executors.class, (m)-> "newVirtualThreadPerTaskExecutor".equals(m.getName())));

        public static ExecutorService newVirtualExecutor(){
           return  safeOrNull( () -> (ExecutorService) virtualExecutorService.invoke(Executors.class, new Object[]{}) );
        }

        public static final boolean IS_SUPPORTED = ( vThreadBuilder != null ) && ( pThreadBuilder != null ) &&
                ( startMethod != null ) && ( unStartedMethod != null );

        public static ZThread thread(final boolean virtual, final ZThread runnable, final boolean startNow){
            if ( IS_SUPPORTED ){ // Java 21+
                final Object threadBuilder = virtual ? vThreadBuilder : pThreadBuilder ;
                final Method called = startNow ? startMethod : unStartedMethod ;
                runnable.sysThread = safeOrNull( () -> (Thread) called.invoke(threadBuilder, runnable) );
            }
            else if ( startNow ){
                runnable.sysThread = runnable;
                runnable.start();
            }
            // Log error in case of use of virtual thread when there is none
            if ( virtual && !IS_SUPPORTED  ){ System.err.println("No virtual thread support - routing to system thread!");}
            return runnable;
        }
    }

    /**
     * Gets a thread
     * @param virtual whether using virtual thread
     * @param startNow whether to start immediately upon creation
     * @return a ZThread
     */
    public ZThread thread(boolean virtual, boolean startNow){
        return VirtualThreading.thread(virtual, this, startNow);
    }

    private void setupCallStack(){
        if  ( f instanceof AnonymousFunctionInstance ){
            callStackUptoThread = new ArrayList<>( ((AnonymousFunctionInstance) f).tlZInterpret.get().callStack());
        } else {
            callStackUptoThread = new ArrayList<>(((ZScriptMethod.ZScriptMethodInstance) f).interpret.callStack());
        }
    }

    /**
     * Creates a Thread using
     * @param threadFunction the function to be used as thread
     * @param arg arguments to the thread
     */
    public ZThread(Function threadFunction, Object... arg){
        this.f = threadFunction ;
        setupCallStack();
        // wrap it...
        threadArg = arg ;
        state  =  THREAD_RUNNING ;
        after = Function.NOP ;
    }

    /**
     * Creates a thread with
     * @param threadFunction the thread function
     * @param after a callback function once the thread is done executing
     */
    public ZThread(Function threadFunction, Function after){
        this.f = threadFunction ;
        setupCallStack();
        // wrap it...
        threadArg = ZArray.EMPTY_ARRAY  ;
        state  =  THREAD_RUNNING ;
        this.after = after ;
    }

    @Override
    public boolean isNil() {
        return state.isNil() ;
    }

    @Override
    public Object value() {
        return state.value();
    }

    @Override
    public Function.MonadicContainer call() throws Exception {
        run(); // do not replace with start(), we do not want to start the thread here...
        if ( error != null ){
            throw new Exception(error);
        }
        return state;
    }

    /**
     * Prints the stack trace for the thread
     * @param thread the underlying thread
     * @param throwable for the error
     */
    protected void printDebugStack(Thread thread, Throwable throwable){
        List<String> stack = errorStacks.getOrDefault(thread, Collections.emptyList());
        System.err.printf("Error in Thread-id %d %n", Thread.currentThread().getId());
        System.err.println( throwable.toString() );
        if ( !stack.isEmpty() ){
            System.err.println( ZTypes.string(stack, "\n"));
        }
        System.err.println( ZTypes.string(callStackUptoThread, "\n"));
        System.err.println(".....");
    }

    private void setNamedArgContext(){
        if ( threadArg == null || !threadArg.getClass().isArray() ) return ;
        Object[] a = (Object[]) threadArg;
        if ( a.length == 1 && a[0] instanceof Function.NamedArgs ){
            final ZContext parent;
            if ( f instanceof AnonymousFunctionInstance ){
                parent = ((AnonymousFunctionInstance) f).tlZInterpret.get().context();
            } else {
                parent = ZContext.EMPTY_CONTEXT ;
            }
            ZContext.FunctionContext fc = new ZContext.FunctionContext( parent,
                    ZContext.ArgContext.EMPTY_ARGS_CONTEXT);
            fc.map.putAll((Map)a[0]);
            f.runContext(fc) ;
        }
    }

    /**
     * Is the thread actually running
     * @return true if it is false otherwise
     */
    public boolean isRunning(){ return state == THREAD_RUNNING; } // our implementation wise

    /**
     * Has the thread finished running ( even with error or whatever )
     * @return true if it did, false otherwise
     */
    public boolean isFinished(){ return getState() == State.TERMINATED ; } // JVM wise

    @Override
    public void run() {
        error = null;
        Thread thread = Thread.currentThread();
        // process args...
        Object[] args = new Object[4];
        // get the id
        args[0] = getId();
        // current
        args[1] = this ;
        // the args are the context
        args[2] = threadArg ;
        setNamedArgContext();
        // there is no partial
        args[3] = Function.NIL;
        try {
            state = f.execute(args);
        }catch (Throwable throwable){
            error = throwable ;
            state = new Function.MonadicContainerBase(throwable);
            printDebugStack(thread, throwable );
        }finally {
            after.execute( state.value() );
            errorStacks.remove(thread);
        }

    }

    /**
     * Runs a function in asynchronous mode
     * @param threadFunction the function to be run
     * @param after the callback function
     * @return a Runnable which is running
     */
    public static Runnable async( Function threadFunction, Function after){
        Thread t = new ZThread(threadFunction, after );
        t.start();
        return t;
    }

    /**
     * Gets an executor
     * @param poolSize size of the thread pool
     * @return an ExecutorService
     */
    public static ExecutorService getExecutor(int poolSize){
        if ( VirtualThreading.IS_SUPPORTED && poolSize < 0 ) {
            return VirtualThreading.newVirtualExecutor();
        }
        if ( poolSize <= 0 || poolSize > 64 ){ // how high is too high?
            return Executors.newCachedThreadPool();
        }
        return Executors.newFixedThreadPool( poolSize );
    }

    /**
     * A class to configure the thread pool and ExecutorService
     */
    public static final class SchedulerInputConfig {

        /**
         * Underlying ExecutorService
         */
        public ExecutorService executorService = null ;

        /**
         * Size of the thread pool
         */
        public int poolSize = 0;

        /**
         * Timeout for the task
         */
        public long taskTimeout = Long.MAX_VALUE ;

        /**
         * Should we wait for the tasks to complete
         * Checks whether taskTimeout greater than 0 or not
         * @return true if we should false otherwise
         */
        public boolean waitForTasksToComplete(){ return taskTimeout > 0; }

        /**
         * Time to shut down the ExecutorService
         */
        public long shutDownTimeout = Long.MAX_VALUE ;

        /**
         * Whether the system would wait for shutdown
         * Checks whether shutDownTimeout greater than 0 or not
         * @return true if we should false otherwise
         */
        public boolean shutDown() { return  shutDownTimeout > 0; }

        /**
         * Which tasks are associated
         * A list of tasks
         */
        public List<Callable<Object>> tasks = new ArrayList<>();

        /**
         * Creates a configuration
         * @param interpret with this underlying interpreter
         * @param anon using this method as the only task
         * @param args arguments
         * @return a SchedulerInputConfig
         */
        public static SchedulerInputConfig fromArgs(ZInterpret interpret, Function anon, Object[] args ){
            final SchedulerInputConfig config = new SchedulerInputConfig();
            if ( args[0] instanceof List ){
                config.tasks = (List<Callable<Object>>) args[0];
            } else if ( args[0] instanceof ExecutorService ){
                config.executorService = (ExecutorService) args[0];
            } else {
                throw new IllegalArgumentException("Only Named Arguments are allowed post first parameter in batch!");
            }
            // Process args
            args = ZTypes.shiftArgsLeft(args);
            args = ZMethodInterceptor.Default.namedArgs(interpret, args);

            if ( anon != null ){
                Callable callable = new ZThread( anon, args ) ;
                config.tasks.add( callable);
                config.shutDownTimeout = -1;
                config.taskTimeout = -1;
            }
            // Now final processing
            if ( args.length > 0 && args[0] instanceof Function.NamedArgs ){
                Function.NamedArgs namedArgs = (Function.NamedArgs) args[0];
                namedArgs.doWhenPresent( "service", (key,value)-> config.executorService = (ExecutorService) value );
                namedArgs.doWhenPresent( "workers", (key,value)-> config.poolSize = ZNumber.integer(value, config.poolSize).intValue() );
                namedArgs.doWhenPresent( "wait", (key,value)-> config.taskTimeout = ZNumber.integer(value, config.taskTimeout).longValue() );
                namedArgs.doWhenPresent( "shutdown", (key,value)-> config.shutDownTimeout = ZNumber.integer(value, config.shutDownTimeout).longValue() );
                namedArgs.doWhenPresent( "tasks", (key,value)-> config.tasks = (List) value );

            }
            return config;
        }
    }

    /**
     * Schedules a list of functions as tasks with an Executor Service
     * @param config configuration for the batch processing
     * @return List of @{Function.MonadicContainer} for the function responses
     */
    public static List<?>  schedule(SchedulerInputConfig config ){
        ExecutorService scheduler = config.executorService == null ? getExecutor(config.poolSize) : config.executorService ;
        try {
            List<Future<Object>> futures;
            if ( config.waitForTasksToComplete() ) {
                futures = scheduler.invokeAll(config.tasks, config.taskTimeout, TimeUnit.MILLISECONDS);
                return futures.stream().map(future -> {
                    try {
                        return future.get(config.taskTimeout, TimeUnit.MILLISECONDS);
                    } catch (Throwable e) {
                        return new ZException.MonadicException(e);
                    }
                }).collect(Collectors.toList());
            } else {
                futures = config.tasks.stream().map(scheduler::submit).collect(Collectors.toList());
            }
            return futures;
        }catch (InterruptedException ie){

        } finally {
            if ( config.shutDown() ) {
                VirtualThreading.safeOrNull(() ->{
                    scheduler.shutdown();
                    return scheduler.awaitTermination( config.shutDownTimeout, TimeUnit.MILLISECONDS);
                });
            }
        }
        return Collections.emptyList();
    }

    /**
     * Poll until predicate is true
     * @param predicate the condition
     * @param args integers on no of loops and poll interval
     * @return true when polling successful false otherwise
     */
    public static boolean poll( Function predicate, Object[] args){
        if ( predicate == null ){
            predicate = Function.TRUE ;
        }
        int maxLoop = 42 ; // no of times
        int pollInterval = 420 ; // ms
        if ( args.length > 0 ){
            maxLoop = ZNumber.integer( args[0] , maxLoop ).intValue() ;
            if ( args.length > 1 ){
                pollInterval = ZNumber.integer( args[1] , pollInterval ).intValue() ;
            }
        }
        while( true ) {
            maxLoop--;
            Function.MonadicContainer container = predicate.execute( );
            if (ZTypes.bool( container.value() , false) ){
                return true ;
            }
            if ( maxLoop == 0 ) return false ;
            try { Thread.sleep( pollInterval ); } catch (Throwable t){}
        }
    }
}
