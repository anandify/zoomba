/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;

import static zoomba.lang.core.operations.Function.NOTHING;
import static zoomba.lang.core.types.ZException.Return.RETURN ;
import java.util.EventObject;

/**
 * Eventing framework for ZoomBA
 */
public interface ZEventAware {

    /**
     * Before or After the Method call
     */
    enum When {
        /**
         * Event for before method call
         */
        BEFORE,
        /**
         * Event for after method call
         */
        AFTER
    }

    /**
     * Event Object for ZoomBA events
     */
    class ZEventArg extends EventObject{

        /**
         * When is this event args generated
         */
        public final When when;

        /**
         * What is the method which was involved in the event
         */
        public final Object method;

        /**
         * What arguments were passed in the method
         */
        public final ZArray args;

        /**
         * The result of the function
         */
        public final Function.MonadicContainer result;

        /**
         * Did invoking the method generated error, then it stores
         */
        public final Throwable error;

        /**
         * Creates an event from
         * @param source the source object
         * @param when before or after the method
         * @param method the method
         * @param args the arguments
         */
        public ZEventArg(Object source, When when, Object method, Object[] args) {
            this( source, when, method, args, NOTHING , RETURN );
        }

        /**
         * Creates an event from
         * @param source the source object
         * @param when before or after the method
         * @param method the method
         * @param args the arguments
         * @param r the optional return value from the method intercepted
         * @param error the error which was involved
         */
        public ZEventArg(Object source, When when, Object method, Object[] args, Function.MonadicContainer r, Throwable error) {
            super(source);
            this.when = when;
            this.method = method ;
            this.args = new ZArray(args,false) ;
            this.result = r;
            this.error = error ;
        }

        @Override
        public String toString() {
            return String.format(
                    "{ \"w\" : \"%s\", \"m\" : \"%s\" , \"a\" : %s , \"r\" : \"%s\", \"e\" : \"%s\"", when,
                    method , args.string(true) ,
                    result.isNil() ? "" : ZTypes.string(result.value(),true),
                    errored() ? error : "" );
        }

        /**
         * Has the function errored ?
         * @return true if it did, false if returned normally or did not get called
         */
        public boolean errored(){
            return !(error instanceof ZException.Return) ;
        }
    }

    /**
     * Gets invoked before calling a function
     * @param before the event object
     */
    void before(ZEventArg before);

    /**
     * Gets invoked after calling of a function
     * @param after the event object
     */
    void after(ZEventArg after);

}
