/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * <a href="https://stackoverflow.com/questions/28400408/what-is-the-new-way-of-getting-all-methods-of-a-class-including-inherited-defau">...</a>
 *
 */
public final class MethodCache{

    /**
     * Limit of cache to be used for cache
     */
    public static int LIMIT = 32;

    private static final Map<Class<?>,MethodCache> DEEP_CACHE = new LinkedHashMap<>() {
        @Override
        protected boolean removeEldestEntry(Map.Entry eldest) {
            return size() > LIMIT;
        }
    };

    private static final Map<Class<?>,MethodCache> NARROW_CACHE = new LinkedHashMap<>() {
        @Override
        protected boolean removeEldestEntry(Map.Entry eldest) {
            return size() > LIMIT;
        }
    };

    /**
     * Flattened up List of Method for the class
     */
    public final List<Method> methodList;

    /**
     *  Method for the class grouped by name - overloads by name
     */
    public final Map<String,List<Method>> overLoads;

    MethodCache(List<Method> list, Map<String,List<Method>> grp){
        this.methodList =  list ;
        this.overLoads = grp;
    }

    /**
     * Creates a MethodCache
     * @param methodSet bunch of methods of same class
     * @return a MethodCache
     */
    public static MethodCache cache(Set<Method> methodSet){
        Map<String,List<Method>> grp = new HashMap<>();
        List<Method> methods = new ArrayList<>(methodSet.size());
        for ( Method m : methodSet ){
            try {
                m.setAccessible(true);
                methods.add(m);
                final String name = m.getName();
                try {
                    grp.get(name).add(m);
                }catch (NullPointerException npe){
                    List<Method> list = new ArrayList<>();
                    list.add(m);
                    grp.put(name, list);
                }
            }catch (Throwable ignored){}
        }
        return new MethodCache(methods, grp);
    }

    private static boolean noPkgOverride(
            Method m, Map<Object, Set<Package>> types, Set<Package> pkgIndependent) {
        Set<Package> pkg = types.computeIfAbsent(methodKey(m), key -> new HashSet<>());
        return pkg != pkgIndependent && pkg.add(m.getDeclaringClass().getPackage());
    }

    private static Object methodKey(Method m) {
        return Arrays.asList(m.getName(),
                MethodType.methodType(m.getReturnType(), m.getParameterTypes()));
    }

    /**
     * Gets all the methods from the class
     * @param clazz                                            the class
     * @param includeAllPackageAndPrivateMethodsOfSuperclasses naturally
     * @param includeOverridenAndHidden                        naturally
     * @return a MethodGroup of all methods
     */
    public static MethodCache getAllMethods(Class<?> clazz,
                                            boolean includeAllPackageAndPrivateMethodsOfSuperclasses,
                                            boolean includeOverridenAndHidden) {

        final boolean deep = includeAllPackageAndPrivateMethodsOfSuperclasses && includeOverridenAndHidden ;
        if ( deep ){
            if (DEEP_CACHE.containsKey(clazz)) return DEEP_CACHE.get(clazz) ;
        } else {
            if (NARROW_CACHE.containsKey(clazz)) return NARROW_CACHE.get(clazz) ;
        }

        Predicate<Method> include = m -> !m.isBridge() && !m.isSynthetic() &&
                Character.isJavaIdentifierStart(m.getName().charAt(0))
                && m.getName().chars().skip(1).allMatch(Character::isJavaIdentifierPart);

        Set<Method> methods = new HashSet<>();
        Collections.addAll(methods, clazz.getMethods());
        methods.removeIf(include.negate());
        Stream.of(clazz.getDeclaredMethods()).filter(include).forEach(methods::add);

        final int access = Modifier.PUBLIC | Modifier.PROTECTED | Modifier.PRIVATE;

        Package p = clazz.getPackage();
        if (!includeAllPackageAndPrivateMethodsOfSuperclasses) {
            int pass = includeOverridenAndHidden ?
                    Modifier.PUBLIC | Modifier.PROTECTED : Modifier.PROTECTED;
            include = include.and(m -> {
                int mod = m.getModifiers();
                return (mod & pass) != 0
                        || (mod & access) == 0 && m.getDeclaringClass().getPackage() == p;
            });
        }
        if (!includeOverridenAndHidden) {
            Map<Object, Set<Package>> types = new HashMap<>();
            final Set<Package> pkgIndependent = Collections.emptySet();
            for (Method m : methods) {
                int acc = m.getModifiers() & access;
                if (acc == Modifier.PRIVATE) continue;
                if (acc != 0) types.put(methodKey(m), pkgIndependent);
                else types.computeIfAbsent(methodKey(m), x -> new HashSet<>()).add(p);
            }
            include = include.and(m -> {
                int acc = m.getModifiers() & access;
                return acc != 0 ? acc == Modifier.PRIVATE
                        || types.putIfAbsent(methodKey(m), pkgIndependent) == null :
                        noPkgOverride(m, types, pkgIndependent);
            });
        }
        final Class<?> originalClass = clazz;
        for (clazz = clazz.getSuperclass(); clazz != null; clazz = clazz.getSuperclass())
            Stream.of(clazz.getDeclaredMethods()).filter(include).forEach(methods::add);
        final MethodCache methodCache = MethodCache.cache ( methods );
        if ( deep ){
            DEEP_CACHE.put(originalClass, methodCache) ;
        } else {
            NARROW_CACHE.put(originalClass, methodCache) ;
        }
        return methodCache;
    }
}

