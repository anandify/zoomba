/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;


import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Implementation of things which are retry-able
 */
public interface Retry {


    /**
     * Can we retry again?
     * This MUST be immutable function
     * @return true if we can , false if we can not
     */
    boolean can();

    /**
     * This is where one must update the state of the Retry
     * @param currentNumberOfFailures current no of retries to be set by the decorator
     */
    void numTries(int currentNumberOfFailures);

    /**
     * Gets the current retry state
     * -1 : Not initialized
     *  0 : First try
     *  1,2,... : Retry count
     * @return Retry state
     */
    default int tries(){
        return 0;
    }

    /**
     * interval of wait between two successive tries
     * @return interval of time in ms
     */
    long interval();

    /**
     * Gets the tracing - if this retry would trace out errors
     * @return true if trace is on, false if trace is off
     */
    default boolean trace(){ return false; }

    /**
     * Sets On/Off the tracing
     * @param enable if true, enables it, else disables
     */
    default void trace(boolean enable){}

    /**
     * A retry to ensure no retry
     */
    Retry NOP = new Retry() {
        @Override
        public boolean can() {
            return false;
        }
        @Override
        public void numTries(int currentNumberOfFailures) {
        }
        @Override
        public long interval() {
            return Long.MAX_VALUE;
        }
    } ;


    /**
     * A Failure in Retry
     */
    interface Failure{
        /**
         * Gets the time stamp of the failure time
         * @return time stamp in nano sec
         */
        long when();

        /**
         * Gets the underlying error
         * @return a Throwable error
         */
        Throwable error();

        /**
         * Creates a Failure instance from error
         * @param t underlying Throwable error
         * @return Failure instance
         */
        static Failure failure(Throwable t){
            return new Failure() {
                final long ts = System.nanoTime();
                @Override
                public long when() {
                    return ts;
                }
                @Override
                public Throwable error() {
                    return t;
                }
            };
        }
    }

    /**
     * A Special Exception with some special properties for failure in Retry
     */
    class MaximumRetryExceededException extends RuntimeException {

        /**
         * Various failures which happened during the course of tries
         */
        public final List<Failure> failures;

        /**
         * Underlying Retry which failed
         */
        public final Retry retry;

        /**
         * Creates MaximumRetryExceededException from
         * @param retry underlying retry
         * @param causes underlying Failure encountered during the execution
         */
        public MaximumRetryExceededException( Retry retry, List<Failure> causes){
            super();
            failures = Collections.unmodifiableList(causes);
            this.retry = retry;
        }

        @Override
        public String toString() {
            Map<String,Object> map = Map.of( "failures",
                    failures.stream().map( f -> Map.of("t", f.when(), "e" , f.error() ) ).collect(Collectors.toList()),
                    "retry", retry.toString(),
                    "numTries", failures.size() );
            return ZTypes.jsonString(map);
        }

        @Override
        public String getMessage() {
            return "Retry exceeded : " + this;
        }
    }

    /**
     * In case an error needs to get bailed out, we need a handler that gets invoked
     * After every time a try in retry fails
     */
    interface ErrorHandler {

        /**
         * Special Error class which gets raised in case an ErrorHandler chose to abort the retries
         */
        class Abort extends RuntimeException {
            /**
             * The source Retry, which is being aborted
             */
            public final Retry source;

            /**
             * Constructor of Abort
             * @param source retry which is being aborted
             * @param parent underlying exception that happened which raised the error handler
             */
            public Abort(Retry source, Throwable parent){
                super(parent);
                this.source = source;
            }
        }

        /**
         * Handler of any try error in retries
         * @param source - the Retry object
         * @param err - what error was raised while trying
         * @param numTries - the current try number
         * @return if we want to abort immediately return true, else return false
         */
        boolean onError(Retry source, Throwable err, int numTries);

        /**
         * The error handler, which traces out any error in Std Out
         */
        ErrorHandler TRACE = (source, err, numTries) -> {
            System.err.printf("%s @ Iteration (%d)-> %s %n", source, numTries, err );
            return false;
        };

        private static ErrorHandler fromZFunction(zoomba.lang.core.operations.Function function){
            return new ErrorHandler() {
                final zoomba.lang.core.operations.Function.Predicate // cache it through for perf
                        predicate = zoomba.lang.core.operations.Function.Predicate.from(function);
                @Override
                public boolean onError(Retry source, Throwable err, int numTries) {
                    return predicate.accept(err,numTries,source);
                }
            };
        }

        private static ErrorHandler fromObject( Object obj){
            if ( obj instanceof ErrorHandler ) return (ErrorHandler) obj;
            if ( obj instanceof zoomba.lang.core.operations.Function) return fromZFunction ( (zoomba.lang.core.operations.Function) obj );
            return Retry.NOP.errorHandler();
        }
    }

    /**
     * Associated ErrorHandler for the Retry
     * @return associated ErrorHandler
     */
    default ErrorHandler errorHandler(){
        return (source, err, numTries) -> false ;
    }

    /**
     * Sets the Associated ErrorHandler for the Retry
     * @param newHandler  the error handler which needs to be pushed
     */
    default void errorHandler( ErrorHandler newHandler){}

    /**
     * Handles an error raised while trying
     * @param throwable currently raised error
     * @param numTries current no of tries
     * @return if we want to abort immediately return true, else return false
     */
    default boolean handleError(Throwable throwable, int numTries){
        if ( trace() ){
            ErrorHandler.TRACE.onError(this, throwable, numTries);
        }
        return errorHandler().onError(this, throwable, numTries);
    }

    /**
     * Creates a Decorator with inner function
     * @param function underlying function which is to be decorated for retries
     * @return a decorated function which automatically does retries
     * @param <T> input type to the Function
     * @param <R> output type of the function
     */
    default  <T,R> Function<T,R>  function( Function<T,R> function){
        // optimization trick : if no retry, do not wrap the stuff...
        if ( !can() ) return function;
        // else do wrapping up
        return t -> {
            int numTries = 0;
            // initialize
            List<Failure> failures = new ArrayList<>();
            numTries(numTries);
            while( true ){
                try {
                    return function.apply(t);
                }catch (Throwable th){
                    if ( handleError( th, numTries ) ){
                        // this returned true, now we need to abort...
                        throw new ErrorHandler.Abort( this, th);
                    }
                    failures.add(Failure.failure( th) );
                    numTries++;
                    numTries(numTries);
                    if (!can()){ // exit immediately, no stray wait...
                        throw new MaximumRetryExceededException( this, failures);
                    }
                    try {
                        Thread.sleep(interval());
                    }catch (InterruptedException e){
                        throw new RuntimeException( new TimeoutException("Possible Timeout Not sure..."));
                    }
                }
            }
        };
    }

    /**
     * Creates a callable out of the underlying retry mechanism
     * @param callable the one to wrap around
     * @return a retirable Callable
     * @param <T> type of the Callable
     */
    default <T> Callable<T> callable(Callable<T> callable ){
        Function<Object, T> wrapper = (x) -> {
            try {
                return callable.call();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
        Function<Object,T> wrapped = function(wrapper);
        return () -> wrapped.apply(null);
    }

    /**
     * Creates a retriable predicate
     * @param predicate the one to wrap around
     * @return a retirable predicate
     * @param <T> type of the predicate
     */
    default <T> Predicate<T> predicate(Predicate<T> predicate ){
        Function<T, Boolean> wrapper = predicate::test;
        Function<T,Boolean> wrapped = function(wrapper);
        return wrapped::apply;
    }


    /**
     * A Random generator for various purposes
     */
    Random random = new SecureRandom();

    /**
     * A Counter Retry Strategy
     */
    class CounterStrategy implements Retry {
        /**
         * Maximum no of retries
         */
        public final int maxRetries;

        /**
         * Interval in ms between retries
         */
        public final long interval ;

        /**
         * current no of retries
         */
        protected int currentState = -1;

        @Override
        public int tries(){
            return currentState;
        }

        /**
         * whether trace is enabled or not
         */
        protected boolean trace = false;

        @Override
        public boolean trace() {
            return trace;
        }
        @Override
        public void trace(boolean enable) {
            trace = enable;
        }

        /**
         * Underlying error handler for the retry
         */
        protected ErrorHandler errorHandler = Retry.super.errorHandler();
        @Override
        public ErrorHandler errorHandler() {
            return errorHandler;
        }

        @Override
        public void errorHandler(ErrorHandler newHandler) {
            this.errorHandler = newHandler ;
        }

        /**
         * Constructs a Counter Strategy
         * @param maxRetries maximum no of retries
         * @param interval in ms between retries
         */
        public CounterStrategy( int maxRetries, long interval ){
            this.maxRetries = maxRetries;
            this.interval = interval;
        }

        @Override
        public boolean can() {
            return currentState <= maxRetries ;
        }

        @Override
        public void numTries(int currentNumberOfFailures) {
            currentState = currentNumberOfFailures;
        }

        @Override
        public long interval() {
            return interval;
        }

        @Override
        public String toString() {
            Map<String,Object> map = new HashMap<>();
            map.put(MAX, maxRetries);
            map.put(INTERVAL, interval);
            map.put(STRATEGY, getClass().getName());
            return ZTypes.jsonString(map);
        }
    }

    /**
     * A counter based Retry
     * @param maxRetries maximum tries
     * @param interval wait time between the tries
     * @return a Retry algorithm
     */
    static Retry counter( int maxRetries, long interval) {
        return new CounterStrategy(maxRetries,interval);
    }

    /**
     * A Retry that has random interval spacing
     * @param maxRetries maximum tries
     * @param avgInterval avg wait time between the tries
     * @return a Retry algorithm
     */
    static Retry randomize( int maxRetries, long avgInterval) {

        return new CounterStrategy(maxRetries,avgInterval){
            @Override
            public long interval() {
                long half = avgInterval / 2 ;
                return half + ZRandom.RANDOM.numRange(
                        new ZNumber(half), new ZNumber(half + avgInterval )
                ).longValue();
            }
        };
    }

    /**
     * A Retry that has  interval spacing increasing exponentially as time progresses
     * @param maxRetries maximum tries
     * @param startInterval  initial time gap after the first failure
     * @return a Retry algorithm
     */
    static Retry exponentialBackOff( int maxRetries, long startInterval) {

        return new CounterStrategy(maxRetries, startInterval ){
            long curInterval = startInterval;

            @Override
            public void numTries(int currentNumberOfFailures) {
                super.numTries(currentNumberOfFailures);
                curInterval = (long)(startInterval * Math.exp( currentNumberOfFailures - 1));
            }
            @Override
            public long interval() {
                return curInterval;
            }
        };
    }


    /**
     * Key for the Retry Strategy in Config
     */
    String STRATEGY = "strategy" ;

    /**
     * Key for the Maximum No of  Retry in Config
     */
    String MAX = "max" ;

    /**
     * Key for the Interval in ms of the Retry  in Config
     */
    String INTERVAL  = "interval";

    /**
     * Key for the tracing enabling of the Retry in Config
     */
    String TRACING  = "debug";

    /**
     * Key for the error handler for the Retry in Config
     */
    String HANDLER  = "err";


    /**
     * Counter Retry Strategy Alias
     */
    String COUNTER = "counter" ;

    /**
     * Exponential Backoff Retry Strategy Alias
     */
    String EXP = "exp" ;

    /**
     * Random Retry Strategy Alias
     */
    String RANDOM = "random" ;

    /**
     * Creates a Retry Strategy out of a Configuration
     * Typical configuration is as follows:
     * { "strategy" : "counter", "max" : 3, "interval" : 3000, "debug" : true }
     * @param config a configuration map
     * @return a Retry Strategy
     */
    static Retry fromConfig(Map<String,Object> config){
        if ( config.isEmpty() ) return NOP;
        final String strategy = config.getOrDefault(STRATEGY, COUNTER).toString();
        final int maxRetries = ZNumber.integer(config.getOrDefault(MAX, 0),0).intValue();
        final long interval = ZNumber.integer(config.getOrDefault(INTERVAL, Long.MAX_VALUE),0).longValue();
        final boolean enableTracing = ZTypes.bool( config.getOrDefault(TRACING, false),false ).booleanValue();
        final ErrorHandler handler = ErrorHandler.fromObject( config.get(HANDLER) );

        final Retry retry;
        switch (strategy.toLowerCase(Locale.ROOT)) {
            case EXP :
                retry = exponentialBackOff(maxRetries, interval);
                break;
            case RANDOM :
                retry = randomize(maxRetries, interval);
                break;
            case COUNTER:
                retry = counter(maxRetries,interval);
                break;
            default:
                retry = NOP;
        }
        retry.trace( enableTracing );
        retry.errorHandler(handler);
        return retry;
    }
}
