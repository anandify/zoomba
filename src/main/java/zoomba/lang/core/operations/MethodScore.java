/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package zoomba.lang.core.operations;

import org.jscience.mathematics.number.LargeInteger;
import org.jscience.mathematics.number.Real;
import zoomba.lang.core.types.ZException;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * Method scoring
 * <a href="https://docs.oracle.com/javase/specs/jls/se8/html/jls-15.html#jls-15.12.2.4">...</a>
 *
 */
public class MethodScore implements Comparable<MethodScore>{

    static final class ScoreKey{
        public final Class<?> clazz;
        public final String methodName;
        public final Class<?>[] argTypes;
        public final int hashCode;

        ScoreKey(Class<?> clazz, String methodName, Object[] args){
            this.clazz = clazz;
            this.methodName = methodName;
            int code = 31 * clazz.hashCode() + methodName.hashCode() ;
            this.argTypes = new Class<?>[args.length] ;
            for ( int i=0; i< args.length; i++ ){
                code = code * 31;
                if( args[i] == null ){
                    argTypes[i] = null ;
                } else {
                    argTypes[i] = args[i].getClass();
                    code += argTypes[i].hashCode();
                }
            }
            hashCode = code;
        }

        @Override
        public int hashCode() { return hashCode; }

        @Override
        public boolean equals(Object obj) {
            try {
                if ( this == obj ) return true;
                ScoreKey other = ((ScoreKey) obj);
                return this.clazz == other.clazz && this.methodName.equals(other.methodName) &&
                        Arrays.equals(this.argTypes, other.argTypes);
            }catch (Throwable th){
                return false;
            }
        }
    }

    /**
     * Limit of cache to be used for args parameter caching in case cache will be used
     */
    public static int LIMIT = 32;

    private static final Map<ScoreKey,PriorityQueue<MethodScore>> CACHE = new LinkedHashMap<>() {
        @Override
        protected boolean removeEldestEntry(Map.Entry eldest) {
            return size() > LIMIT;
        }
    };

    /**
     * Underlying method
     */
    public final Method method;

    /**
     * Underlying Constructor
     */
    public final Constructor<?> constructor;

    /**
     * Underlying computer score for the args for the method or constructor
     */
    public final int score;

    /**
     * Is the underlying method/constructor variable length args ? then yes, else false
     */
    public final boolean isVarArgs;

    /**
     * Scores primitive matches
     * @param pType parameter types found from underlying method / constructor
     * @param aType actual argument types
     * @return a score more low the better match, higher up, badder match
     */
    public static int scorePrimitiveMatch(Class<?> pType, Class<?> aType) {
        if (pType == boolean.class && aType == Boolean.class) return -1;
        if (Number.class.isAssignableFrom(aType)) {
            // Double
            if ( aType == Double.class && pType == double.class ) return -3;
            if ( aType == Double.class && pType == float.class ) return -2;
            // Float
            if ( aType == Float.class && pType == float.class ) return -3;
            if ( aType == Float.class && pType == double.class ) return -2;

            // Int
            if ( aType == Integer.class && pType == int.class ) return -3;
            if ( aType == Integer.class && pType == long.class ) return -2;

            // Long
            if ( aType == Long.class && pType == long.class ) return -3;
            if ( aType == Long.class && pType == int.class ) return -2;

            if ( aType == Real.class && pType == double.class ) return -2;
            if ( aType == LargeInteger.class && pType == long.class ) return -2;

            if ( aType == BigDecimal.class && pType == double.class ) return -2;
            if ( aType == BigInteger.class && pType == long.class ) return -2;

            if ( pType == int.class || pType == long.class ||
                    pType == short.class || pType == byte.class ||
                    pType == float.class || pType == double.class) {
                return -1;
            }
        }
        if (pType == char.class && aType == Character.class) return -1;
        return IMPOSSIBLE;
    }

    /**
     * An underlying constant to score impossible matches
     */
    public static final int IMPOSSIBLE = 42424242;

    /**
     * Tries to do JVM method resolution of sorts by producing score for a method
     * @param pTypes the method parameter types which needs to be scored against a bunch of parameters
     * @param isVarArgs  is the method variable length args
     * @param args arguments against the scoring would be performed
     * @return score of the method against the args, lower the better ( Comparator logic )
     */
    public static int parameterScore(Class<?>[] pTypes, boolean isVarArgs , Object[] args) {
        // var args can be empty, which means the following
        final int mandatoryParams = isVarArgs ? pTypes.length - 1 : pTypes.length;
        // this is impossible, we are at wrong method
        if ( args.length < mandatoryParams || (!isVarArgs &&  args.length != pTypes.length ) ) return IMPOSSIBLE;
        int score = 0;

        for (int i = 0; i < mandatoryParams; i++) {
            if (args[i] == null) { // there is no way to cast a null to a primitive type, hence..
                if ( pTypes[i].isPrimitive() ) return IMPOSSIBLE;
                continue; // this makes sense, cause any Boxed Object can be null, really... so..???
            }
            Class<?> pType = pTypes[i];
            Class<?> aType = args[i].getClass();
            if (pType.isPrimitive() ) { // start with primitive, else all Boxed will match Object
                score += scorePrimitiveMatch(pType, aType);
            }
            else if (pType.isAssignableFrom(aType)) { // trouble ahead...
                if ( pType == Object.class ){
                    score -= 1; // barely matches, everything is an object, hence...
                } else {
                    score -= 2 ; // solid match
                }
            } else{
                return IMPOSSIBLE; // hard mismatch, return score
            }
        }
        return score;
    }

    /**
     * Creates a priority queue of methods with args passed down by scoring each method with name in the class
     * @param clazz the class
     * @param methodName name of the method
     * @param args arguments which were handed down
     * @return a PriorityQueue of MethodScore
     */
    public static PriorityQueue<MethodScore> scoreMethods(Class<?> clazz, String methodName, Object[] args){
        MethodCache mc = MethodCache.getAllMethods( clazz, true, true);
        final List<Method> namedMethod = mc.overLoads.getOrDefault( methodName, Collections.emptyList());
        if (namedMethod.isEmpty()) { // this is proper method not found
            final String err = String.format("No method with name '%s' in class '%s' found!", methodName, clazz.getName());
            throw new NoSuchMethodError(err);
        }
        ScoreKey scoreKey = null;
        if ( namedMethod.size() > 2 ){
            // else it is not worth th compute ... 33% hit is a bad idea 25% works
           scoreKey = new ScoreKey(clazz,methodName,args);
           if ( CACHE.containsKey(scoreKey) ) return  CACHE.get(scoreKey);
        }
        final PriorityQueue<MethodScore> pq = new PriorityQueue<>();
        for ( Method m : namedMethod ){
            int score = parameterScore(m.getParameterTypes(), m.isVarArgs(), args);
            if ( score < IMPOSSIBLE ) {
                pq.add(new MethodScore(m, score));
            }
        }
        if ( scoreKey != null ){
            CACHE.put(scoreKey, pq);
        }
        return pq;
    }

    static String formatInvalidArgMessage( Class<?> clazz, String methodName, Object[] args ){
        MethodCache mc = MethodCache.getAllMethods( clazz, true, true);
        final List<Method> namedMethod = mc.overLoads.getOrDefault( methodName, Collections.emptyList());
        StringBuilder sb = new StringBuilder("\nArgs were: ");
        Arrays.stream(args).forEach( a -> sb.append( a == null? "null" : a.getClass().getName()).append(", ") );
        sb.deleteCharAt(sb.length()-1);
        sb.deleteCharAt(sb.length()-1);
        sb.append("\nMethods considered:\n");
        // now add
        namedMethod.forEach(m -> sb.append(m.toString()).append("\n") );
        return sb.toString();
    }

    /**
     * Creates a priority queue of constructors with args passed down by scoring each method with name in the class
     * @param clazz the class
     * @param args arguments which were handed down
     * @return a PriorityQueue of MethodScore
     */
    public static PriorityQueue<MethodScore> scoreConstructors(Class<?> clazz, Object[] args){
        final Constructor<?>[] constructors = clazz.getConstructors();
        final PriorityQueue<MethodScore> pq = new PriorityQueue<>();
        for ( Constructor<?> c : constructors ){
            int score = parameterScore(c.getParameterTypes(), c.isVarArgs(), args);
            if ( score < IMPOSSIBLE ) {
                pq.add(new MethodScore(c, score));
            }
        }
        return pq;
    }

    MethodScore( Method m, int s){
        method = m;
        score = s;
        constructor = null;
        isVarArgs = m.isVarArgs();
    }

    MethodScore( Constructor<?> c, int s){
        method = null;
        score = s;
        constructor = c;
        isVarArgs = c.isVarArgs();
    }

    /**
     * When score match - there is this problem:
     * this.isVarArgs | o.isVarArgs | Result
     *      false     |  false      |  0
     *      false     |  true       | -1
     *      true      |  false      |  1
     *      true      |  true       |  0
     *
     * @param o the object to be compared.
     * @return -1 , 0, 1 based on current is smaller, same or larger than other
     */
    @Override
    public int compareTo(MethodScore o) {
        if ( score < o.score ) return -1;
        if ( score > o.score ) return 1;
        /*  scores match, so now... */
        if ( isVarArgs ){
            if ( o.isVarArgs ) return 0;
            return 1;
        }
        if ( o.isVarArgs ) return -1;
        return 0;
    }
    @Override
    public String toString() {
        if ( method != null ) {
            return String.format("%d\t%s", score, method);
        }
        if ( constructor != null ){
            return String.format("%d\t%s", score, constructor);
        }
        return "";
    }
}