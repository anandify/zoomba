/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import org.jscience.mathematics.number.LargeInteger;
import org.jscience.mathematics.number.Real;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import static zoomba.lang.core.operations.Function.NIL;

/**
 * Arbitrary large range in ZoomBA
 * With Large Precision, Arbitrary Precision support
 */
public class ZXRange extends ZRange{


    /**
     * Checks if the range should be extended or not
     * @param args Object array of args
     * @return true if any arg is extended type
     */
    static boolean hasXtype(Object[] args){
        for (Object o : args) {
            if (o instanceof Double || o instanceof Float
                    || o instanceof BigInteger || o instanceof BigDecimal
                    || o instanceof LargeInteger || o instanceof Real
            ) return true;
        }
       return false;
    }

    final ZNumber b;

    final ZNumber e;

    final ZNumber s;

    ZNumber c;

    @Override
    public void reset() {
        c = new ZNumber(b.num);
    }

    /**
     * Creates a ZXRange from ZNumbers
     * @param b begin
     * @param e end
     * @param s step
     */
    public ZXRange(ZNumber b, ZNumber e, ZNumber s) {
        super();
        this.b = b ;
        this.e = e ;
        this.s = s ;
        decreasing = ( b.compareTo( e) > 0 );
        reset();
    }

    /**
     * Creates a ZXRange from ZNumbers
     * @param b begin
     * @param e end
     */
    public ZXRange(ZNumber b, ZNumber e) {
        super();
        this.b = b ;
        this.e = e ;
        decreasing = ( b.compareTo( e) > 0 );
        if ( decreasing ) {
            s = new ZNumber(-1);
        }else{
            s = new ZNumber(1);
        }
        reset();
    }

    @Override
    public ZRange yielding() {
        return new ZXRange( b, e, s );
    }

    @Override
    public Object object(long l) {
        return NIL;
    }

    @Override
    public String toString() {
        return String.format("[%s:%s:%s]" , b,e,s);
    }

    @Override
    public void toEnd() {
        c = new ZNumber(e);
        c.sub_mutable( s );
    }

    @Override
    public boolean hasNext() {
        if ( decreasing ) {
            return c.compareTo( e ) > 0 ;
        }
        return c.compareTo(e) < 0 ;
    }

    @Override
    public Object next() {
        Number next = c.actual() ;
        c.add_mutable(s);
        return next ;
    }

    @Override
    public Object get(Number n) {
        // n * s + b
        ZNumber l =   new ZNumber( n );
        l.mul_mutable(s);
        l.add_mutable( b );

        if ( decreasing ){
            if ( l.compareTo( e ) >= 0 ) return l.actual();
            return NIL;
        }
        if ( l.compareTo( e ) <= 0 ) return l.actual();
        return NIL;
    }

    @Override
    public boolean hasPrevious() {
        if ( decreasing ) {
            return c.compareTo( b ) <= 0 ;
        }
        return c.compareTo(b) >= 0 ;
    }

    @Override
    public Object previous() {
        Number prev = c.actual() ;
        c.sub_mutable(s);
        return prev ;
    }

    @Override
    public Number zSize(){
        Real d = e.num.minus(b.num);
        Real size = d.divide(s.num);
        return size.round();
    }

    @Override
    public int size(){
        return zSize().intValue();
    }

    @Override
    public ZRange reverse() {
        return new ZXRange( new ZNumber( e._sub_(s) ) , new ZNumber( b._sub_(s) ) , new ZNumber( s._not_() ) );
    }

    @Override
    public ZRange inverse() {
        return new ZXRange(e, b, new ZNumber( s._not_() ) );
    }

    /**
     * In case of IN operation how many digits of precision to look for
     */
    public int maxPrecision = 3;

    @Override
    public List asList(){
        final ZXRange r = this;
        return new ZRangeList(this){
            @Override
            public boolean isIn(ZNumber n){
                Real t = n.num;
                int cb = t.compareTo(r.b.num);
                if ( cb == 0 ) return true ; // equal to beginning of range, so yes.
                int ce = t.compareTo(r.e.num);
                if ( decreasing ){
                    if ( ce <= 0 || cb > 0 ) return false;
                } else {
                    if ( cb < 0 || ce >= 0 ) return false;
                }
                Real rs = r.s.num;
                Real rb = r.b.num;
                if ( s.fractional()|| b.fractional() ){
                    // find multiplier in terms of 10^x which would make both integer and then try
                    rb =  b.num.copy() ;
                    rs = s.num.copy() ;
                    int counter = maxPrecision;
                    while ( counter  > 0  && ( !ZNumber.integerValued(rb) || !ZNumber.integerValued(rs) ) ){
                        rb = rb.times(10);
                        rs = rs.times(10);
                        t = t.times(10);
                        counter --;
                    }
                    if ( !ZNumber.integerValued(t) ) return false;
                }
                // deal with it as if large no
                final LargeInteger s = rs.round().abs();
                LargeInteger li = t.minus(rb).round().abs();
                return li.mod(s).equals(0L);
            }
        };
    }
}
