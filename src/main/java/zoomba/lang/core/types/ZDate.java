/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.chrono.ChronoLocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAmount;
import java.util.Date;
import java.util.function.BiFunction;

import org.jscience.mathematics.number.LargeInteger;
import zoomba.lang.core.operations.Arithmetic.* ;

/**
 * A Date wrapper for Date related operations
 * Wraps over Java 8 Instant
 */
public class ZDate implements BasicArithmeticAware, ZTypes.StringX<Date>{

    /**
     * Underlying actual Instant
     */
    protected Instant _instant = Instant.now(); // creation instant

    /**
     * Gets the instant back
     * @return instant when this object was created
     */
    public Instant timeStamp(){ return _instant; }

    @Override
    public Date deepCopy() {
        return date();
    }

    /**
     * Creates a ZDate using arguments
     * @param args can be:
     *      long
     *      Date
     *      String ( default format should be yyyyMMdd )
     *      String, String ( value, formatString )
     * @return a ZDate
     */
    public static ZDate time(Object...args){
        if ( args.length == 0 ) return new ZDate();
        if ( args[0] == null  ) return new ZDate();
        try {
            if (args[0] instanceof LocalDateTime) return new ZDate((LocalDateTime) args[0]);
            if (args[0] instanceof Date) {
                if (args.length > 1) return new ZDate((Date) args[0], args[1].toString());
                return new ZDate((Date) args[0]);
            }
            if (args[0] instanceof ZDate) {
                return new ZDate((ZDate) args[0]);
            }
            if (args[0] instanceof String) {
                if (args.length > 1) {
                    return new ZDate(args[0].toString(), args[1].toString());
                }
                return new ZDate(args[0].toString());
            }
            if (args[0] instanceof Number) {
                return new ZDate((Number) args[0]);
            }
        }catch (Throwable t){
            // log, later... now eat up ?
        }
        return null;
    }

    @Override
    public String string(Object... args) {
        if ( args.length == 0 ) return time.toString();
        if ( args[0] instanceof String ){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern((String)args[0]);
            try {
                return formatter.format(time);
            }catch (Throwable t){
                // perhaps does not have ms and all?
                return formatter.format( time.toLocalDate() );
            }
        }
        return time.toString();
    }

    @Override
    public String toString() {
        return time.toString();
    }

    static final ThreadLocal<SimpleDateFormat> defaultFormatter = new ThreadLocal<>();

    static {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        fmt.setLenient(false);
        defaultFormatter.set( fmt );
    }

    /**
     * Underlying LocalDateTime for the instance
     */
    protected LocalDateTime time;

    /**
     * Tries to return nano sec time
     * @return this time in nano sec epoch
     */
    public LargeInteger toNano(){
        long ms = date().getTime();
        LargeInteger ts = LargeInteger.valueOf(ms);
        ts = ts.times( 1000000L );
        // this will be almost right when both point to same ms,
        // That is, no arge constructor,
        // else it would be random nano sec added
        return ts.plus( _instant.getNano() % 100000L);
    }

    /**
     * Gets the underlying LocalDateTime
     * @return inner LocalDateTime
     */
    public LocalDateTime getTime(){ return time ; }

    /**
     * Gets the Java 7 Date()
     * @return same Date object
     */
    public Date date(){ return  Date.from(time.atZone(ZoneId.systemDefault()).toInstant()); }

    /**
     * Converts this date time to ZoneId date time
     * @param toZoneId the zoneId where we need to shift the time
     * @return time in the toZoneId
     */
    public ZDate at(String toZoneId){
        ZonedDateTime zonedDateTime = time.atZone( ZoneId.systemDefault() );
        ZoneId newZone = ZoneId.of(toZoneId);
        return new ZDate(zonedDateTime.withZoneSameInstant(newZone).toLocalDateTime());
    }

    /**
     * Given a zoneId returns current Date on that zone
     * @param zoneId the zone id
     * @return self in that time zone
     */
    public Date date(String zoneId){
        return at(zoneId).date();
    }

    /**
     * Creates a ZDate
     */
    public ZDate(){
        time = LocalDateTime.ofInstant(_instant, ZoneId.systemDefault());
    }

    /**
     * Creates ZDate from local time
     * @param time local time
     */
    public ZDate(LocalDateTime time){
        this.time = time ;
    }

    /**
     * Creates a ZDate from another
     * Not a clone, shares underlying time
     * @param zDate another
     */
    public ZDate(ZDate zDate){
        this.time = zDate.time  ;
        this._instant = zDate._instant ;
    }

    /**
     * Creates a date from string date time
     * @param dateTime the date time
     */
    public ZDate(String dateTime){
        if ( dateTime.startsWith("Z") || dateTime.startsWith("+") || dateTime.startsWith("-")){
           time = LocalDateTime.now(ZoneOffset.of(dateTime));
        }else {
            try{
                Date date = defaultFormatter.get().parse(dateTime);
                time = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            }catch (Exception e){
                throw new UnsupportedOperationException("Invalid date !");
            }
        }
    }

    /**
     * Creates a ZDate from string date time and format
     * @param dateTime the date time
     * @param format the format string
     */
    public ZDate(String dateTime, String format){
        try {
            SimpleDateFormat fmt = new SimpleDateFormat(format);
            fmt.setLenient(false);
            Date d = fmt.parse(dateTime);
            time = d.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        }catch (Exception e){
            throw new UnsupportedOperationException("Invalid date !");
        }
    }

    /**
     * Create ZDate from java 7 Date
     * @param date the date
     */
    public ZDate(Date date){
        time = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    /**
     * Create a date in the time zone
     * @param date the date
     * @param zoneId the time zone id
     */
    public ZDate(Date date, String  zoneId){
        time = date.toInstant().atZone(ZoneOffset.of(zoneId)).toLocalDateTime();
    }

    /**
     * Create a date from a timestamp
     * @param timeStamp the number timestamp
     */
    public ZDate(Number timeStamp){
        this( new Date(timeStamp.longValue() ));
    }

    private static LocalDateTime timeSpanAdjust(LocalDateTime time, String timeSpanString,
                                               BiFunction<LocalDateTime,TemporalAmount,LocalDateTime> additiveFunction){
        if ( !timeSpanString.startsWith("P") ) return null;
        int inxTimeMarker = timeSpanString.indexOf('T');
        try {
            if ( inxTimeMarker < 0 ){ // this is pure Period 'PXXXX'
                return additiveFunction.apply( time, Period.parse(timeSpanString) );
            }
            if ( inxTimeMarker == 1 ){ // this is pure Duration 'PTYYYY'
                return additiveFunction.apply( time, Duration.parse(timeSpanString) );
            }
            // We have mixed bag here ...
            final String period = timeSpanString.substring(0, inxTimeMarker );
            final String duration = "P" + timeSpanString.substring(inxTimeMarker);
            return additiveFunction.apply( additiveFunction.apply( time, Period.parse(period)),
                    Duration.parse(duration));

        } catch (Throwable ignore){ }
        return null;
    }

    @Override
    public Object _add_(Object o) {
        // if o is Number, add to millis
        if ( o instanceof Number ){
            LocalDateTime aTime = time.plus( Duration.ofMillis ( ( (Number) o).longValue() ) );
            return new ZDate(aTime);
        }
        if ( o instanceof TemporalAmount){
            LocalDateTime aTime = time.plus((TemporalAmount)o);
            return new ZDate(aTime);
        }
        LocalDateTime aTime = timeSpanAdjust( time, String.valueOf(o), LocalDateTime::plus );
        if ( aTime != null ){
            return new ZDate(aTime);
        }
        throw new UnsupportedOperationException("Can not add non duration into Date!");
    }

    @Override
    public Object _sub_(Object o) {
        if ( o instanceof Date ){
            o = new ZDate((Date) o);
        }
        if ( o instanceof ZDate ){
            o = ((ZDate)o).time ;
        }
        if ( o instanceof LocalDateTime ){
            // see that it is start - end, imagines start <= end
            return Duration.between( (LocalDateTime)o, time );
        }
        // if o is Number, add to millis
        if ( o instanceof Number ){
            LocalDateTime aTime = time.minus( Duration.ofMillis ( ( (Number) o).longValue() )  );
            return new ZDate(aTime);
        }
        if ( o instanceof TemporalAmount){
            LocalDateTime aTime = time.minus((TemporalAmount)o);
            return new ZDate(aTime);
        }
        LocalDateTime aTime = timeSpanAdjust( time, String.valueOf(o), LocalDateTime::minus );
        if ( aTime != null ){
            return new ZDate(aTime);
        }
        throw new UnsupportedOperationException("Can not subtract non duration into Date!");
    }

    @Override
    public Object _mul_(Object o) {
        throw new UnsupportedOperationException("Can not multiply date!");
    }

    @Override
    public Object _div_(Object o) {
        throw new UnsupportedOperationException("Can not divide date!");
    }

    @Override
    public Object _pow_(Object o) {
        throw new UnsupportedOperationException("Can not raise power of date!");
    }

    @Override
    public void add_mutable(Object o) {
        // if o is Number, add to millis
        if ( o instanceof Number ){
            time = time.plus( Duration.ofMillis ( ( (Number) o).longValue() )  );
            return ;
        }
        if ( o instanceof TemporalAmount){
            this.time = time.plus((TemporalAmount)o);
            return;
        }
        LocalDateTime aTime = timeSpanAdjust( time, String.valueOf(o), LocalDateTime::plus );
        if ( aTime != null ){
            this.time = aTime;
            return;
        }
        throw new UnsupportedOperationException("Can not add non duration into Date!");
    }

    @Override
    public void sub_mutable(Object o) {
        // if o is Number, add to millis
        if ( o instanceof Number ){
            time = time.minus( Duration.ofMillis ( ( (Number) o).longValue() )  );
            return ;
        }
        if ( o instanceof TemporalAmount){
            this.time = time.minus((TemporalAmount)o);
            return;
        }
        LocalDateTime aTime = timeSpanAdjust( time, String.valueOf(o), LocalDateTime::minus );
        if ( aTime != null ){
            this.time = aTime ;
            return;
        }
        throw new UnsupportedOperationException("Can not subtract non duration into Date!");
    }

    @Override
    public void mul_mutable(Object o) {
        throw new UnsupportedOperationException("Can not multiply date!");
    }

    @Override
    public void div_mutable(Object o) {
        throw new UnsupportedOperationException("Can not divide date!");
    }

    @Override
    public int compareTo(Object o) {
        if ( o == null ) return 1 ;
        if ( o == this ) return  0;
        if ( o instanceof ChronoLocalDateTime) return time.compareTo((ChronoLocalDateTime<?>)o);
        if ( o instanceof ZDate ){
            ZDate other = (ZDate)o;
            return time.compareTo(other.time);
        }
        if ( o instanceof Date ){
            ZDate other = new ZDate((Date) o);
            return time.compareTo(other.time);
        }
        if ( o instanceof Number ){
            ZDate other = new ZDate((Number) o);
            return time.compareTo(other.time);
        }
        throw new UnsupportedOperationException("Can not compare against something which is not date!");
    }

    @Override
    public int hashCode() {
        return time.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return ( obj == this ) || ( 0 == compareTo(obj) );
    }
}
