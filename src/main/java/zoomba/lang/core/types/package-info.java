/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Contains all the non container Types ZoomBA uses
 * <pre>
 *     1. Wrapper over Date Types
 *     2. Exceptions and Assertions
 *     3. Wrapper over Numbers for unlimited Arithmetic
 *     4. Ranges and Extended range ( unlimited length range )
 *     5. Sequences ( Abstract Sequences, Permutations, Combinations )
 *     6. String wrapper for += operation
 *     7. Type checking utilities
 *     8. Xml Handling
 * </pre>
 */
package zoomba.lang.core.types;