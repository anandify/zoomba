/*
 * Copyright 2024 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import zoomba.lang.core.collections.ZList;

import java.time.Duration;
import java.util.*;
import static zoomba.lang.core.operations.Function.NIL;

/**
 * A Basic Range functionality
 * [x:y] is the syntax
 */
public abstract class ZRange implements ListIterator {

    /**
     * A List out of the Iterator
     */
    public class ZRangeList implements List{

        /**
         * Underlying ZRange
         */
        public final ZRange zRange ;

        /**
         * Constructs a ZRangeList
         * @param range with this range
         */
        public ZRangeList(ZRange range) {
            zRange = range.yielding();
            zRange.reset();
        }

        @Override
        public boolean add(Object o) {
            return false;
        }

        @Override
        public int size() {
            return zRange.size();
        }

        @Override
        public boolean isEmpty() {
            return (size() == 0 );
        }

        /**
         * Is the number exist as a member value in this range
         * @param n a number
         * @return true if it exists in this range, false otherwise
         */
        protected boolean isIn(ZNumber n){
            long t = n.longValue();
            if (semiClosed && t == end) return false;
            if (t == begin) return true;
            final long s ;
            if ( decreasing ){
                if ( t < end || t > begin ) return false;
                s = -step;
            } else {
                if (t < begin || t > end ) return false;
                s = step;
            }
            return (t - begin) % s == 0;
        }

        @Override
        public boolean contains(Object o) {
            try {
                return isIn( new ZNumber(o));
            }catch (Exception e) {
                return false;
            }
        }

        @Override
        public Iterator iterator() {
            ZRange r = zRange.yielding();
            r.reset();
            return r;
        }

        @Override
        public Object[] toArray() {
            return ((ZRange)iterator()).list().toArray();
        }

        @Override
        public Object[] toArray(Object[] a) {
            return toArray();
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection c) {
            for ( Object o : c ){
                if ( !contains(o) ) return false ;
            }
            return true ;
        }

        @Override
        public boolean addAll(Collection c) {
            return false;
        }

        @Override
        public boolean addAll(int index, Collection c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public Object get(int index) {
            return  zRange.get( index );
        }

        @Override
        public Object set(int index, Object element) {
            throw new UnsupportedOperationException("This List can not be Modified!");
        }

        @Override
        public void add(int index, Object element) {
            throw new UnsupportedOperationException("This List can not be Modified!");
        }

        @Override
        public Object remove(int index) {
            throw new UnsupportedOperationException("This List can not be Modified!");
        }

        @Override
        public int indexOf(Object o) {
           Iterator li  = iterator();
           for ( int i=0; li.hasNext(); i++){
               Object next = li.next();
               if ( Objects.equals(next,o)) return i;
           }
           return -1;
        }

        @Override
        public int lastIndexOf(Object o) {
            List l = zRange.reverse().list();
            int size = l.size();
            Iterator li  = l.iterator();
            for ( int i=0; li.hasNext(); i++){
                Object next = li.next();
                if ( Objects.equals(next,o)) return (size-i-1);
            }
            return -1;
        }

        @Override
        public ListIterator listIterator() {
            return zRange.yielding();
        }

        @Override
        public ListIterator listIterator(int index) {
            ZRange r = zRange.yielding();
            r.toIndex(index);
            return r;
        }

        @Override
        public List subList(int fromIndex, int toIndex) {
            return Collections.EMPTY_LIST ;
        }

    }

    /**
     * An empty range
     */
    public static final ZRange EMPTY_RANGE = new NumRange(0,0);

    /**
     * Beginning of the the range
     */
    protected final long begin;

    /**
     * End of the range
     */
    protected final long end;

    /**
     * Step of the range
     */
    protected final long step;

    /**
     * Is this range a decreasing range
     */
    protected boolean decreasing;

    /**
     * All ranges are generally [a,b) this is semiClosed : x &gt;= a and x &lt; b
     * For a range that will be full closed will be [a,b] x &gt;= a and x &lt;= b
     * Right now, off the shelf only implementation of a closed range is CharRange
     */
    protected boolean semiClosed = true ;

    /**
     * All ranges are generally [a,b) this is semiClosed : x &gt;= a and x &lt; b
     * For a range that will be full closed will be [a,b] x &gt;= a and x &lt;= b
     * Right now, off the shelf only implementation of a closed range is CharRange
     * @return true if semi closed, false if not
     */
    public boolean isSemiClosed(){ return semiClosed; }

    /**
     * Current index of the range
     */
    protected long current;

    /**
     * Gets the reverse range of the range
     * [x:y] with x less than y reversed into [y-1:x-1]
     * @return the reverse range
     */
    public abstract ZRange reverse();

    /**
     * Gets the Inverse range of the range
     * [x:y] with x less than y inverses into [y:x]
     * @return the reverse range
     */
    public abstract ZRange inverse();

    /**
     * Size of the range ( approx items in the range )
     * @return size of the range
     */
    public int size(){
        return (int) ( zSize().intValue() ); // TODO base bug
    }

    /**
     * Actual Size of the range because they can be arbitrarily large
     * @return as a Number
     */
    public Number zSize(){
        return ( (end - begin )/step  );
    }

    @Override
    public String toString() {
        return String.format("[%s:%s:%s]", object(begin),object(end), step );
    }

    /**
     * Returns a list representation from the range
     * @return a list
     */
    public List asList(){
        return new ZRangeList(this);
    }

    /**
     * Generates a
     * @return a list containing all item in the range
     */
    public List list(){
        ZRange r = yielding();
        ZList l = new ZList();
        while ( r.hasNext() ){
            l.add( r.next() );
        }
        return l;
    }

    /**
     * Resets the internal index so that range can be reused
     */
    public void reset(){
        current = begin - step ; // bug here, there is a  possibility of underflow , but, does not matter
    }

    /**
     * Jumps to the end of the range - halts the iteration
     */
    public void toEnd(){
        current = end ;
    }

    /**
     * Jumps to an internal index
     * @param index the index to jump to
     */
    public void toIndex(int index){
        long l = begin + index * step ;
        current = l ;
    }

    /**
     * Constructs a ZRange
     */
    protected ZRange(){
        begin = end = step = 0 ;
        decreasing = false;
    }

    /**
     * Crates a range
     * @param b begin ( start )
     * @param e end
     * @param s step
     */
    public ZRange(long b, long e, long s){
        begin = b ;
        end = e ;
        step = s ;
        decreasing = begin > end ;
        reset();
    }

    /**
     * Creates a range
     * @param b begin ( start )
     * @param e end ( step is implicitly either +1, or -1)
     */
    public ZRange(long b, long e){
        begin = b ;
        end = e ;
        decreasing = begin > end ;
        if ( decreasing ){
            step = -1 ;
        } else {
            step = 1;
        }
        reset();
    }

    /**
     * Should return a cloned copy of the self, with current state
     * @return a cloned copy of self
     */
    public abstract ZRange yielding();

    /**
     * Given a long return appropriate object
     * @param l the long
     * @return the appropriate object
     */
    public abstract Object object(long l);

    /**
     * Given an index return the object in range
     * @param n the index
     * @return object at n th index
     */
    public Object get(Number n){
        long l = begin + step * n.longValue() ;
        if ( begin <= end ) {
            if (l <= end) return l;
            return NIL;
        }
        if (l >= end) return l;
        return NIL;
    }

    @Override
    public boolean hasNext() {
        if ( step >= 0 ) return ( current + step < end );
        return current + step > end ;
    }

    @Override
    public Object next() {
        int i = (int)(current += step);
        if ((long)i == current  ){
            return i;
        }
        return current ;
    }

    @Override
    public boolean hasPrevious() {
        if ( step >= 0 ) {
            return (current - step >= begin);
        }
        return (current - step <= begin) ;
    }

    @Override
    public Object previous() {
        int i = (int)(current -= step);
        if ((long)i == current  ){
            return i;
        }
        return current ;
    }

    @Override
    public int nextIndex() {
        return (int)(current + step) ;
    }

    @Override
    public int previousIndex() {
        return (int)(current - step) ;
    }

    @Override
    public void remove() { }

    @Override
    public void set(Object o) { }

    @Override
    public void add(Object o) { }

    /**
     * A number range [0:10:3]
     */
    public static class NumRange extends ZRange{

        @Override
        public Object object(long l) {
            int i = (int)(l);
            if ((long)i == l  ){
                return i;
            }
            return l;
        }

        /**
         * Constructs a Numeric Range
         * @param b begin ( inclusive )
         * @param e end ( exclusive )
         * @param s step ( incremental )
         */
        public NumRange(long b, long e, long s){
            super(b,e,s);
        }

        /**
         * Constructs a Numeric Range
         * @param b begin ( inclusive )
         * @param e end ( exclusive )
         */
        public NumRange(long b, long e){
            super(b,e);
        }

        @Override
        public ZRange yielding() {
            NumRange nr = new NumRange(begin,end,step);
            nr.current = current ;
            return nr;
        }

        @Override
        public ZRange reverse() {
            return new NumRange( end - step,  begin - step, -step );
        }

        @Override
        public ZRange inverse() {
            return new NumRange( end ,  begin , -step );
        }
    }

    /**
     * A Character Range
     */
    public static class CharRange extends ZRange {

        String string;

        @Override
        public Object object(long l) {
            return Character.valueOf((char) l);
        }

        @Override
        public ZRange yielding() {
            CharRange nr = new CharRange((char)begin,(char)end,(int)step);
            nr.current = current ;
            return nr;
        }

        /**
         * Entire range returned as a string
         * @return a string representing the entire data of the range
         */
        public String string(){
            if ( string == null ){
                CharRange cr = (CharRange) yielding();
                StringBuilder buf = new StringBuilder();
                while ( cr.hasNext() ){
                    buf.append(cr.next());
                }
                string = buf.toString();
            }
            return string ;
        }

        /**
         * Constructs a CharRange
         * @param b begin ( inclusive )
         * @param e end ( exclusive )
         * @param step step size ( incremental )
         */
        public CharRange(char b, char e, int step){
            super(b,e,step);
            semiClosed = false;
        }

        /**
         * Constructs a CharRange
         * @param b begin ( inclusive )
         * @param e end ( exclusive )
         */
        public CharRange(char b, char e){
            super(b,e);
            semiClosed = false;
        }

        @Override
        public int size() {
            // because the end is inclusive here...
            return super.size() + 1 ;
        }

        @Override
        public Number zSize(){
            return super.zSize().longValue() + 1 ;
        }

        @Override
        public boolean hasNext() {
            return step > 0 ? (current + step <= end) : ( current + step >= end );
        }

        @Override
        public Object next() {
            int i = (int)super.next();
            return (char)i;
        }

        @Override
        public Object previous() {
            long l = (long)super.previous();
            return (char)l;
        }

        @Override
        public ZRange reverse() {
            return inverse();
        }

        @Override
        public ZRange inverse() {
            int s = (int)step;
            return new CharRange( (char)end, (char)begin, -s );
        }
    }

    /**
     * A Date Range Implementation
     */
    public static class DateRange extends ZRange{

        /**
         * No of MS in an entire day
         */
        public static final long MS_IN_A_DAY = 24 * 60 * 60 * 1000 ;

        @Override
        public Object object(long l) {
            return new Date(l);
        }

        @Override
        public ZRange yielding() {
            DateRange dr = new DateRange(begin,end,step);
            dr.current = current ;
            return dr;

        }

        /**
         * Constructs a DateRange
         * @param b begin ( inclusive )
         * @param e end ( exclusive )
         * @param s step ( increment )
         */
        protected DateRange(long b, long e, long s ){
            super(b,e,s);
        }

        /**
         * Constructs a DateRange
         * @param b begin ( inclusive )
         * @param e end ( exclusive )
         * @param s step ( increment , ms )
         */
        public DateRange(Date b, Date e, long s) {
            super(b.getTime(), e.getTime(), s);
        }

        /**
         * Constructs a DateRange, step is one day
         * @param b begin ( inclusive )
         * @param e end ( exclusive )
         */
        public DateRange(Date b, Date e) {
            this(b, e, MS_IN_A_DAY );
        }

        /**
         * Constructs a DateRange
         * @param b begin ( inclusive )
         * @param e end ( exclusive )
         * @param s step ( increment , ms )
         */
        public DateRange(ZDate b, ZDate e, long s) {
            this(b.date(), e.date(), s);
        }

        /**
         * Constructs a DateRange
         * @param b begin ( inclusive )
         * @param e end ( exclusive )
         */
        public DateRange(ZDate b, ZDate e) {
            this(b, e, MS_IN_A_DAY );
        }

        /**
         * Constructs a DateRange
         * @param b begin ( inclusive )
         * @param e end ( exclusive )
         * @param s step ( increment )
         */
        public DateRange(ZDate b, ZDate e, Duration s) {
            this(b.date(), e.date(), s.toMillis() );
        }

        /**
         * TODO this will break in case of Period
         * Constructs a DateRange
         * @param b begin ( inclusive )
         * @param e end ( exclusive )
         * @param s step ( increment , duration formatted as string )
         */
        public DateRange(ZDate b, ZDate e, String s) {
            this(b, e, Duration.parse(s) );
        }

        @Override
        public Object next() {
            Number n = (Number) super.next();
            return new Date(n.longValue());
        }

        @Override
        public Object previous() {
            Number n = (Number) super.previous();
            return new Date(n.longValue());
        }

        @Override
        public ZRange reverse() {
            return new DateRange( end - step,  begin - step, -step );
        }

        @Override
        public ZRange inverse() {
            return new DateRange( end ,  begin , -step );
        }
    }
}
