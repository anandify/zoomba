# The ZoomBA Programming Language 
Welcome! If you were searching for how to reduce code - for business, or for QA activities, 
you have come to the right place. 
In ZoomBA land, we are not only bothered about the lines of code,
we are even bothered about the number of characters! We promote provable,
 maintainable, minimal coding. That does not mean things are easy and it is 
 *codeless* as some marketing gimmick guys suggest. They certainly are not. 
But just like cycling, once you get hang of it, it becomes a habit.
ZoomBA promotes declarative thinking over imperative mindset.

## Reference Manual & Book

A Reference Book is created ( source is in [book](book/latex/) ) while the final PDF is [here](https://nmondal.github.io/assets/pdfs/zoomba.pdf).

## About Stability

As of now, ZoomBA is in 0.2 release.
Release means production quality for us.
We are 68% branch covered, and 80% instruction covered.
And for the jargon lovers, we do follow test driven development.

![Code Coverage](cc.png)

### Start Using ZoomBA

#### Quick Install 

Just execute the following script:

```sh
bash -c "$(curl -fsSL https://gitlab.com/non.est.sacra/zoomba/-/raw/master/install.sh)"
```

Once done, add the resulting `alias` commands into your `.zshrc` or `.profile` or `.bashrc`
or whatever is your startup file is.
Simply restart terminal and and type in `zmb` or `zmc` and you should be able to get into zoomba prompt!


#### Detailed Install  
* You need to have at least Java 9 runtime installed.
* Download the binary distribution (one-jar) from here:
  * [released](https://repo1.maven.org/maven2/org/zoomba-lang/zoomba.lang.core/0.2/)
  * [prod snapshot ](https://oss.sonatype.org/content/repositories/snapshots/org/zoomba-lang/zoomba.lang.core/0.3-SNAPSHOT/) 
  * [instrumented,debuggable snapshot](https://oss.sonatype.org/content/repositories/snapshots/org/zoomba-lang/zoomba.lang.core/INSTRUMENTED-0.3-SNAPSHOT/) 
* Simply run now :

```
java -jar zoomba.lang.core-0.2.onejar.jar 
```
or

```
java -jar zoomba.lang.core-0.3-<some-date>.onejar.jar 
java -jar zoomba.lang.core-INSTRUMENTED-0.3-SNAPSHOT-onejar.jar 
```

You should see the *(zoomba)* prompt.
Suggested is to use the *snapshot* version, because for a long time,
no release has been gone through, while many crucial changes happened over 
the last years.

### From Maven Repo 
If you want to programmatically call ZoomBA from JVM - here is the dependency:

```xml
<dependency>
    <groupId>org.zoomba-lang</groupId>
    <artifactId>zoomba.lang.core</artifactId>
    <version>${zoomba.version}</version>
</dependency>
```

Current stable ```zoomba.version``` is 0.2.

### If you would like to Build

The project is built using standard maven technique.
Thus, 

     mvn clean install 

should do the trick of building and installing. 
There is no need to skip tests.

### How to Use Stand Alone

Once the building is done, go to the target directory, and you can run 

```shell
java -jar zoomba.lang.core-0.2.one-jar.jar 
# NOTE: maven repo has onejar, this has one-jar in the file name
# This may cause a bit of confusion
```

#### Java 9+

In case of Java 9+ you should :

```
java --add-opens java.base/jdk.internal.loader=ALL-UNNAMED\
   -jar zoomba.lang.core-0.2.one-jar.jar  
```
Otherwise the "programmable classloader" won't work. `load()` function will fail.


This should fire up the minimalistic REPL we have created.
Alternatively, you can alias the same command :

```
alias zmb="java -jar /path/to/zoomba-jar/zoomba.lang.core-0.2.one-jar.jar"     
```

Create a file hello.zm :

     println("Hello, ZoomBA!")

And now, you can use ZoomBA :

     $zmb hello.zm 
     "Hello, ZoomBA!"
     $

#### Using in Android

Because the whole scripting ecosystem is android is in sad state - you may want to use ZoomBA in Android. It can easily be done. Given `javax` does not exist in Android, all you really need to do is to add the `jsr223.jar` from [here](http://www.java2s.com/Code/Jar/j/Downloadjsr223jar.htm), into the projects lib folder and then adding it to the `build.gradle` as follows from this StackOverFlow [link](https://stackoverflow.com/questions/25660166/how-to-add-a-jar-in-external-libraries-in-android-studio). 

The minimal code, should be, as always:

```kotlin
val zs = ZScript("42")
val mc = zs.execute()
print(mc.value()) // should print 42 
```



### Command Line Help

ZoomBA comes with a command line help, so 

```
(zoomba)//h topic_name
```
prints whatever is there on the topic. 



### Using Javax Scripting

ZoomBA can be used using `javax.scripting` in the following way (after including as dependency) :

```java
@Test
public void javaxScripting() throws Exception {
    ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
    ScriptEngine engine = scriptEngineManager.getEngineByName("zoomba");
    assertNotNull(engine);    
    Object result = engine.eval("2+2");
    assertEquals(4,result);
    Bindings bindings = engine.createBindings();
    bindings.put("x", 2 );
    bindings.put("y", 2 );
    result = engine.eval("x+y", bindings);
    assertEquals(4,result);
    ScriptContext scriptContext = engine.getContext();
    scriptContext.setAttribute("x", 2, ScriptContext.ENGINE_SCOPE );
    scriptContext.setAttribute("y", 2, ScriptContext.GLOBAL_SCOPE );
    result = engine.eval("x+y");
    assertEquals(4,result);
    CompiledScript compiledScript = ((Compilable)engine).compile("x+y");
    assertEquals( engine, compiledScript.getEngine());
    result = compiledScript.eval(bindings);
    assertEquals(4,result);
}
```





### Examples

The repository is rich in samples. See the samples folder in the source repo. Specifically see the test directory inside the samples folder.
They have everything - every feature ZoomBA is capable of. ZoomBA team maintains a [CareerCup profile](https://www.careercup.com/user?id=5687896666275840) where we answer standard trick algorithms in ZoomBA.
That is another starting point. Albeit only last 50 gets shown there. 

### Embedding in Other Languages

Again, see the test source code, or the `Main.java` file. They are descriptive enough, how to use ZoomBA from Java. As of now, JVM is the only runtime we do support. In the future, we would port the same to .NET/Mono.

### IDE Settings

We support vim, Sublime Text and Intellij Idea.
The corresponding files can be found in the source repo under 
[ide\_settings\_files](https://gitlab.com/non.est.sacra/zoomba/tree/master/ide_settings_files). That would start syntax highlighting.

For usage in GitHub and GitLab (syntax highlight in the web) 

this extension is very handy : https://github.com/ajafri2001/zoomba-highlighter .

### More Details and Documentation

ZoomBA is not our full time Job. This is a thankless job we are doing, 
and it would take time. A wiki is being constructed as of now, 
but we are very busy being ourselves.
But, we put up [ZoomBA99](https://gitlab.com/non.est.sacra/zoomba99)
which has samples on how to use ZoomBA for many examples.

## Licensing

ZoomBA is shipped with Apache 2. 
