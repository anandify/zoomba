FROM eclipse-temurin:17-jre-alpine
LABEL maintainer="non.est.sacra@gmail.com"
COPY target/*jar  /home/zmb/
ENTRYPOINT ["java", "--add-opens", "java.base/jdk.internal.loader=ALL-UNNAMED" , "-jar" ,"/home/zmb/zoomba.lang.core-0.3-SNAPSHOT.one-jar.jar" ]
