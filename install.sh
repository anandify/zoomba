##############################################
# ZoomBA install script which installs ZMB to
# Any *nix machine
# curl -s -L URL_TO_SCRIPT_HERE | bash
###############################################
if ! java --version; then
    echo "Java Runtime is not found in your system"
    echo "Please ensure 'java' in your path!"
    echo "Then, try again!"
    exit 42
fi
echo "Java RunTime exists in your system, will be downloading ZoomBA binaries!"
# now we can continue 
cwd=$(pwd)
ZMB_DIR="$HOME/.local/bin/zmb"
mkdir -p $ZMB_DIR
cd $ZMB_DIR
rm *.jar 2> /dev/null
echo "Finding latest prod snapshot jar..."
url=$(curl https://oss.sonatype.org/content/repositories/snapshots/org/zoomba-lang/zoomba.lang.core/0.3-SNAPSHOT/ | grep 'onejar.jar"' | tail -1 | cut -f2 -d'"')
snp_jar_name=$(echo $url | cut -f11 -d'/') 
echo "Found latest prod snapshot jar : $snp_jar_name ; Downloading..."
curl $url -o $snp_jar_name   
echo "Download Complete!"

echo "Finding latest instrumented snapshot jar..."
url=$(curl https://oss.sonatype.org/content/repositories/snapshots/org/zoomba-lang/zoomba.lang.core/INSTRUMENTED-0.3-SNAPSHOT/ | grep 'onejar.jar"' | tail -1 | cut -f2 -d'"')
snp_ins_jar_name=$(echo $url | cut -f11 -d'/') 
echo "Found latest instrumented snapshot jar : $snp_ins_jar_name ; Downloading..."
curl $url -o $snp_ins_jar_name   
echo "Download Complete!"
cd $cwd 
echo "done!"
echo "_______________________________________________________________________"
echo "                            IMPORTANT                                  "
echo "_______________________________________________________________________"
echo "You should already be having JRE 9+ in your system"
echo "To use ZoomBA please add the following aliases into your profile file:"
echo "alias zmb='java --add-opens java.base/jdk.internal.loader=ALL-UNNAMED -jar $HOME/.local/bin/zmb/$snp_jar_name'"
echo "alias zmc='java --add-opens java.base/jdk.internal.loader=ALL-UNNAMED -Dzoomba.intercept=true -jar $HOME/.local/bin/zmb/$snp_ins_jar_name'"
echo "alias zdb='java --add-opens java.base/jdk.internal.loader=ALL-UNNAMED -Dzoomba.debug=4242 -jar $HOME/.local/bin/zmb/$snp_ins_jar_name'"
echo "_______________________________________________________________________"

exit 0
