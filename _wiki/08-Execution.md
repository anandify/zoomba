# Processes And Threads

## Clocking Execution Time  

It is of importance to figure out how much time it took to execute 
a block of code. This is inherent in ZoomBA. The block *#clock* is responsible for the house keeping.

```scala
 #(t,o) = #clock{ code-body }
```

As we can see, the block returns a tuple, the timing in seconds gets into the variable t, while the output of the block gets into the variable o.
Clearly, the timing t is given in double for precision. We thought about it, 
going back and forth about how to represent time delta, from nanoseconds to millis to seconds and found that humans like to be notified in seconds.
ZoomBA is for the human audience.

```scala
 #(t,o) = #clock{  for([0:1000000]){ x = $  } }
 println(t) // base lining : 0.17823307500000002 // Double
 println(o) // 999999 // Integer 
```

## Processes

### system 

To create an external process, simply use *system()* function:

```js
// simple, call system function to do something
es = system("ls -al")
assert ( es == 0 , 'yea, the status should be zero' )
```

Obviously, this is checking for the exit status. That is why the name aptly is *es*. 

#### Multiple Words 

The issue with a single string is, sometimes, the shell needs to be invoked and you probably want multiple words
to be properly passed as args :

```js
es = system("ls" , "-al")
assert ( es == 0 , 'yea, the status should be zero' )
```

#### Wrong Command 

In case, we give wrong command all together:

```js
es = system("xaasaa" , "boo hahahaa")
assert ( es < 0 , 'yea, the status should be less than zero' )
```

### popen 
The Unix style way to treat everything as resource is :

```js
// now create useful stuff with popen
p = popen( "ls" ,"-al")
p.waitOn(4000) // wait till the process is complete - for 4000 ms
status_code = p.status().code // the exit code for the process 
assert( status_code == 0 , "Process did not complete successfully!" )
out = p.status().out // get the output string for the process 
panic ( empty(out) , 'The output should not be empty!' )
err = p.status().err // get the error string 
assert ( empty(err) , 'The error should be empty!' )
```

As you can see, *popen* opens a process, and then you 
can actually wait on it, then you can actually read what output or error the process produced. In the same way :


```js
p = popen("ls -al") // is also legal 
// automatic wait for the process to exit and get the code 
p.status().code 
```

#### Errors in Process

There are two types of errors possible in `popen()` :

* Executable is wrong - or does not exists 
* Executable is correct, but argument wrong

The first scenario throws an exception, which can be handled by :

```scala
#( p ? e ) = poen('xyz')
if ( empty(p) ){ /* do error check */ 
   bye("guys, sorry - popen failed!")
}
```

What to do when generally there is a fatal error? That is why there is a `bye()` function. The execution halts when `bye` gets invoked.

#### Handling Standard Output and Error 

What if we want to redirect standard output and error into files? How to specify those? Those can be done by passing a `Map` to the `popen()` function :

```json
map = { "args" : [ "ls" , "-al" ] ,  
         "redirect" : { "stdout" : "out.txt" , "stderr" : "err.txt"  } }
p = popen(map) // try doing all of these...
```

Thus, after the execution is over, the redirected files will have the output text and the error text.

## Threads 

Occasional creation of process makes sense, but we, the designers of ZoomBA, could not understand why threads will be needed for business programming.  It will almost never be, because ZoomBA would be invoked
from another JVM, if one wants to create a thread, we should create them there. BUT, of course, *coolness* comes to foray, and thus, 
we have the *thread()* function. Like rest of ZoomBA, it is minimal, 
and complete. 

### thread 


```js
 current_thread = thread() // returns current thread 
 t = thread( ) as {  call_some_function( ) } //  
 t = thread( arg1, arg2, ... ) as { func_with_args( $.c )  }
```

Inside that anonymous function body, the following are the definition :

```js
 $.o : Thread itself 
 $.i : Id of the thread 
 $.c : Arguments passed to the thread() function 
 $.p : Not defined.  
```

Given a thread with a body block, as usual, the last executed line
of the thread block is the return value of the thread block. But, 
as the thread function returns a thread, this return value has to be stored somewhere. Unlike other languages, thus, ZoomBA has a return value for threads.

```js
 t = thread() -> {  x = 42 }
 t.join() // wait for t to be over 
 println( t.value ) // 42, that is stored in value field 
```

#### Understanding Closure of Variables

ZoomBA makes it very easy to create threads :

```scala
t  = thread() as { /* body of the thread */ }
```

Which brings about issues inherent in using variables which are in the main scope. Subtle issues are demonstrated :

```scala
def f( x ){
  printf("id %d --> %d %n", thread().id , x)
}
l = list([0:10]) as {
  p = $.o 
  thread() as { f(p) }
}
while ( true ) ;
```

While the expectation should be a simple `0,1,2,...9` one of the most probably outcomes are :

```shell
id 13 --> 3
id 11 --> 1
id 14 --> 4
id 15 --> 5
id 12 --> 2
id 16 --> 5
id 17 --> 6
id 18 --> 8
id 19 --> 8
id 20 --> 9
```

Why this is happening? Simply because, the `as {}` of the thread function is of a new thread! While the variable it is trying to use is a shared one, one coming from the main thread, so to speak! 

To avoid this sharing of "closed" variable ( global scope in some sense ) we should actually push variables in the thread as shown :

```scala
def f( x ){
  printf("id %d --> %d %n", thread().id , x)
}
l = list([0:10]) as { 
  thread($.o) as { f($.c.0) /* first argument passed for the thread */ }
}
while ( true ) ;
```

Suddenly, we see that all items in `0,1,...8,9` appeared in the output, because variable sharing is not happening at all. Thus to summarise, threads can be given variables. And we should be very careful with what is in the closure or not.

#### Coolness Overload : Performance Testing Websites

We are now well equipped to showcase, in a very minor way, what ZoomBA
can accomplish. We are going to performance test http://www.bing.com.
And here is the code :

```scala
    def benchmark_bing(times=10){
       url = "http://www.bing.com"
       l = list([0:times] ) as {
           thread() as {
               #(t,o) = #clock{  read(url) }
               t // thread.value becomes the time in sec ( double )
           }
       }
       while ( exists( l ) where { $.o.alive } ); // beat that? 
       // sort threads based on their value attr
       sortd(l) where { $.o.0.value <  $.o.1.value }
       // print them all
       lfold(l) -> { println( $.o.value ) }
       inx = ceil( 0.1 * times) // ceil :: ceiling function 
       println("====== 90% ======")
       println( l[ inx ].value ) // this is 90 %ile.
    }  
```

### Fibers - Virtual Threads 

If the underlying JRE is `>= 21` we can invoke virtual threads.
The function to create virtual thread is called `fiber()`. Syntax is exactly like `thread()` :

```scala
// now virtual threads
vts =  list([0:10]) as {
  val = $.o
  fiber(x=val ) as {
     for ( [0:10000]) { }
     x // return x
  }
}
while ( exists( vts ) where { $.o.running } );
for ( [0:10] ){ assert(  vts[$].value == $ , "mismatch in v thread " + $  )  }

```
We can create infinite fibers, to be honest. The system automatically switches to the actual threads
if the system does not v-threads.  


### Poll

Just in the previous example, we had to actually wait for all 
the threads to complete. That was done using the line :

```js
while ( exists( l ) where { $.o.alive } ); // beat that? 
```

But that is not really cool. We are spinning continually, and polling to check if all the threads exited or not. Thus the idea of *polling* comes into foray. Hence, ZoomBA has a function *poll()* :

```js
poll ([  num_tries [ , poll_interval] ] ) where { condition }
```

The previous example could have been a hang, because one of the threads could have decided to hang! But observe the cooler ```poll()``` now :

```js
success = poll( 42, 300 ) :: { !exists( l ) where { $.o.alive }  }
```

This is nice! We have a poll interval: 300 ms, number of tries: 42, and then we have the condition as specified. Till the condition is true, the poll will loop over, till the it reaches the stipulated no of tries. It would return false then. If the condition becomes true, it would return true. 
*poll()* does not throw any exception. A dogmatic use of poll is to make it work like a *sleep()* : 

```js
poll( 10, 42 ) :: { false } 
```

### Atomic 

To ensure that a block of execution is locked, or single entry for multiple threads, ZoomBA uses *#atomic{ }* blocks. It has two properties:

* No other thread can enter the block while it is being hold by another.
* Variables changed here, gets reset, if the block errors out. 

Why these properties? Because, as the design goal goes, these are the basic 
transactional properties. 90% of the cases, you won't need anything beyond. And 80% of the cases, you would need them togather.

#### Thread Synchronizer 
This is how the synchronization works out. Obvious, one must use 
a global variable to do so, or a reference.


```js
$global_i = 0 
def my_sync(){
 #atomic{
    $global_i += 1 
 }
}
l = list([0:10] ) as {
   thread() as {
       my_sync() // yes, how global changes?
   }
}
// wait ...
while ( exists(l) where { $.o.alive } ); // do nothing  
println( $global_i ) //  should be 10, always
```

#### Transaction Block 
To check the transactional property, observe this :
​    
```js    
some_var = 10 
#atomic{
   some_var = 11 
   println( foo_bar ) // error 
}
println(some_var) // should be 10 
```

But, this :

```scala
some_var = 10 
#atomic{
   some_var = 11 
   #(o ? e ) =  println( foo_bar ) // error caught
}
println(some_var) // should be 11  
```

### Mutability 

To pass information across threads of execution, one must use either global variables, or references. A variable reference can not be modified in a function scope, but the reference it points to can be.

```js
 im = 10 // is a variable.
 mu = [10] // now, mu is reference type, and mutable  
 mu.0 = 11 // yes, I can modify .
```

To demonstrate :

```scala
im = 10 // immutable, because primitive 
mu = [10] // mutable, because reference ( container )
def show_problem(){
   im = 11 // no, it creates a new im
   mu.0 = 42 // oh, boy.
}
println( im ) // prints 10 
println( mu ) // prints [ 42 ] 
```

### Being Asynchronous 

If ever need be, and we , the designers really could not fathom why, 
apart from existing fad, people need asynchronous computation, it is there 
in ZoomBA. You can be asynchronous if you want. That can be showcased 
using the function *async()* .

```js
thread = async( post_handler_method ) as { /*  async request body */ }
thread = async( method , post_handler_method ) 
```
The sample is, of course:

```js
$value = 0
// now do async calls
def handler(){
  printf( "I am done [ %s ] \n" , @ARGS.0 )
  $value = @ARGS.0
}
ta = async( handler ) as  {
    x = 0
    for ( i : [0:1000] ){
      x = i
    }
}
ta.join()
assert ( $value == 999 , "Handler worked well!" )
```

And thus, we see, how async works out.     

## Scheduling 
One interesting piece is running async `batch` like jobs, via a schedular. This gets solved via the following:

### Executor Service

One can create an executor service by simply :

```scala
es = batch()
```
Now then, you can simply allocate threads to do your bidding to that schedular as follows:

```scala
for (  [0:10 ] ){
  batch(es,  x=$ ) as {
    ct = thread()
    ct.sleep( 100 )
    printf( "%s -> %s %n", x , ct )
  }
}
batch(es, shutdown = 5000)
assert( es.isShutdown() , "Executor Service was not shutdown after 5000 ms !!!")

```

### Tasks 
Another way to attain the same is by using `task()` function to create `callable()` :

```scala
l = list( [0:10] ) as {  task( $.o ) as {  printf( "%d " , $.c.0 ) ; $.c.0 + 1 } }
println()
// and this is how to use batch
lr = batch( l )
s = sum( lr ) as { $.o.value }
```

And that is another use case of `batch()` function.
