# Integrating Inside JVM 

## ZoomBA Programming Model 

ZoomBA is interpretive mode. The ZoomBA interpreter, iterprets human needs.
One important thing about ZoomBA is that of heavy usage of Monadic Containers. That is very specific jargon - the maybe monad.

### Monadic Containers 
So, what precisely is the MonadicContainer? 
Here is what it is :

```java
interface MonadicContainer{

   boolean isNil(); // is there anything there?

   Object value(); // given something is there, what is the value?

}
```
This lets the designer avoid absolute nonsense that is *null*. 
See the [evil-ness of null](http://programmers.stackexchange.com/questions/12777/are-null-references-really-a-bad-thing).

ZoomBA has almost all important decision making based on maybe monad.
See the *return* strategy :

```java
public static final class Return extends MonadicException{

    private Return(){ super();}

    public Return(Object o){ super(o);}

}
```

This way, we can ensure that *return* may or may not return something or nothing at all. In the same spirit, *break* , *continue* , all are implemented as *maybe* monads.
Everything eventually returns a *MonadicContainer* be it function, or
a script. Thus it is suggested that people who wants to use the system, 
relies on this behaviour, rather than passing *null* which is *dull* and evil and lame and lazy.

### Scripts 
ZoomBA scripts starts with a file or a string filled up with ZoomBA, 
to dance the interpreter round about.

#### Import 
To create a ZoomBA script, there are two alternatives :

##### String
To import from a string, use this :

```java
ZScript zs = new ZScript(txt); 
```
##### File 
If you are importing from file, there are two alternatives.
Either you are importing the script as the main script, or a sidekick
of the script, so :

```java
ZScript zMain = new ZScript(file, null); // Oh ho...   
```
that null defines that the script does not have any parent.
If there would be a parent script, then :

```java
ZScript zSideKick = new ZScript(file, zMain); // yes...   
```

#### Execution  
Now, to execute, here is what you should do :

```java
Object[] scripArgs ; // yea 
MonadicContainer mc = zs.execute(scripArgs);
```
Within the script, the *@ARGS* syntax ensures that the script 
gets the args we passed.

## ZoomBA Memory Model 

ZoomBA uses a direct addressable memory model, jargon aside, 
that thing is called an indexed memory ( well, less jargonic ), 
so we call it a map. Yes, that is what it is. ZoomBA holds the variables
in Maps.

### Environment

Any environment you want to set, obviously then, you can do it, 
once you have get hold of a memory - in ZoomBA land, they are called Context. You can put the name of the variable, and the value, 
and the variable would be accessible from the ZoomBA script.

### Stack of Execution
When method call happens, every method gets its own Context, 
which shadows the parent in case one writes data back. For every thread, 
different stack gets maintained, because ZoomBA, thanks to JVM, does not have [GIL](https://en.wikipedia.org/wiki/Global_interpreter_lock). 

In fact, such a thread of execution is automatically handled by 
the ZoomBA interpreter, each gets spawned for a thread.

## Sample 
Nobody can really tell you how ZoomBA really works.
You have [see it](https://gitlab.com/non.est.sacra/zoomba/blob/master/src/main/java/zoomba/lang/Main.java) on your own. However, ZoomBA supports `javax.Scripting` as :

 ```java
@Test
public void javaxScripting() throws Exception {
    ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
    ScriptEngine engine = scriptEngineManager.getEngineByName("zoomba");
    assertNotNull(engine);
    Object result = engine.eval("2+2");
    assertEquals(4,result);
    Bindings bindings = engine.createBindings();
    bindings.put("x", 2 );
    bindings.put("y", 2 );
    result = engine.eval("x+y", bindings);
    assertEquals(4,result);
    ScriptContext scriptContext = engine.getContext();
    scriptContext.setAttribute("x", 2, ScriptContext.ENGINE_SCOPE );
    scriptContext.setAttribute("y", 2, ScriptContext.GLOBAL_SCOPE );
    result = engine.eval("x+y");
    assertEquals(4,result);
    CompiledScript compiledScript = ((Compilable)engine).compile("x+y");
    assertEquals( engine, compiledScript.getEngine());
    result = compiledScript.eval(bindings);
    assertEquals(4,result);
    // function invoke ?
    engine.eval("def add(a,b){ a + b }",scriptContext);
    result = ((Invocable)engine).invokeFunction("add", 2, 2);
    assertEquals(4,result);
    // method invoke ?
    Object zo = engine.eval("def ZO{ def add(a,b){ a + b } } ; new(ZO) ",scriptContext);
    assertNotNull(zo);
    assertTrue(zo instanceof ZObject);
    result = ((Invocable)engine).invokeMethod(zo,"add", 2, 2);
    assertEquals(4,result);
}
 ```



## Debugging 

ZoomBA produces stack trace. Given a problem, the runtime interpreter will produce a stack trace as follows :

```scala
def f(){
 g()
}
def g(){
  h()
}
def h(){
  // using a variable that is not there 
  println(y) 
}
f() // call 
```

This produces the following output *Stack Trace* :

```shell
zoomba.lang.core.types.ZException$Variable: y :  --> tmp.zm at line 9, cols 11:11
|- println --> tmp.zm at line 9, cols 3:12
|- h --> tmp.zm at line 5, cols 3:5
|- g --> tmp.zm at line 2, cols 2:4
|- f --> tmp.zm at line 11, cols 1:3
```

This way we know the trace of the error.